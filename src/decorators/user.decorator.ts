import { createParamDecorator, ExecutionContext, UnauthorizedException } from '@nestjs/common';

export default createParamDecorator((ensureUser: boolean = true, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();

  if (ensureUser && !request.user) {
    throw new UnauthorizedException();
  }

  return request.user;
});
