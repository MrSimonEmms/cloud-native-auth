import { createParamDecorator, UnauthorizedException } from '@nestjs/common';

jest.mock('@nestjs/common', () => ({
  createParamDecorator: jest.fn(),
  UnauthorizedException: jest.fn(),
}));

describe('UserDecorator', () => {
  let fn: any;
  let ctx: any;
  let switchToHttp: any;
  let getRequest: any;
  beforeEach(async () => {
    const decorator = 'some-decorator';
    (<any>createParamDecorator).mockReturnValue(decorator);

    const { default: userDecorator } = await import('./user.decorator');

    expect(userDecorator).toBe(decorator);
    [[fn]] = (<any>createParamDecorator).mock.calls;

    getRequest = jest.fn();
    switchToHttp = jest.fn().mockReturnValue({ getRequest });
    ctx = { switchToHttp };
  });

  afterEach(() => {
    expect(switchToHttp).toBeCalledWith();
    expect(getRequest).toBeCalledWith();
  });

  describe('ensureUser not set', () => {
    it('should return user if present', () => {
      const user = 'some-user';

      getRequest.mockReturnValue({ user });

      expect(fn(undefined, ctx)).toBe(user);
    });

    it('should error if user not present', () => {
      const user = undefined;

      getRequest.mockReturnValue({ user });

      try {
        expect(fn(undefined, ctx)).toBe(user);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new UnauthorizedException());
      }
    });
  });

  describe('ensureUser is true', () => {
    it('should return user if present', () => {
      const user = 'some-user';

      getRequest.mockReturnValue({ user });

      expect(fn(true, ctx)).toBe(user);
    });

    it('should error if user not present', () => {
      const user = undefined;

      getRequest.mockReturnValue({ user });

      try {
        expect(fn(true, ctx)).toBe(user);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new UnauthorizedException());
      }
    });
  });

  describe('ensureUser is false', () => {
    it('should return user if present', () => {
      const user = 'some-user';

      getRequest.mockReturnValue({ user });

      expect(fn(false, ctx)).toBe(user);
    });

    it('should error if user not present', () => {
      const user = undefined;

      getRequest.mockReturnValue({ user });

      expect(fn(false, ctx)).toBe(undefined);
    });
  });
});
