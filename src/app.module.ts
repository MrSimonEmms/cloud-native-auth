import { Inject, Injectable, Module, OnApplicationBootstrap } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { LoggerModule, Params, PinoLogger } from 'nestjs-pino';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import { TerminusModule } from '@nestjs/terminus';
import { PrometheusModule } from '@willsoto/nestjs-prometheus';
import { AccessControlModule, RolesBuilder } from 'nest-access-control';

import config from './config';
import HealthController from './health.controller';
import MetricsController from './metrics.controller';
import OrganizationModule from './organization/organization.module';
import PassportModule from './passport/passport.module';
import RequestParserModule from './request-parser/request-parser.module';
import RolesModule from './roles/roles.module';
import UserModule from './user/user.module';
import ProviderModule from './provider/provider.module';
import RolesInterceptor from './roles/roles.interceptor';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: config,
    }),
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService): Promise<Params> =>
        configService.get<Params>('logger'),
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService): Promise<MongooseModuleOptions> =>
        configService.get<MongooseModuleOptions>('mongodb'),
    }),
    PrometheusModule.register({
      controller: MetricsController,
    }),
    AccessControlModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService, PinoLogger],
      useFactory: async (configService: ConfigService, logger: PinoLogger): Promise<RolesBuilder> =>
        RolesModule.factory(configService, logger),
    }),
    TerminusModule,
    OrganizationModule,
    PassportModule,
    ProviderModule,
    RequestParserModule,
    RolesModule,
    UserModule,
  ],
  controllers: [HealthController],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: RolesInterceptor,
    },
  ],
})
@Injectable()
export default class AppModule implements OnApplicationBootstrap {
  @Inject(ConfigService)
  private readonly config;

  @Inject(PinoLogger)
  private readonly logger;

  onApplicationBootstrap(): any {
    const providerConfig = this.config.get('providers');

    const providers = Object.keys(providerConfig)
      .map((provider) => ({
        provider,
        config: providerConfig[provider],
      }))
      .filter((item) => item.config.enabled)
      .map(({ provider }) => provider);

    if (providers.length === 0) {
      throw new Error('No providers enabled');
    }

    this.logger.info({ providers }, 'Providers enabled');

    const { confirmEmail } = this.config.get('hooks');

    if (confirmEmail.enabled) {
      if (!confirmEmail.redirectUrl) {
        throw new Error('Redirect URL must be set if confirmEmail hook is enabled');
      }

      if (!confirmEmail.url) {
        throw new Error('URL must be set if confirmEmail hook is enabled');
      }
    }
  }
}
