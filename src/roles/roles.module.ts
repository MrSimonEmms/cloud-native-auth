import { promises as fs } from 'fs';
import { ConfigService } from '@nestjs/config';
import { forwardRef, Module } from '@nestjs/common';
import * as yaml from 'js-yaml';
import { RolesBuilder } from 'nest-access-control';
import { PinoLogger } from 'nestjs-pino';

import RolesController from './roles.controller';
import RolesService from './roles.service';
import { OrgRoles, OrgSubjects } from '../organization/interfaces';
import { UserRoles, UserSubjects } from '../user/interfaces';
// eslint-disable-next-line import/no-cycle
import OrganizationModule from '../organization/organization.module';

@Module({
  imports: [forwardRef(() => OrganizationModule)],
  controllers: [RolesController],
  providers: [RolesService],
  exports: [RolesService],
})
export default class RolesModule {
  private static baseRoles(rolesBuilder: RolesBuilder): RolesBuilder {
    rolesBuilder
      /* Org user role - broadly speaking, only has read access */
      .grant(OrgRoles.ORG_USER)
      .readOwn(OrgSubjects.Organization)
      /* Org maintainer role - can do pretty-much anything, including the org user role */
      .grant(OrgRoles.ORG_MAINTAINER)
      .extend(OrgRoles.ORG_USER)
      .createOwn(OrgSubjects.Organization)
      .updateOwn(OrgSubjects.Organization)
      .deleteOwn(OrgSubjects.Organization)
      /* User admin role - can do anything on our own account */
      .grant(UserRoles.USER_ADMIN)
      .readOwn(OrgSubjects.Organization)
      .createOwn(OrgSubjects.Organization)
      .createOwn(UserSubjects.User)
      .readOwn(UserSubjects.User)
      .updateOwn(UserSubjects.User)
      .deleteOwn(UserSubjects.User);

    return rolesBuilder;
  }

  static async factory(configService: ConfigService, logger: PinoLogger): Promise<RolesBuilder> {
    const filePath = configService.get('roles.filePath');
    logger.debug({ filePath }, 'Loading roles file');
    const roles = yaml.load(await fs.readFile(filePath, 'utf8')) ?? {};
    const rolesBuilder = new RolesBuilder(roles);
    RolesModule.baseRoles(rolesBuilder);
    logger.debug(
      { roles: rolesBuilder.getRoles(), grants: rolesBuilder.getGrants() },
      'Roles loaded',
    );
    return rolesBuilder;
  }
}
