import { IUser } from '../user/interfaces';

// export enum Roles {
//   ORG_MAINTAINER = 'ORG_MAINTAINER',
//   ORG_USER = 'ORG_USER',
//   /* Everyone has these roles */
//   USER_ADMIN = 'USER_ADMIN',
// }

export interface IRoleUser extends IUser {
  // roles: Roles[];
}
