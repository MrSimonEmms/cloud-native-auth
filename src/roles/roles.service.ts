import { ForbiddenException, Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PinoLogger } from 'nestjs-pino';
import { InjectRolesBuilder, Role, RolesBuilder } from 'nest-access-control';
import { IQueryInfo, Permission } from 'accesscontrol';
import { UserDocument } from '../user/user.schema';
import OrganizationService from '../organization/organization.service';
import { UserRoles } from '../user/interfaces';

@Injectable()
export default class RolesService {
  @Inject(ConfigService)
  private readonly configService: ConfigService;

  @Inject(PinoLogger)
  private readonly logger: PinoLogger;

  @Inject(OrganizationService)
  private readonly organizationService: OrganizationService;

  @InjectRolesBuilder()
  private readonly roleBuilder: RolesBuilder;

  getGrants() {
    return this.roleBuilder.getGrants();
  }

  getRoles() {
    return this.roleBuilder.getRoles();
  }

  async hasAccess(
    user: UserDocument,
    resourceRoles: Role[],
    organizationId?: string,
  ): Promise<{ permissions: Permission[]; hasAccess: boolean; userRoles: string[] }> {
    /* Everyone has user admin - allows user's to edit themselves */
    const userRoles: string[] = [UserRoles.USER_ADMIN];

    if (organizationId) {
      this.logger.info('Adding organization roles');
      userRoles.push(...(await this.organizationService.getUserRoles(organizationId, user)));
    }

    this.logger.info(
      { organizationId, userId: user.get('id'), userRoles, resourceRoles },
      'User roles retrieved',
    );

    const permissions: Permission[] = [];
    let hasAccess: boolean = false;

    try {
      permissions.push(
        ...resourceRoles.map(
          (role): Permission => {
            const queryInfo: IQueryInfo = {
              ...role,
              role: userRoles,
            };
            return this.roleBuilder.permission(queryInfo);
          },
        ),
      );

      hasAccess = permissions.every(({ granted }) => granted);
    } catch (err) {
      this.logger.warn({ err }, 'Error getting role permissions');
    }

    this.logger.info({ userRoles, resourceRoles, hasAccess }, 'Verifying role access');

    if (!hasAccess) {
      throw new ForbiddenException();
    }

    return {
      permissions,
      hasAccess,
      userRoles,
    };
  }
}
