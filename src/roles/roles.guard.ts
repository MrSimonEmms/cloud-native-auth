import { Role } from 'nest-access-control';
import { CanActivate, ExecutionContext, Inject, Injectable } from '@nestjs/common';
import RolesService from './roles.service';
import RolesContext from './roles.context';

@Injectable()
export default class RoleGuard extends RolesContext implements CanActivate {
  @Inject(RolesService)
  private readonly rolesService: RolesService;

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const roles = this.reflector.get<Role[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }

    const user = this.getUser(context);
    const orgId = this.getOrgId(context);

    const { hasAccess } = await this.rolesService.hasAccess(user, roles, orgId);

    return hasAccess;
  }
}
