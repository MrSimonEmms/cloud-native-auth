import { Test, TestingModule } from '@nestjs/testing';
import { Reflector } from '@nestjs/core';
import { when } from 'jest-when';
import RoleGuard from './roles.guard';
import RolesService from './roles.service';
import { OrgIdMetaData } from '../organization/orgId.decorator';

describe('RolesGuard', () => {
  let guard: RoleGuard;
  let rolesService: any;
  let reflector: any;
  let context: any;

  beforeEach(async () => {
    rolesService = {
      hasAccess: jest.fn(),
    };
    reflector = {
      get: jest.fn(),
    };
    context = {
      getHandler: jest.fn(),
      switchToHttp: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RoleGuard,
        {
          provide: RolesService,
          useValue: rolesService,
        },
        {
          provide: Reflector,
          useValue: reflector,
        },
      ],
    }).compile();

    guard = module.get<RoleGuard>(RoleGuard);
  });

  it('should return true if no roles defined', async () => {
    const handler = 'some-handler';
    context.getHandler.mockReturnValue(handler);
    reflector.get.mockReturnValue(undefined);

    expect(await guard.canActivate(context)).toBe(true);

    expect(context.getHandler).toBeCalledWith();
    expect(reflector.get).toBeCalledWith('roles', handler);
  });

  it('should return hasAccess result if roles defined - no org defined', async () => {
    const handler = 'some-handler2';
    const roles = 'some-roles';
    const hasAccess = 'access-result';
    const req = {
      user: 'some-user',
    };
    const http = {
      getRequest: jest.fn().mockReturnValue(req),
    };
    context.getHandler.mockReturnValue(handler);
    context.switchToHttp.mockReturnValue(http);
    when(reflector.get).calledWith('roles').mockReturnValue(roles);
    when(reflector.get).calledWith(OrgIdMetaData).mockReturnValue(undefined);
    rolesService.hasAccess.mockResolvedValue({ hasAccess });

    expect(await guard.canActivate(context)).toBe(true);

    expect(context.getHandler).toBeCalledWith();
    expect(reflector.get).toBeCalledWith('roles', handler);
  });

  it('should return hasAccess result if roles defined - org metadata undefined', async () => {
    const handler = 'some-handler6';
    const roles = 'some-roles6';
    const hasAccess = 'access-result6';
    const metadata = undefined;
    const req = {
      user: 'some-user3',
    };
    const http = {
      getRequest: jest.fn().mockReturnValue(req),
    };
    context.getHandler.mockReturnValue(handler);
    context.switchToHttp.mockReturnValue(http);
    when(reflector.get).calledWith('roles', handler).mockReturnValue(roles);
    when(reflector.get).calledWith(OrgIdMetaData, handler).mockReturnValue(metadata);
    rolesService.hasAccess.mockResolvedValue({ hasAccess });

    expect(await guard.canActivate(context)).toBe(hasAccess);

    expect(rolesService.hasAccess).toBeCalledWith(req.user, roles, undefined);
  });

  it('should return hasAccess result if roles defined - org param defined', async () => {
    const handler = 'some-handler3';
    const roles = 'some-roles3';
    const hasAccess = 'access-result3';
    const metadata = {
      param: true,
      flag: 'orgId',
    };
    const req = {
      user: 'some-user3',
      params: {
        [metadata.flag]: 'some-org-id',
      },
    };
    const http = {
      getRequest: jest.fn().mockReturnValue(req),
    };
    context.getHandler.mockReturnValue(handler);
    context.switchToHttp.mockReturnValue(http);
    when(reflector.get).calledWith('roles', handler).mockReturnValue(roles);
    when(reflector.get).calledWith(OrgIdMetaData, handler).mockReturnValue(metadata);
    rolesService.hasAccess.mockResolvedValue({ hasAccess });

    expect(await guard.canActivate(context)).toBe(hasAccess);

    expect(rolesService.hasAccess).toBeCalledWith(req.user, roles, req.params[metadata.flag]);
  });

  it('should return hasAccess result if roles defined - org query defined', async () => {
    const handler = 'some-handler3';
    const roles = 'some-roles3';
    const hasAccess = 'access-result3';
    const metadata = {
      false: true,
      flag: 'orgId',
    };
    const req = {
      user: 'some-user3',
      query: {
        [metadata.flag]: 'some-org-id',
      },
    };
    const http = {
      getRequest: jest.fn().mockReturnValue(req),
    };
    context.getHandler.mockReturnValue(handler);
    context.switchToHttp.mockReturnValue(http);
    when(reflector.get).calledWith('roles', handler).mockReturnValue(roles);
    when(reflector.get).calledWith(OrgIdMetaData, handler).mockReturnValue(metadata);
    rolesService.hasAccess.mockResolvedValue({ hasAccess });

    expect(await guard.canActivate(context)).toBe(hasAccess);

    expect(rolesService.hasAccess).toBeCalledWith(req.user, roles, req.query[metadata.flag]);
  });
});
