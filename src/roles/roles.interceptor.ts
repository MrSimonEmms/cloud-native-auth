import { CallHandler, ExecutionContext, Inject, Injectable, NestInterceptor } from '@nestjs/common';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Permission } from 'accesscontrol';
import { InjectRolesBuilder, Role, RolesBuilder } from 'nest-access-control';
import { PinoLogger } from 'nestjs-pino';
import RolesService from './roles.service';
import RolesContext from './roles.context';
import { IgnoreRolesInterceptorKey } from './ignoreRolesInterceptor.decorator';

export interface Response<T> {
  data: T;
}

@Injectable()
export default class RolesInterceptor<T>
  extends RolesContext
  implements NestInterceptor<T, Response<T>> {
  @Inject(RolesService)
  private readonly rolesService: RolesService;

  @InjectRolesBuilder()
  private readonly roleBuilder: RolesBuilder;

  @Inject(PinoLogger)
  private readonly logger: PinoLogger;

  protected filterByPermissions(permissions: Permission[], input: Object) {
    return permissions.reduce((data, permission) => permission.filter(data), input);
  }

  protected getOutputData(data: { toObject?: Function }): Object {
    const hasToObjectFn = typeof data?.toObject === 'function';

    this.logger.debug({ hasToObjectFn }, 'Converting data to object format');

    if (hasToObjectFn) {
      return data.toObject();
    }
    return data;
  }

  async intercept(context: ExecutionContext, next: CallHandler): Promise<Observable<Response<T>>> {
    return next.handle().pipe(
      map((data) => {
        const ignoreInterceptor = this.reflector.get<boolean | void>(
          IgnoreRolesInterceptorKey,
          context.getHandler(),
        );
        if (ignoreInterceptor) {
          this.logger.debug('Interceptor ignored - returning data as-is');
          return data;
        }

        const roles = this.reflector.get<Role[]>('roles', context.getHandler());
        if (!roles) {
          this.logger.debug('No roles specified - returning data as-is');
          return data;
        }

        const user = this.getUser(context);
        const orgId = this.getOrgId(context);

        this.logger.debug('Filtering output by role permissions');

        return this.rolesService
          .hasAccess(user, roles, orgId)
          .then(({ permissions }) =>
            this.filterByPermissions(permissions, this.getOutputData(data)),
          );
      }),
    );
  }
}
