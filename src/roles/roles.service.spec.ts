import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import { PinoLogger } from 'nestjs-pino';
import { Role, ROLES_BUILDER_TOKEN } from 'nest-access-control';
import { ForbiddenException } from '@nestjs/common';

import RolesService from './roles.service';
import OrganizationService from '../organization/organization.service';
import { UserRoles } from '../user/interfaces';

jest.mock('nestjs-pino');
jest.mock('@nestjs/config');

describe('RolesService', () => {
  let service: RolesService;
  let orgService: any;
  let rolesBuilder: any;

  beforeEach(async () => {
    orgService = {
      getUserRoles: jest.fn(),
    };
    rolesBuilder = {
      getGrants: jest.fn(),
      getRoles: jest.fn(),
      permission: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RolesService,
        {
          provide: ConfigService,
          useClass: ConfigService,
        },
        {
          provide: PinoLogger,
          useClass: PinoLogger,
        },
        {
          provide: OrganizationService,
          useValue: orgService,
        },
        {
          provide: ROLES_BUILDER_TOKEN,
          useValue: rolesBuilder,
        },
      ],
    }).compile();

    service = module.get<RolesService>(RolesService);
  });

  describe('#getGrants', () => {
    it('should return the result of getGrants', () => {
      const grants = 'some-grants';
      rolesBuilder.getGrants.mockReturnValue(grants);

      expect(service.getGrants()).toBe(grants);
    });
  });

  describe('#getRoles', () => {
    it('should return the result of getRoles', () => {
      const roles = 'some-roles';
      rolesBuilder.getRoles.mockReturnValue(roles);

      expect(service.getRoles()).toBe(roles);
    });
  });

  describe('#hasAccess', () => {
    let user: any;
    let resourcesRoles: Role[];
    beforeEach(() => {
      user = {
        get: jest.fn(),
      };
      resourcesRoles = [];
    });

    describe('approved', () => {
      it('should grant permission with no organization ID and no roles', async () => {
        expect(await service.hasAccess(user, resourcesRoles)).toEqual({
          permissions: [],
          hasAccess: true,
          userRoles: [UserRoles.USER_ADMIN],
        });

        expect(orgService.getUserRoles).not.toBeCalled();
      });

      it('should grant permission with no organization ID', async () => {
        resourcesRoles.push({
          resource: 'some-resource',
          action: 'create',
          possession: 'own',
        });

        const permissions = [
          {
            permission: 1,
            granted: true,
          },
        ];

        rolesBuilder.permission.mockReturnValue(permissions[0]);

        expect(await service.hasAccess(user, resourcesRoles)).toEqual({
          permissions,
          hasAccess: true,
          userRoles: [UserRoles.USER_ADMIN],
        });

        expect(orgService.getUserRoles).not.toBeCalled();
      });

      it('should grant permission with an organization ID', async () => {
        resourcesRoles.push({
          resource: 'some-resource',
          action: 'create',
          possession: 'own',
        });
        const orgId = 'some-org-id';
        const userRoles = ['userRole1', 'userRole2'];
        orgService.getUserRoles.mockResolvedValue(userRoles);

        const permissions = [
          {
            permission: 1,
            granted: true,
          },
        ];

        rolesBuilder.permission.mockReturnValue(permissions[0]);

        expect(await service.hasAccess(user, resourcesRoles, orgId)).toEqual({
          permissions,
          hasAccess: true,
          userRoles: [UserRoles.USER_ADMIN, ...userRoles],
        });

        expect(orgService.getUserRoles).toBeCalledWith(orgId, user);
      });
    });

    describe('rejected', () => {
      it('should reject permission if RoleBuilder.permission errors', async () => {
        resourcesRoles.push({
          resource: 'some-resource',
          action: 'create',
          possession: 'own',
        });

        rolesBuilder.permission.mockImplementation(() => {
          throw new Error('permission error');
        });

        try {
          await service.hasAccess(user, resourcesRoles);
          throw new Error('invalid');
        } catch (err) {
          expect(err).toEqual(new ForbiddenException());
        }

        expect(orgService.getUserRoles).not.toBeCalled();
      });

      it('should reject permission with no organization ID', async () => {
        resourcesRoles.push({
          resource: 'some-resource',
          action: 'create',
          possession: 'own',
        });

        const permissions = [
          {
            permission: 1,
            granted: false,
          },
        ];

        rolesBuilder.permission.mockReturnValue(permissions[0]);

        try {
          await service.hasAccess(user, resourcesRoles);
          throw new Error('invalid');
        } catch (err) {
          expect(err).toEqual(new ForbiddenException());
        }

        expect(orgService.getUserRoles).not.toBeCalled();
      });

      it('should reject permission with an organization ID', async () => {
        resourcesRoles.push({
          resource: 'some-resource',
          action: 'create',
          possession: 'own',
        });
        const orgId = 'some-org-id2';
        const userRoles = ['userRole12', 'userRole22'];
        orgService.getUserRoles.mockResolvedValue(userRoles);

        const permissions = [
          {
            permission: 1,
            granted: false,
          },
        ];

        rolesBuilder.permission.mockReturnValue(permissions[0]);

        try {
          await service.hasAccess(user, resourcesRoles, orgId);
          throw new Error('invalid');
        } catch (err) {
          expect(err).toEqual(new ForbiddenException());
        }

        expect(orgService.getUserRoles).toBeCalledWith(orgId, user);
      });

      it('should reject permission with an organization ID if not roles all are granted', async () => {
        resourcesRoles.push(
          {
            resource: 'some-resource',
            action: 'create',
            possession: 'own',
          },
          {
            resource: 'some-resource2',
            action: 'delete',
            possession: 'any',
          },
        );
        const orgId = 'some-org-id2';
        const userRoles = ['userRole12', 'userRole22'];
        orgService.getUserRoles.mockResolvedValue(userRoles);

        const permissions = [
          {
            permission: 1,
            granted: true,
          },
          {
            permission: 2,
            granted: false,
          },
        ];

        permissions.forEach((item) => {
          rolesBuilder.permission.mockReturnValue(item);
        });

        try {
          await service.hasAccess(user, resourcesRoles, orgId);
          throw new Error('invalid');
        } catch (err) {
          expect(err).toEqual(new ForbiddenException());
        }

        expect(orgService.getUserRoles).toBeCalledWith(orgId, user);
      });
    });
  });
});
