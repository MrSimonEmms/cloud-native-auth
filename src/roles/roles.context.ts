import { ExecutionContext, Inject, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UserDocument } from '../user/user.schema';
import { IOrgFlagMetadata } from '../organization/interfaces';
import { OrgIdMetaData } from '../organization/orgId.decorator';

@Injectable()
export default class RolesContext {
  @Inject(Reflector)
  protected readonly reflector: Reflector;

  protected getOrgId(context: ExecutionContext): string | undefined {
    let orgId: string | undefined;

    const metadata = this.reflector.get<IOrgFlagMetadata>(OrgIdMetaData, context.getHandler());
    if (metadata) {
      const req = context.switchToHttp().getRequest();
      orgId = (metadata.param ? req.params : req.query)[metadata.flag];
    }

    return orgId;
  }

  protected getUser(context: ExecutionContext): UserDocument {
    const request = context.switchToHttp().getRequest();
    return request.user;
  }
}
