import { SetMetadata } from '@nestjs/common';

export const IgnoreRolesInterceptorKey: string = 'NO_ROLES_INTERCEPTOR';

export const IgnoreRolesInterceptor = () =>
  SetMetadata<string, boolean>(IgnoreRolesInterceptorKey, true);
