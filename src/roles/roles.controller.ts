import { Body, Controller, Get, HttpCode, Inject, Post, UseGuards } from '@nestjs/common';
import { Role } from 'nest-access-control';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiResponse } from '@nestjs/swagger';

import RolesService from './roles.service';
import User from '../decorators/user.decorator';
import { UserRoles } from '../user/interfaces';

@UseGuards(AuthGuard('jwt'))
@Controller('role')
export default class RolesController {
  @Inject(RolesService)
  private readonly rolesService: RolesService;

  @Inject(Reflector)
  private readonly reflector: Reflector;

  @Get('/')
  @ApiBearerAuth('jwt')
  @ApiResponse({
    status: 200,
    description: 'Return the roles and grants',
    schema: {
      type: 'object',
      properties: {
        grants: {
          type: 'object',
          example: {
            [UserRoles.USER_ADMIN]: {
              'create:own': ['*'],
              'delete:own': ['*'],
              'read:own': ['*'],
              'update:own': ['*'],
            },
          },
        },
        roles: {
          type: 'array',
          example: [UserRoles.USER_ADMIN],
          items: {
            type: 'string',
          },
        },
      },
    },
  })
  @ApiOperation({ tags: ['roles'], summary: 'Return the roles and grants' })
  getPermissions() {
    return {
      grants: this.rolesService.getGrants(),
      roles: this.rolesService.getRoles(),
    };
  }

  @Post()
  @HttpCode(200)
  @ApiBearerAuth('jwt')
  @ApiResponse({
    status: 200,
    description: 'Verify if access granted',
    schema: {
      type: 'object',
      properties: {
        hasAccess: {
          type: 'boolean',
        },
        orgId: {
          type: 'string',
        },
        userRoles: {
          type: 'array',
          example: [UserRoles.USER_ADMIN],
          items: {
            type: 'string',
          },
        },
      },
    },
  })
  @ApiOperation({ tags: ['roles'], summary: 'Verify if access granted' })
  async hasAccess(@User() user, @Body() { orgId, roles }: { orgId?: string; roles: Role[] }) {
    const { hasAccess, userRoles } = await this.rolesService.hasAccess(user, roles, orgId);

    return {
      hasAccess,
      orgId,
      userRoles,
    };
  }
}
