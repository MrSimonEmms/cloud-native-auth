import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import { HTTP_CODE_METADATA, METHOD_METADATA, PATH_METADATA } from '@nestjs/common/constants';
import { RequestMethod } from '@nestjs/common';

import RolesController from './roles.controller';
import RolesService from './roles.service';

describe('RolesController', () => {
  let controller: RolesController;
  let rolesService: any;

  beforeEach(async () => {
    rolesService = {
      getGrants: jest.fn(),
      getRoles: jest.fn(),
      hasAccess: jest.fn(),
    };
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RolesController],
      providers: [
        { provide: RolesService, useValue: rolesService },
        {
          provide: ConfigService,
          useClass: ConfigService,
        },
      ],
    }).compile();

    controller = module.get<RolesController>(RolesController);
  });

  describe('#getPermissions', () => {
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.getPermissions)).toEqual('/');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.getPermissions)).toEqual(
        RequestMethod.GET,
      );
    });

    it('should return the grants and roles', () => {
      const grants = 'some-grants';
      const roles = 'some-roles';
      rolesService.getGrants.mockReturnValue(grants);
      rolesService.getRoles.mockReturnValue(roles);

      expect(controller.getPermissions()).toEqual({
        grants,
        roles,
      });
    });
  });

  describe('#hasAccess', () => {
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.hasAccess)).toEqual('/');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.hasAccess)).toEqual(
        RequestMethod.POST,
      );
      expect(Reflect.getMetadata(HTTP_CODE_METADATA, controller.hasAccess)).toEqual(200);
    });

    it('should return access rights', async () => {
      const result = {
        hasAccess: 'some-access',
        userRoles: 'some-user-roles',
        permissions: 'some-permission',
      };
      const user = 'some-user';
      const body: any = {
        orgId: 'some-org-id',
        roles: 'some-roles',
      };

      rolesService.hasAccess.mockResolvedValue(result);

      expect(await controller.hasAccess(user, body)).toEqual({
        hasAccess: result.hasAccess,
        orgId: body.orgId,
        userRoles: result.userRoles,
      });

      expect(rolesService.hasAccess).toBeCalledWith(user, body.roles, body.orgId);
    });
  });
});
