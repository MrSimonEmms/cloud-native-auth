import { Test, TestingModule } from '@nestjs/testing';
import { PinoLogger } from 'nestjs-pino';
import { RequestQueryParser } from '@nestjsx/crud-request';
import { BadRequestException } from '@nestjs/common';
import { ParsedRequestParams } from '@nestjsx/crud-request/lib/interfaces';
import RequestParserService from './request-parser.service';

jest.mock('nestjs-pino');
jest.mock('@nestjsx/crud-request');

describe('RequestParserService', () => {
  let service: RequestParserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RequestParserService,
        {
          provide: PinoLogger,
          useClass: PinoLogger,
        },
      ],
    }).compile();

    service = module.get<RequestParserService>(RequestParserService);
  });

  describe('#queryBuilder', () => {
    beforeEach(() => {
      jest.spyOn(service, 'queryParser' as any);
    });

    it('should handle no input', () => {
      expect((<any>service).queryBuilder({ filter: [], or: [] })).toEqual({});
    });

    it('should handle filters and ors', () => {
      const filter = [
        {
          field: 'some-filter-field',
          value: 'some-filter-value',
        },
      ];
      const or = [
        {
          field: 'some-or-field',
          value: 'some-or-value',
        },
        {
          field: 'some-or-field2',
          value: 'some-or-value2',
        },
      ];

      const result = {
        $or: [],
      };

      filter.forEach((item) => {
        (<any>service).queryParser.mockImplementationOnce((fn) => () => fn(item, item.value));

        result[item.field] = item.value;
      });

      let count = 0;
      (<any>service).queryParser.mockImplementationOnce((fn) => () => {
        const item = or[count];

        count += 1;

        result.$or.push({
          [item.field]: item.value,
        });

        return fn(item, item.value);
      });

      expect((<any>service).queryBuilder({ filter, or } as ParsedRequestParams)).toEqual(result);

      expect((<any>service).queryParser).toBeCalledTimes(2);
    });
  });

  describe('#queryMapper', () => {
    let mapper;
    let findQuery;

    beforeEach(() => {
      findQuery = (
        operator: string,
      ): {
        query: string;
        operator?: string;
        factory?: ({ value: unknown }) => Object;
      } => {
        const query = (<any>service).queryMapper().find((item) => item.query === operator);

        if (!query) {
          throw new Error(`Not found: ${query}`);
        }

        return query;
      };
    });

    describe('#between', () => {
      beforeEach(() => {
        mapper = findQuery('$between');

        expect(mapper.query).toBe('$between');
        expect(mapper.operator).toBeUndefined();
        expect(typeof mapper.factory).toBe('function');
      });

      it('should error if no "from" value', () => {
        expect(() => mapper.factory({ value: [] })).toThrowError(
          new Error('Between query must have "from" defined'),
        );
      });

      it('should error if no "to" value', () => {
        expect(() => mapper.factory({ value: ['from'] })).toThrowError(
          new Error('Between query must have "to" defined'),
        );
      });

      it('should return the mongodb query', () => {
        const from = 'some-from';
        const to = 'some-to';
        expect(mapper.factory({ value: [from, to] })).toEqual({
          $gte: from,
          $lte: to,
        });
      });
    });

    describe('#cont', () => {
      beforeEach(() => {
        mapper = findQuery('$cont');

        expect(mapper.query).toBe('$cont');
        expect(mapper.operator).toBeUndefined();
        expect(typeof mapper.factory).toBe('function');
      });

      it('should return the mongodb query', () => {
        const value = 'some-value';
        expect(mapper.factory({ value })).toEqual({
          $regex: new RegExp(value),
        });
      });
    });

    describe('#excl', () => {
      beforeEach(() => {
        mapper = findQuery('$excl');

        expect(mapper.query).toBe('$excl');
        expect(mapper.operator).toBeUndefined();
        expect(typeof mapper.factory).toBe('function');
      });

      it('should return the mongodb query', () => {
        const value = 'some-value';
        expect(mapper.factory({ value })).toEqual({
          $not: {
            $regex: new RegExp(value),
          },
        });
      });
    });

    describe('#ends', () => {
      beforeEach(() => {
        mapper = findQuery('$ends');

        expect(mapper.query).toBe('$ends');
        expect(mapper.operator).toBeUndefined();
        expect(typeof mapper.factory).toBe('function');
      });

      it('should return the mongodb query', () => {
        const value = 'some-value';
        expect(mapper.factory({ value })).toEqual({
          $regex: new RegExp(`${value}$`),
        });
      });
    });

    describe('#isnull', () => {
      beforeEach(() => {
        mapper = findQuery('$isnull');

        expect(mapper.query).toBe('$isnull');
        expect(mapper.operator).toBeUndefined();
        expect(typeof mapper.factory).toBe('function');
      });

      it('should return the mongodb query', () => {
        expect(mapper.factory()).toEqual({
          $eq: null,
        });
      });
    });

    describe('#notin', () => {
      it('should return the mongodb query', () => {
        mapper = findQuery('$notin');

        expect(mapper.query).toBe('$notin');
        expect(mapper.operator).toBe('$nin');
        expect(mapper.factory).toBeUndefined();
      });
    });

    describe('#notnull', () => {
      beforeEach(() => {
        mapper = findQuery('$notnull');

        expect(mapper.query).toBe('$notnull');
        expect(mapper.operator).toBeUndefined();
        expect(typeof mapper.factory).toBe('function');
      });

      it('should return the mongodb query', () => {
        expect(mapper.factory()).toEqual({
          $ne: null,
        });
      });
    });

    describe('#starts', () => {
      beforeEach(() => {
        mapper = findQuery('$starts');

        expect(mapper.query).toBe('$starts');
        expect(mapper.operator).toBeUndefined();
        expect(typeof mapper.factory).toBe('function');
      });

      it('should return the mongodb query', () => {
        const value = 'some-value';
        expect(mapper.factory({ value })).toEqual({
          $regex: new RegExp(`^${value}`),
        });
      });
    });
  });

  describe('#queryParser', () => {
    beforeEach(() => {
      jest.spyOn(service, 'queryMapper' as any);
    });

    it('should use a mapped factory', () => {
      const query = 'item';
      const mockReturn = { hello: 'world' };
      const queryMap = [
        {
          query,
          factory: jest.fn().mockReturnValue(mockReturn),
        },
      ];
      (<any>service).queryMapper.mockReturnValue(queryMap);

      const factory = jest.fn();
      const fn = (<any>service).queryParser(factory);
      const input = { operator: query };

      expect(fn(input)).toBeUndefined();

      expect(factory).toBeCalledWith(input, mockReturn);
    });

    it('should use a mapped operator', () => {
      const query = 'item';
      const operator = 'some-operator';
      const value = 'some-value';
      const queryMap = [
        {
          query,
          operator,
        },
      ];
      (<any>service).queryMapper.mockReturnValue(queryMap);

      const factory = jest.fn();
      const fn = (<any>service).queryParser(factory);
      const input = { operator: query, value };

      expect(fn(input)).toBeUndefined();

      expect(factory).toBeCalledWith(input, { [operator]: value });
    });

    it('should use the included operator', () => {
      const query = 'item2';
      const value = 'some-value2';
      (<any>service).queryMapper.mockReturnValue([]);

      const factory = jest.fn();
      const fn = (<any>service).queryParser(factory);
      const input = { operator: query, value };

      expect(fn(input)).toBeUndefined();

      expect(factory).toBeCalledWith(input, { [query]: value });
    });
  });

  describe('#parse', () => {
    beforeEach(() => {
      jest.spyOn(service, 'queryBuilder' as any);
    });

    it('should error if cannot parse the query strings', () => {
      const error = new Error('some-error');
      const parser = {
        parseQuery: jest.fn(() => {
          throw error;
        }),
      };
      (<any>RequestQueryParser.create).mockReturnValue(parser);

      const queryString = { hello: 'world' };
      try {
        service.parse(queryString);
      } catch (err) {
        expect(err).toEqual(new BadRequestException(err.message));

        expect(parser.parseQuery).toBeCalledWith(queryString);
      }
    });

    it('should error if cannot parse the query', () => {
      const parser = {
        parseQuery: jest.fn(),
        sort: [
          {
            field: 'field1',
            order: 'ASC',
          },
          {
            field: 'field2',
            order: 'DESC',
          },
        ],
      };
      (<any>RequestQueryParser.create).mockReturnValue(parser);

      const error = new Error('query builder error');

      (<any>service).queryBuilder.mockImplementation(() => {
        throw error;
      });

      const queryString = { hello2: 'world2' };
      try {
        service.parse(queryString);
      } catch (err) {
        expect(err).toEqual(new BadRequestException(err.message));

        expect(parser.parseQuery).toBeCalledWith(queryString);
        expect((<any>service).queryBuilder).toBeCalledWith(parser);
      }
    });

    it('should return the query and sortBy values', () => {
      const parser = {
        parseQuery: jest.fn(),
        sort: [
          {
            field: 'field1',
            order: 'ASC',
          },
          {
            field: 'field2',
            order: 'DESC',
          },
        ],
      };
      (<any>RequestQueryParser.create).mockReturnValue(parser);

      const query = 'some-query';

      (<any>service).queryBuilder.mockReturnValue(query);

      const queryString = { hello3: 'world3' };

      expect(service.parse(queryString)).toEqual({
        query,
        sortBy: parser.sort.reduce(
          (result, item) => ({
            ...result,
            [item.field]: item.order === 'ASC' ? 1 : -1,
          }),
          {},
        ),
      });

      expect(parser.parseQuery).toBeCalledWith(queryString);
      expect((<any>service).queryBuilder).toBeCalledWith(parser);
    });
  });
});
