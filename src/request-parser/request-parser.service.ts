import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { QueryFilter } from '@nestjsx/crud-request/lib/types';
import { ParsedRequestParams } from '@nestjsx/crud-request/lib/interfaces';
import { FilterQuery } from 'mongoose';
import { RequestQueryParser } from '@nestjsx/crud-request';
import { PinoLogger } from 'nestjs-pino';

@Injectable()
export default class RequestParserService {
  @Inject(PinoLogger)
  private readonly logger: PinoLogger;

  protected queryBuilder({ filter, or }: ParsedRequestParams): FilterQuery<Object> {
    const query: FilterQuery<Object> = {};

    filter.forEach(
      this.queryParser((item, value) => {
        query[item.field] = value;
      }),
    );
    or.forEach(
      this.queryParser((item, value) => {
        if (!Array.isArray(query.$or)) {
          query.$or = [];
        }

        query.$or.push({
          [item.field]: value,
        });
      }),
    );

    this.logger.debug({ filter, or, query }, 'Parsed query strings');

    return query;
  }

  protected queryMapper(): {
    query: string;
    operator?: string;
    factory?: (QueryFilter) => Object;
  }[] {
    return [
      {
        query: '$between',
        factory: ({ value }) => {
          const [from, to] = value;
          if (!from) {
            throw new Error('Between query must have "from" defined');
          }

          if (!to) {
            throw new Error('Between query must have "to" defined');
          }

          return {
            $gte: from,
            $lte: to,
          };
        },
      },
      {
        query: '$cont',
        factory: ({ value }) => ({
          $regex: new RegExp(value),
        }),
      },
      {
        query: '$excl',
        factory: ({ value }) => ({
          $not: {
            $regex: new RegExp(value),
          },
        }),
      },
      {
        query: '$ends',
        factory: ({ value }) => ({
          $regex: new RegExp(`${value}$`),
        }),
      },
      {
        query: '$isnull',
        factory: () => ({
          $eq: null,
        }),
      },
      {
        query: '$notin',
        operator: '$nin',
      },
      {
        query: '$notnull',
        factory: () => ({
          $ne: null,
        }),
      },
      {
        query: '$starts',
        factory: ({ value }) => ({
          $regex: new RegExp(`^${value}`),
        }),
      },
    ];
  }

  protected queryParser(
    factory: (item: QueryFilter, value: Object) => void,
  ): (item: QueryFilter) => void {
    return (input: QueryFilter) => {
      const mapped = this.queryMapper().find((mapper) => mapper.query === input.operator);

      const value = {};
      if (mapped) {
        /* Operator defined - value needs to be cast */
        if (mapped.factory) {
          Object.assign(value, mapped.factory(input));
        } else {
          value[mapped.operator] = input.value;
        }
      } else {
        /* No operator mapped - use as-is */
        value[input.operator] = input.value;
      }

      factory(input, value);
    };
  }

  parse(queryStrings: Object): { query: FilterQuery<Object>; sortBy: Object } {
    const parser = RequestQueryParser.create();
    try {
      parser.parseQuery(queryStrings);
    } catch (err) {
      this.logger.error({ err }, 'Query parser error');
      throw new BadRequestException(err.message);
    }

    /* Convert sort to Mongo logic */
    const sortBy = parser.sort.reduce(
      (result, { field, order }) => ({
        ...result,
        [field]: order === 'ASC' ? 1 : -1,
      }),
      {},
    );

    try {
      return {
        query: this.queryBuilder(parser),
        sortBy,
      };
    } catch (err) {
      this.logger.error({ err }, 'Query builder error');
      throw new BadRequestException(err.message);
    }
  }
}
