import { registerAs } from '@nestjs/config';

export default registerAs('jwt', () => ({
  expiresIn: process.env.JWT_EXPIRY ?? '30d',
  issuer: process.env.JWT_ISSUER ?? process.env.npm_package_name,
  secret: process.env.JWT_SECRET,
}));
