import { registerAs } from '@nestjs/config';
import { MongooseModuleOptions } from '@nestjs/mongoose';

export default registerAs(
  'mongodb',
  (): MongooseModuleOptions => ({
    uri: process.env.MONGODB_URL,
    autoIndex: process.env.MONGODB_AUTO_INDEX !== 'false',
    dbName: process.env.MONGODB_DB_NAME,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: Number(process.env.MONGODB_POOL_SIZE ?? 5),
    useCreateIndex: true,
  }),
);
