import * as path from 'path';
import { registerAs } from '@nestjs/config';

export default registerAs('roles', () => ({
  filePath: process.env.ROLES_PATH ?? path.join(__dirname, '..', '..', 'roles.yaml'),
}));
