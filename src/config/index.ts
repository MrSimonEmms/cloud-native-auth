import cookies from './cookies';
import domain from './domain';
import hooks from './hooks';
import jwt from './jwt';
import logger from './logger';
import mongodb from './mongodb';
import organization from './organization';
import providers from './providers';
import roles from './roles';
import server from './server';

export default [
  cookies,
  domain,
  hooks,
  jwt,
  logger,
  mongodb,
  organization,
  providers,
  roles,
  server,
];
