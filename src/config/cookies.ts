import { registerAs } from '@nestjs/config';

export default registerAs('cookies', () => ({
  secret: process.env.COOKIE_SECRET,
  secure: process.env.COOKIE_SECURE === 'true',
}));
