import { registerAs } from '@nestjs/config';

export default registerAs('server', () => ({
  enableShutdownHooks: process.env.SERVER_ENABLE_SHUTDOWN_HOOKS !== 'false',
  helmet: {
    enabled: process.env.SERVER_HELMET_ENABLED !== 'false',
  },
  port: Number(process.env.SERVER_PORT ?? 3000),
  swagger: {
    enabled: process.env.SERVER_SWAGGER_ENABLED !== 'false',
  },
}));
