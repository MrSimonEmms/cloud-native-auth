import { registerAs } from '@nestjs/config';

export default registerAs('domain', () => {
  const config = {
    current: process.env.DOMAIN_CURRENT,
    login: process.env.DOMAIN_LOGIN_PATH,
  };

  if (!config.current) {
    throw new Error('DOMAIN_CURRENT must be set');
  }
  if (!config.login) {
    throw new Error('DOMAIN_LOGIN_PATH must be set');
  }

  return config;
});
