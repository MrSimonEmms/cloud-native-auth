import { registerAs } from '@nestjs/config';

export interface IProviderValues {
  [key: string]: any;
}

export interface IProviderSetup {
  key: string;
  value: string;
  required?: false;
}

function generateConfig(
  key: string,
  opts: IProviderSetup[] = [],
): { [key: string]: IProviderValues } {
  const envvarKey = key.toUpperCase();
  const enabled = process.env[`${envvarKey}_ENABLED`] === 'true';
  const values: IProviderValues = {
    enabled,
    additionalScopes: (process.env[`${envvarKey}_SCOPES`] ?? '')
      .split(',')
      .map((item) => item.trim())
      .filter((item) => item !== ''),
  };

  opts.forEach((opt) => {
    const envVar = `${envvarKey}_${opt.value}`;
    const value = process.env[envVar];

    if (opt.required !== false && !value && enabled) {
      throw new Error(`${envVar} must be set`);
    }
    values[opt.key] = value;
  });

  return {
    [key]: values,
  };
}

export default registerAs('providers', () => ({
  ...generateConfig('password'),
  ...generateConfig('github', [
    {
      key: 'clientId',
      value: 'CLIENT_ID',
    },
    {
      key: 'clientSecret',
      value: 'CLIENT_SECRET',
    },
  ]),
  ...generateConfig('gitlab', [
    {
      key: 'clientId',
      value: 'CLIENT_ID',
    },
    {
      key: 'clientSecret',
      value: 'CLIENT_SECRET',
    },
  ]),
  ...generateConfig('google', [
    {
      key: 'clientId',
      value: 'CLIENT_ID',
    },
    {
      key: 'clientSecret',
      value: 'CLIENT_SECRET',
    },
  ]),
  ...generateConfig('twitter', [
    {
      key: 'apiKey',
      value: 'API_KEY',
    },
    {
      key: 'apiSecretKey',
      value: 'API_SECRET_KEY',
    },
  ]),
}));
