import { registerAs } from '@nestjs/config';

export default registerAs('hooks', () => ({
  confirmEmail: {
    enabled: process.env.HOOKS_CONFIRM_EMAIL_ENABLED === 'true',
    redirectUrl: process.env.HOOKS_CONFIRM_EMAIL_REDIRECT_URL,
    url: process.env.HOOKS_CONFIRM_EMAIL_URL,
  },
}));
