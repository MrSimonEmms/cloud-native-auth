import { registerAs } from '@nestjs/config';

export default registerAs('organization', () => ({
  pagination: {
    maxLimit: Number(process.env.PAGINATION_MAX_LIMIT ?? 100),
  },
}));
