import { Controller, Get } from '@nestjs/common';
import { HealthCheck, HealthCheckService, MongooseHealthIndicator } from '@nestjs/terminus';
import { ApiOperation } from '@nestjs/swagger';

@Controller('health')
export default class HealthController {
  constructor(private health: HealthCheckService, private mongodb: MongooseHealthIndicator) {}

  @Get()
  @HealthCheck()
  @ApiOperation({ tags: ['monitoring'], summary: 'Perform API health checks' })
  check() {
    return this.health.check([
      () =>
        this.mongodb.pingCheck('mongodb', {
          timeout: 1000,
        }),
    ]);
  }
}
