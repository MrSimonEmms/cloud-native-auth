import * as qs from 'querystring';
import {
  BadRequestException,
  Controller,
  Delete,
  Get,
  Inject,
  Next,
  Param,
  Redirect,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import * as passport from 'passport';
import { ApiBearerAuth, ApiParam, ApiResponse, ApiOperation, ApiQuery } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { AuthGuard } from '@nestjs/passport';

import User from '../decorators/user.decorator';
import UserService from '../user/user.service';
import { IUserQueryStrings } from '../user/interfaces';
import { User as UserSchema } from '../user/user.schema';

const userCookieId = 'loginUserId';

@Controller('provider')
export default class ProviderController {
  @Inject(ConfigService)
  private readonly config;

  @Inject('PROVIDER_LIST')
  private readonly providerList;

  @Inject('PROVIDER_FINDER')
  private readonly findProvider;

  @Inject(UserService)
  private readonly userService: UserService;

  @Get('/')
  @ApiOperation({ tags: ['provider'], summary: 'Get list of available providers' })
  @ApiResponse({
    status: 200,
    description: 'Return list of providers',
    schema: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          id: {
            type: 'string',
          },
          name: {
            type: 'string',
          },
        },
      },
    },
  })
  list() {
    return this.providerList.map((item) => item.providerConfig());
  }

  @Get('/:id')
  @UseGuards(AuthGuard(['jwt', 'anonymous'])) // Endpoint allow anonymous users
  @ApiOperation({ tags: ['provider'], summary: 'Dispatch to authentication provider login' })
  @ApiParam({ name: 'id', description: 'Provider ID', example: 'github' })
  @ApiQuery({ name: '_auth', description: 'Authorization token', type: 'string', required: false })
  @ApiResponse({ status: 302, description: 'Dispatch to authentication provider login' })
  @ApiBearerAuth('jwt')
  providerLogin(
    @Param('id') providerId,
    @User(false) user,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    const provider = this.findProvider(providerId);

    const logger = req.log.child({ providerId });

    if (user) {
      const userId = user.get('id');
      logger.info({ userId }, 'Linking user to provider');

      const existingProvider = user.getProvider(providerId);

      if (existingProvider) {
        logger.warn('Provider already registered');
        throw new BadRequestException({
          message: 'Provider already registered',
          provider: providerId,
        });
      }

      /* Set to cookie */
      const cookieSettings = {
        httpOnly: true,
        secure: this.config.get('cookies.secure'),
        signed: true,
      };

      logger.info(
        { cookieName: userCookieId, value: userId, cookieSettings },
        'Clearing user cookie',
      );
      res.cookie(userCookieId, userId, cookieSettings);
    } else {
      logger.info({ cookieName: userCookieId }, 'Adding new user to system and clearing cookie');

      /* Delete cookie */
      res.clearCookie(userCookieId);
    }

    passport.authenticate(provider.strategy(), { session: false })(req, res, next);
  }

  @Delete('/:id')
  @ApiOperation({ tags: ['provider'], summary: 'Delete a provider from the user' })
  @ApiParam({ name: 'id', description: 'Provider ID', example: 'github' })
  @ApiResponse({ status: 200, description: 'Return the user object', type: UserSchema })
  @ApiBearerAuth('jwt')
  @UseGuards(AuthGuard(['jwt']))
  async deleteProvider(@Param('id') providerId, @User() user) {
    this.findProvider(providerId);

    return this.userService.removeProvider(user, providerId);
  }

  @Redirect()
  @Get('/:id/callback')
  @ApiOperation({ tags: ['provider'], summary: 'Verify the provider and save to user' })
  @ApiParam({ name: 'id', description: 'Provider ID', example: 'github' })
  @ApiResponse({ status: 302, description: 'Dispatch to the successfully logged in page' })
  async providerCallback(@User() providerUser, @Req() req, @Res({ passthrough: true }) res) {
    const linkingUserId: string | undefined = req.signedCookies[userCookieId];
    let responseQueryParams: IUserQueryStrings;

    try {
      const user = await this.userService.addProviderToUser(providerUser, linkingUserId);

      responseQueryParams = this.userService.getPublicLoginQueryStrings(user);
    } catch (err) {
      req.log.error({ err }, 'Failed to add provider to user');

      responseQueryParams = {
        error: /* istanbul ignore next */ err?.response?.code ?? err?.message,
      };
    }

    req.log.info({ cookieName: userCookieId }, 'Clearing cookie after linking to user');
    res.clearCookie(userCookieId);

    return {
      statusCode: 302,
      url: `${this.config.get('domain.login')}?${qs.stringify(responseQueryParams)}`,
    };
  }
}
