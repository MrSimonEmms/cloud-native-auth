/* eslint-disable import/first */

import { UnauthorizedException } from '@nestjs/common';

jest.mock('passport');

import * as passport from 'passport';

import ProviderMiddleware from './provider.middleware';

describe('ProviderController', () => {
  let middleware: any;
  let findProvider: any;
  let configService: any;

  beforeEach(() => {
    findProvider = jest.fn();
    configService = {
      get: jest.fn(),
    };

    middleware = new ProviderMiddleware(findProvider, configService);
  });

  describe('#use', () => {
    let req: any;
    let res: any;

    beforeEach(() => {
      req = {
        params: {},
        log: {
          debug: jest.fn(),
          error: jest.fn(),
        },
      };
      res = {
        send: jest.fn(),
        status: jest.fn().mockReturnThis(),
      };

      (<any>passport.initialize).mockReturnValue(
        jest.fn((request, response, cb) => {
          expect(passport.initialize).toBeCalledWith();

          expect(request).toBe(req);
          expect(response).toBe(res);

          cb();
        }),
      );
    });

    it('should simiulate a successful authentication', (done) => {
      req.params.id = 'some-provider';
      const strategy = 'some-strategy';
      const provider = {
        strategy: jest.fn().mockReturnValue(strategy),
      };

      (<any>passport.authenticate).mockReturnValue(
        jest.fn((request, response, cb) => {
          expect(request).toBe(req);
          expect(response).toBe(res);

          cb();
        }),
      );

      findProvider.mockReturnValue(provider);

      middleware.use(req, res, () => {
        expect(passport.authenticate).toBeCalledWith(strategy, { session: false });

        expect(res.status).not.toBeCalled();

        done();
      });
    });

    it('should simiulate an error', (done) => {
      req.params.id = 'some-provider2';
      const strategy = 'some-strategy2';
      const provider = {
        strategy: jest.fn().mockReturnValue(strategy),
      };
      const err = 'some-error';
      (<any>passport.authenticate).mockReturnValue(
        jest.fn((request, response, cb) => {
          expect(request).toBe(req);
          expect(response).toBe(res);

          cb(err);
        }),
      );

      findProvider.mockReturnValue(provider);

      middleware.use(req, res, (error) => {
        expect(passport.authenticate).toBeCalledWith(strategy, { session: false });

        expect(error).toEqual(new UnauthorizedException());

        done();
      });
    });
  });
});
