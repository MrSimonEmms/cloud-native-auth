import { Strategy } from 'passport-strategy';

export interface IProviderConfig<T> {
  id: string;
  name: string;
  factory(): T;
}

export interface IProvider<T> {
  enabled(): boolean;
  providerConfig(): IProviderConfig<T>;
  strategy(): Strategy;
}

export interface IProviderUserData {
  tokens: {
    [key: string]: any;
  };
  type: string;
  userId: string;
  emailAddress?: string;
  name?: string;
  username?: string;
}
