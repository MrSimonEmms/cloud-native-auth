import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import { when } from 'jest-when';
import { Strategy } from 'passport-twitter';
import TwitterStrategy from './twitter.strategy';

jest.mock('passport-twitter');

describe('TwitterStrategy', () => {
  let configService: any;
  let strategy: any;
  beforeEach(async () => {
    configService = {
      get: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TwitterStrategy,
        {
          provide: ConfigService,
          useValue: configService,
        },
      ],
    }).compile();

    strategy = module.get<TwitterStrategy>(TwitterStrategy);
  });

  describe('#enabled', () => {
    it('should return the enabled status', () => {
      const enabled = 'some-response';
      configService.get.mockReturnValue(enabled);

      expect(strategy.enabled()).toBe(enabled);

      expect(configService.get).toBeCalledWith('providers.twitter.enabled');
    });
  });

  describe('#providerConfig', () => {
    it('should return the configuration', () => {
      const config = {
        apiKey: 'some-client-id',
        apiSecretKey: 'some-client-secret',
        domainCurrent: 'some-domain',
      };

      when(configService.get).calledWith('providers.twitter.apiKey').mockReturnValue(config.apiKey);
      when(configService.get)
        .calledWith('providers.twitter.apiSecretKey')
        .mockReturnValue(config.apiSecretKey);
      when(configService.get).calledWith('domain.current').mockReturnValue(config.domainCurrent);

      const providerConfig = strategy.providerConfig();

      expect(providerConfig.id).toBe('twitter');
      expect(providerConfig.name).toBe('Twitter');
      expect(providerConfig.factory()).toEqual({
        consumerKey: config.apiKey,
        consumerSecret: config.apiSecretKey,
        callbackURL: `${config.domainCurrent}/provider/twitter/callback`,
      });
    });
  });

  describe('#strategy', () => {
    let providerCallback: any;
    let providerConfig: any;
    beforeEach(() => {
      providerConfig = {
        id: 'some-id',
        factory: () => 'some-factory',
      };

      strategy.providerConfig = jest.fn().mockReturnValue(providerConfig);

      strategy.strategy();

      const [calls] = (<any>Strategy).mock.calls;

      expect(calls[0]).toEqual(providerConfig.factory());

      [, providerCallback] = calls;
    });

    it('should handle if empty profile data', (done) => {
      const token = 'some-access-token';
      const tokenSecret = 'some-refresh-token';
      const profile = undefined;

      providerCallback(token, tokenSecret, profile, (err, user) => {
        expect(err).toBeNull();

        expect(user).toEqual({
          emailAddress: null,
          type: providerConfig.id,
          tokens: {
            token,
            tokenSecret,
          },
          userId: undefined,
          name: undefined,
          username: undefined,
        });

        done();
      });
    });

    it('should handle if no profile data', (done) => {
      const token = 'some-access-token';
      const tokenSecret = 'some-refresh-token';
      const profile = {};

      providerCallback(token, tokenSecret, profile, (err, user) => {
        expect(err).toBeNull();

        expect(user).toEqual({
          emailAddress: null,
          type: providerConfig.id,
          tokens: {
            token,
            tokenSecret,
          },
          userId: undefined,
          name: undefined,
          username: undefined,
        });

        done();
      });
    });

    it('should handle if no profile email', (done) => {
      const token = 'some-access-token2';
      const tokenSecret = 'some-refresh-token2';
      const profile = {
        emails: [],
        id: 'some-id',
        displayName: 'some-name',
        username: 'some-username',
      };

      providerCallback(token, tokenSecret, profile, (err, user) => {
        expect(err).toBeNull();

        expect(user).toEqual({
          emailAddress: null,
          type: providerConfig.id,
          tokens: {
            token,
            tokenSecret,
          },
          userId: profile.id,
          name: profile.displayName,
          username: profile.username,
        });

        done();
      });
    });

    it('should handle if no profile email set', (done) => {
      const token = 'some-access-token2';
      const tokenSecret = 'some-refresh-token2';
      const profile = {
        emails: [{}],
        id: 'some-id',
        displayName: 'some-name',
        username: 'some-username',
      };

      providerCallback(token, tokenSecret, profile, (err, user) => {
        expect(err).toBeNull();

        expect(user).toEqual({
          emailAddress: null,
          type: providerConfig.id,
          tokens: {
            token,
            tokenSecret,
          },
          userId: profile.id,
          name: profile.displayName,
          username: profile.username,
        });

        done();
      });
    });

    it('should handle if profile email set', (done) => {
      const token = 'some-access-token2';
      const tokenSecret = 'some-refresh-token2';
      const profile = {
        emails: [
          {
            value: 'some-email',
          },
        ],
        id: 'some-id',
        displayName: 'some-name',
        username: 'some-username',
      };

      providerCallback(token, tokenSecret, profile, (err, user) => {
        expect(err).toBeNull();

        expect(user).toEqual({
          emailAddress: profile.emails[0].value,
          type: providerConfig.id,
          tokens: {
            token,
            tokenSecret,
          },
          userId: profile.id,
          name: profile.displayName,
          username: profile.username,
        });

        done();
      });
    });
  });
});
