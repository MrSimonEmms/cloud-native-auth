import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Strategy, StrategyOptions } from 'passport-google-oauth20';

import { IProvider, IProviderConfig, IProviderUserData } from '../interfaces';

@Injectable()
export default class GoogleStrategy implements IProvider<StrategyOptions> {
  @Inject(ConfigService)
  private readonly configService: ConfigService;

  enabled(): boolean {
    return this.configService.get<boolean>('providers.google.enabled');
  }

  providerConfig(): IProviderConfig<StrategyOptions> {
    const id = 'google';

    return {
      id,
      name: 'Google',
      factory: () => ({
        clientID: this.configService.get('providers.google.clientId'),
        clientSecret: this.configService.get('providers.google.clientSecret'),
        callbackURL: `${this.configService.get('domain.current')}/provider/${id}/callback`,
        scope: ['email', 'profile', ...this.configService.get('providers.google.additionalScopes')],
      }),
    };
  }

  strategy(): Strategy {
    const providerConfig = this.providerConfig();

    return new Strategy(
      providerConfig.factory(),
      (
        accessToken: string,
        refreshToken: string,
        profile: any,
        done: (err: any, user: IProviderUserData) => void,
      ) => {
        done(null, {
          emailAddress: profile?.emails?.[0]?.value ?? null,
          type: providerConfig.id,
          tokens: {
            accessToken,
            refreshToken,
          },
          userId: profile?.id,
          name: profile?.displayName,
          username: profile?.username,
        });
      },
    );
  }
}
