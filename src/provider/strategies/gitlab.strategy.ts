import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Strategy, StrategyOptions } from 'passport-gitlab2';

import { IProvider, IProviderConfig, IProviderUserData } from '../interfaces';

@Injectable()
export default class GitlaStrategy implements IProvider<StrategyOptions> {
  @Inject(ConfigService)
  private readonly configService: ConfigService;

  enabled(): boolean {
    return this.configService.get<boolean>('providers.gitlab.enabled');
  }

  providerConfig(): IProviderConfig<StrategyOptions> {
    const id = 'gitlab';

    return {
      id,
      name: 'GitLab',
      factory: () => ({
        clientID: this.configService.get('providers.gitlab.clientId'),
        clientSecret: this.configService.get('providers.gitlab.clientSecret'),
        callbackURL: `${this.configService.get('domain.current')}/provider/${id}/callback`,
        scope: ['api', ...this.configService.get('providers.gitlab.additionalScopes')],
      }),
    };
  }

  strategy(): Strategy {
    const providerConfig = this.providerConfig();

    return new Strategy(
      providerConfig.factory(),
      (
        accessToken: string,
        refreshToken: string,
        profile: any,
        done: (err: any, user: IProviderUserData) => void,
      ) => {
        done(null, {
          emailAddress: profile?.emails?.[0]?.value ?? null,
          type: providerConfig.id,
          tokens: {
            accessToken,
            refreshToken,
          },
          userId: profile?.id,
          name: profile?.displayName,
          username: profile?.username,
        });
      },
    );
  }
}
