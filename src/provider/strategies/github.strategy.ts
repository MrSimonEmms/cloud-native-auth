import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Strategy, StrategyOptions } from 'passport-github2';

import { IProvider, IProviderConfig, IProviderUserData } from '../interfaces';

@Injectable()
export default class GithubStrategy implements IProvider<StrategyOptions> {
  @Inject(ConfigService)
  private readonly configService: ConfigService;

  enabled(): boolean {
    return this.configService.get<boolean>('providers.github.enabled');
  }

  providerConfig(): IProviderConfig<StrategyOptions> {
    const id = 'github';

    return {
      id,
      name: 'GitHub',
      factory: () => ({
        clientID: this.configService.get('providers.github.clientId'),
        clientSecret: this.configService.get('providers.github.clientSecret'),
        callbackURL: `${this.configService.get('domain.current')}/provider/${id}/callback`,
        scope: [
          'read:user',
          'user:email',
          'repo',
          ...this.configService.get('providers.github.additionalScopes'),
        ],
      }),
    };
  }

  strategy(): Strategy {
    const providerConfig = this.providerConfig();

    return new Strategy(
      providerConfig.factory(),
      (
        accessToken: string,
        refreshToken: string,
        profile: any,
        done: (err: any, user: IProviderUserData) => void,
      ) => {
        done(null, {
          emailAddress: profile?.emails?.[0]?.value ?? null,
          type: providerConfig.id,
          tokens: {
            accessToken,
            refreshToken,
          },
          userId: profile?.id,
          name: profile?.displayName,
          username: profile?.username,
        });
      },
    );
  }
}
