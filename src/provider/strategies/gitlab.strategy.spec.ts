import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import { when } from 'jest-when';
import { Strategy } from 'passport-gitlab2';
import GitlabStrategy from './gitlab.strategy';

jest.mock('passport-gitlab2');

describe('GitLabStrategy', () => {
  let configService: any;
  let strategy: any;
  beforeEach(async () => {
    configService = {
      get: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GitlabStrategy,
        {
          provide: ConfigService,
          useValue: configService,
        },
      ],
    }).compile();

    strategy = module.get<GitlabStrategy>(GitlabStrategy);
  });

  describe('#enabled', () => {
    it('should return the enabled status', () => {
      const enabled = 'some-response';
      configService.get.mockReturnValue(enabled);

      expect(strategy.enabled()).toBe(enabled);

      expect(configService.get).toBeCalledWith('providers.gitlab.enabled');
    });
  });

  describe('#providerConfig', () => {
    it('should return the configuration', () => {
      const config = {
        clientId: 'some-client-id',
        clientSecret: 'some-client-secret',
        domainCurrent: 'some-domain',
        additionalScopes: ['some', 'additional', 'scope'],
      };

      when(configService.get)
        .calledWith('providers.gitlab.clientId')
        .mockReturnValue(config.clientId);
      when(configService.get)
        .calledWith('providers.gitlab.clientSecret')
        .mockReturnValue(config.clientSecret);
      when(configService.get).calledWith('domain.current').mockReturnValue(config.domainCurrent);
      when(configService.get)
        .calledWith('providers.gitlab.additionalScopes')
        .mockReturnValue(config.additionalScopes);

      const providerConfig = strategy.providerConfig();

      expect(providerConfig.id).toBe('gitlab');
      expect(providerConfig.name).toBe('GitLab');
      expect(providerConfig.factory()).toEqual({
        clientID: config.clientId,
        clientSecret: config.clientSecret,
        callbackURL: `${config.domainCurrent}/provider/gitlab/callback`,
        scope: ['api', ...config.additionalScopes],
      });
    });
  });

  describe('#strategy', () => {
    let providerCallback: any;
    let providerConfig: any;
    beforeEach(() => {
      providerConfig = {
        id: 'some-id',
        factory: () => 'some-factory',
      };

      strategy.providerConfig = jest.fn().mockReturnValue(providerConfig);

      strategy.strategy();

      const [calls] = (<any>Strategy).mock.calls;

      expect(calls[0]).toEqual(providerConfig.factory());

      [, providerCallback] = calls;
    });

    it('should handle if empty profile data', (done) => {
      const accessToken = 'some-access-token';
      const refreshToken = 'some-refresh-token';
      const profile = undefined;

      providerCallback(accessToken, refreshToken, profile, (err, user) => {
        expect(err).toBeNull();

        expect(user).toEqual({
          emailAddress: null,
          type: providerConfig.id,
          tokens: {
            accessToken,
            refreshToken,
          },
          userId: undefined,
          name: undefined,
          username: undefined,
        });

        done();
      });
    });

    it('should handle if no profile data', (done) => {
      const accessToken = 'some-access-token';
      const refreshToken = 'some-refresh-token';
      const profile = {};

      providerCallback(accessToken, refreshToken, profile, (err, user) => {
        expect(err).toBeNull();

        expect(user).toEqual({
          emailAddress: null,
          type: providerConfig.id,
          tokens: {
            accessToken,
            refreshToken,
          },
          userId: undefined,
          name: undefined,
          username: undefined,
        });

        done();
      });
    });

    it('should handle if no profile email', (done) => {
      const accessToken = 'some-access-token2';
      const refreshToken = 'some-refresh-token2';
      const profile = {
        emails: [],
        id: 'some-id',
        displayName: 'some-name',
        username: 'some-username',
      };

      providerCallback(accessToken, refreshToken, profile, (err, user) => {
        expect(err).toBeNull();

        expect(user).toEqual({
          emailAddress: null,
          type: providerConfig.id,
          tokens: {
            accessToken,
            refreshToken,
          },
          userId: profile.id,
          name: profile.displayName,
          username: profile.username,
        });

        done();
      });
    });

    it('should handle if no profile email set', (done) => {
      const accessToken = 'some-access-token2';
      const refreshToken = 'some-refresh-token2';
      const profile = {
        emails: [{}],
        id: 'some-id',
        displayName: 'some-name',
        username: 'some-username',
      };

      providerCallback(accessToken, refreshToken, profile, (err, user) => {
        expect(err).toBeNull();

        expect(user).toEqual({
          emailAddress: null,
          type: providerConfig.id,
          tokens: {
            accessToken,
            refreshToken,
          },
          userId: profile.id,
          name: profile.displayName,
          username: profile.username,
        });

        done();
      });
    });

    it('should handle if profile email set', (done) => {
      const accessToken = 'some-access-token2';
      const refreshToken = 'some-refresh-token2';
      const profile = {
        emails: [
          {
            value: 'some-email',
          },
        ],
        id: 'some-id',
        displayName: 'some-name',
        username: 'some-username',
      };

      providerCallback(accessToken, refreshToken, profile, (err, user) => {
        expect(err).toBeNull();

        expect(user).toEqual({
          emailAddress: profile.emails[0].value,
          type: providerConfig.id,
          tokens: {
            accessToken,
            refreshToken,
          },
          userId: profile.id,
          name: profile.displayName,
          username: profile.username,
        });

        done();
      });
    });
  });
});
