import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Strategy, IStrategyOption } from 'passport-twitter';

import { IProvider, IProviderConfig, IProviderUserData } from '../interfaces';

@Injectable()
export default class TwitterStrategy implements IProvider<IStrategyOption> {
  @Inject(ConfigService)
  private readonly configService: ConfigService;

  enabled(): boolean {
    return this.configService.get<boolean>('providers.twitter.enabled');
  }

  providerConfig(): IProviderConfig<IStrategyOption> {
    const id = 'twitter';

    return {
      id,
      name: 'Twitter',
      factory: () => ({
        consumerKey: this.configService.get('providers.twitter.apiKey'),
        consumerSecret: this.configService.get('providers.twitter.apiSecretKey'),
        callbackURL: `${this.configService.get('domain.current')}/provider/${id}/callback`,
      }),
    };
  }

  strategy(): Strategy {
    const providerConfig = this.providerConfig();

    return new Strategy(
      providerConfig.factory(),
      (
        token: string,
        tokenSecret: string,
        profile: any,
        done: (err: any, user: IProviderUserData) => void,
      ) => {
        done(null, {
          emailAddress: profile?.emails?.[0]?.value ?? null,
          type: providerConfig.id,
          tokens: {
            token,
            tokenSecret,
          },
          userId: profile?.id,
          name: profile?.displayName,
          username: profile?.username,
        });
      },
    );
  }
}
