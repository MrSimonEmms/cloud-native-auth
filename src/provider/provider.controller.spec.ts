/* eslint-disable import/first */

jest.mock('passport');

import * as qs from 'querystring';
import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import { METHOD_METADATA, PATH_METADATA, REDIRECT_METADATA } from '@nestjs/common/constants';
import { BadRequestException, RequestMethod } from '@nestjs/common';
import * as passport from 'passport';
import { when } from 'jest-when';

import ProviderController from './provider.controller';
import UserService from '../user/user.service';

describe('ProviderController', () => {
  let controller: ProviderController;
  let configService: any;
  let providerList: any;
  let findProvider: any;
  let userService: any;

  beforeEach(async () => {
    configService = {
      get: jest.fn(),
    };
    providerList = [];
    findProvider = jest.fn();
    userService = {
      addProviderToUser: jest.fn(),
      getPublicLoginQueryStrings: jest.fn(),
      removeProvider: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProviderController],
      providers: [
        {
          provide: ConfigService,
          useValue: configService,
        },
        {
          provide: 'PROVIDER_LIST',
          useValue: providerList,
        },
        {
          provide: 'PROVIDER_FINDER',
          useValue: findProvider,
        },
        {
          provide: UserService,
          useValue: userService,
        },
      ],
    }).compile();

    controller = module.get<ProviderController>(ProviderController);
  });

  describe('#list', () => {
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.list)).toEqual('/');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.list)).toEqual(RequestMethod.GET);
    });

    it('should return the list of providers', () => {
      providerList.push(
        ...[
          {
            providerConfig: () => 'item1',
          },
          {
            providerConfig: () => 'item2',
          },
        ],
      );
      expect(controller.list()).toEqual(providerList.map((item) => item.providerConfig()));
    });
  });

  describe('#providerLogin', () => {
    let log: any;
    let req: any;
    let res: any;
    let next: any;
    let passportAuthClosure: any;
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.providerLogin)).toEqual('/:id');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.providerLogin)).toEqual(
        RequestMethod.GET,
      );

      log = {
        info: jest.fn(),
        warn: jest.fn(),
      };

      req = {
        log: {
          child: jest.fn().mockReturnValue(log),
        },
      };
      res = {
        clearCookie: jest.fn(),
        cookie: jest.fn(),
      };
      next = jest.fn();
      passportAuthClosure = jest.fn();

      (<any>passport.authenticate).mockReturnValue(passportAuthClosure);
    });

    it('should throw error if user already registered with provider', () => {
      const providerId = 'some-provider-id3';
      const strategyValue = 'some-strategy3';
      findProvider.mockReturnValue({
        strategy: () => strategyValue,
      });
      const userId = 'some-user-id3';
      const user = {
        get: jest.fn().mockReturnValue(userId),
        getProvider: jest.fn().mockReturnValue('some-response'),
      };
      const cookieSecure = true;
      when(configService.get).calledWith('cookies.secure').mockReturnValue(cookieSecure);

      try {
        controller.providerLogin(providerId, user, req, res, next);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(
          new BadRequestException({
            message: 'Provider already registered',
            provider: providerId,
          }),
        );
      }

      expect(passport.authenticate).not.toBeCalled();
      expect(passportAuthClosure).not.toBeCalled();

      expect(res.cookie).not.toBeCalled();

      expect(user.getProvider).toBeCalledWith(providerId);
    });

    it('should store the user data in a cookie if user set', () => {
      const providerId = 'some-provider-id';
      const strategyValue = 'some-strategy';
      findProvider.mockReturnValue({
        strategy: () => strategyValue,
      });
      const userId = 'some-user-id';
      const user = {
        get: jest.fn().mockReturnValue(userId),
        getProvider: jest.fn().mockReturnValue(undefined),
      };
      const cookieSecure = true;
      when(configService.get).calledWith('cookies.secure').mockReturnValue(cookieSecure);

      controller.providerLogin(providerId, user, req, res, next);

      expect(passport.authenticate).toBeCalledWith(strategyValue, { session: false });
      expect(passportAuthClosure).toBeCalledWith(req, res, next);

      expect(res.cookie).toBeCalledWith('loginUserId', userId, {
        httpOnly: true,
        secure: cookieSecure,
        signed: true,
      });

      expect(user.getProvider).toBeCalledWith(providerId);
    });

    it('should clear cookie if user not set', () => {
      const providerId = 'some-provider-id2';
      const strategyValue = 'some-strategy2';
      findProvider.mockReturnValue({
        strategy: () => strategyValue,
      });

      controller.providerLogin(providerId, undefined, req, res, next);

      expect(res.clearCookie).toBeCalledWith('loginUserId');

      expect(passport.authenticate).toBeCalledWith(strategyValue, { session: false });
      expect(passportAuthClosure).toBeCalledWith(req, res, next);
    });
  });

  describe('#deleteProvider', () => {
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.deleteProvider)).toEqual('/:id');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.deleteProvider)).toEqual(
        RequestMethod.DELETE,
      );
    });

    it('should call the removeProvider method on the userService', async () => {
      const providerId = 'provider-id';
      const provider = 'some-provider';
      const result = 'some-result';
      const user = 'some-user';
      findProvider.mockReturnValue(provider);
      userService.removeProvider.mockResolvedValue(result);

      expect(await controller.deleteProvider(providerId, user)).toEqual(result);

      expect(findProvider).toBeCalledWith(providerId);
      expect(userService.removeProvider).toBeCalledWith(user, providerId);
    });
  });

  describe('#providerCallback', () => {
    let req: any;
    let res: any;
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.providerCallback)).toEqual(
        '/:id/callback',
      );
      expect(Reflect.getMetadata(METHOD_METADATA, controller.providerCallback)).toEqual(
        RequestMethod.GET,
      );
      expect(Reflect.getMetadata(REDIRECT_METADATA, controller.providerCallback)).toEqual({
        statusCode: undefined,
        url: '',
      });

      req = {
        log: {
          error: jest.fn(),
          info: jest.fn(),
        },
        signedCookies: {},
      };
      res = {
        clearCookie: jest.fn(),
      };
    });

    it('should redirect an error', async () => {
      const providerUser = 'providerUser';
      const err = 'some-error';
      userService.addProviderToUser.mockRejectedValue({
        response: {
          code: err,
        },
      });
      const domain = 'my-domain';

      when(configService.get).calledWith('domain.login').mockReturnValue(domain);

      expect(await controller.providerCallback(providerUser, req, res)).toEqual({
        statusCode: 302,
        url: `${domain}?${qs.stringify({
          error: err,
        })}`,
      });

      expect(userService.addProviderToUser).toBeCalledWith(providerUser, undefined);
      expect(userService.getPublicLoginQueryStrings).not.toBeCalled();

      expect(res.clearCookie).toBeCalledWith('loginUserId');
    });

    it('should redirect an error message', async () => {
      const providerUser = 'providerUser';
      const err = 'some-error2';
      userService.addProviderToUser.mockRejectedValue({
        message: err,
      });
      const domain = 'my-domain2';

      when(configService.get).calledWith('domain.login').mockReturnValue(domain);

      expect(await controller.providerCallback(providerUser, req, res)).toEqual({
        statusCode: 302,
        url: `${domain}?${qs.stringify({
          error: err,
        })}`,
      });

      expect(userService.addProviderToUser).toBeCalledWith(providerUser, undefined);
      expect(userService.getPublicLoginQueryStrings).not.toBeCalled();

      expect(res.clearCookie).toBeCalledWith('loginUserId');
    });

    it('should redirect a successful login', async () => {
      const providerUser = 'providerUser';
      const user = 'some-user';
      const domain = 'my-domain';
      const token = { name: 'some-token' };

      userService.addProviderToUser.mockResolvedValue(user);
      userService.getPublicLoginQueryStrings.mockReturnValue(token);

      when(configService.get).calledWith('domain.login').mockReturnValue(domain);

      expect(await controller.providerCallback(providerUser, req, res)).toEqual({
        statusCode: 302,
        url: `${domain}?${qs.stringify(token)}`,
      });

      expect(userService.addProviderToUser).toBeCalledWith(providerUser, undefined);
      expect(userService.getPublicLoginQueryStrings).toBeCalledWith(user);

      expect(res.clearCookie).toBeCalledWith('loginUserId');
    });

    it('should redirect a successful login with a known user in the cookie', async () => {
      const providerUser = 'providerUser';
      const user = 'some-user';
      const domain = 'my-domain';
      const token = { name: 'some-token' };
      const linkedUser = 'linked-user';

      req.signedCookies.loginUserId = linkedUser;
      userService.addProviderToUser.mockResolvedValue(user);
      userService.getPublicLoginQueryStrings.mockReturnValue(token);

      when(configService.get).calledWith('domain.login').mockReturnValue(domain);

      expect(await controller.providerCallback(providerUser, req, res)).toEqual({
        statusCode: 302,
        url: `${domain}?${qs.stringify(token)}`,
      });

      expect(userService.addProviderToUser).toBeCalledWith(providerUser, linkedUser);
      expect(userService.getPublicLoginQueryStrings).toBeCalledWith(user);

      expect(res.clearCookie).toBeCalledWith('loginUserId');
    });
  });
});
