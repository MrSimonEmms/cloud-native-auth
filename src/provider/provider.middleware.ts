import { Inject, Injectable, NestMiddleware, UnauthorizedException } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import * as passport from 'passport';
import { ConfigService } from '@nestjs/config';

@Injectable()
export default class ProviderMiddleware implements NestMiddleware {
  constructor(
    @Inject('PROVIDER_FINDER') private readonly findProvider,
    @Inject(ConfigService) private readonly config: ConfigService,
  ) {}

  use(req: Request, res: Response, next: NextFunction): void {
    const providerId = req.params.id;
    const provider = this.findProvider(providerId);

    req.log.debug('Initializing PassportJS');

    passport.initialize()(req, res, () => {
      req.log.debug({ provider: providerId }, 'Authenticating user against provider');

      passport.authenticate(provider.strategy(), {
        session: false,
      })(req, res, (err) => {
        if (err) {
          req.log.error({ err, provider: providerId }, 'Provider login failed');

          next(new UnauthorizedException());
          return;
        }

        next();
      });
    });
  }
}
