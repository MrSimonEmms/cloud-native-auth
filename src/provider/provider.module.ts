import { MiddlewareConsumer, Module, NestModule, NotFoundException } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';

import GithubStrategy from './strategies/github.strategy';
import GitlaStrategy from './strategies/gitlab.strategy';
import GoogleStrategy from './strategies/google.strategy';
import TwitterStrategy from './strategies/twitter.strategy';
import ProviderController from './provider.controller';
import ProviderMiddleware from './provider.middleware';
import UserModule from '../user/user.module';

/* List of providers */
const providerList = [GithubStrategy, GitlaStrategy, GoogleStrategy, TwitterStrategy];

@Module({
  imports: [PassportModule, UserModule],
  providers: [
    ...providerList,
    {
      provide: 'PROVIDER_LIST',
      inject: providerList,
      useFactory: (...providers) => providers.filter((item) => item.enabled()),
    },
    {
      provide: 'PROVIDER_FINDER',
      inject: ['PROVIDER_LIST'],
      useFactory: (providers) => (providerId: string) => {
        const provider = providers.find((item) => item.providerConfig().id === providerId);

        if (!provider) {
          throw new NotFoundException('UNKNOWN_PROVIDER');
        }

        return provider;
      },
    },
  ],
  controllers: [ProviderController],
})
export default class ProviderModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ProviderMiddleware).forRoutes('/provider/:id/callback');
  }
}
