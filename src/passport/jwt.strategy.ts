import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import UserService from '../user/user.service';
import { UserDocument } from '../user/user.schema';

@Injectable()
export default class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(config: ConfigService, private readonly userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromAuthHeaderAsBearerToken(),
        ExtractJwt.fromUrlQueryParameter('_auth'),
      ]),
      issuer: config.get('jwt.issuer'),
      secretOrKey: config.get('jwt.secret'),
    });
  }

  async validate({ id }: { id: string }): Promise<UserDocument | void> {
    return this.userService.findById(id);
  }
}
