import { Module } from '@nestjs/common';

import JwtStrategy from './jwt.strategy';
import UserModule from '../user/user.module';
import AnonymousStrategy from './anonymous.strategy';

@Module({
  imports: [UserModule],
  providers: [AnonymousStrategy, JwtStrategy],
})
export default class PassportModule {}
