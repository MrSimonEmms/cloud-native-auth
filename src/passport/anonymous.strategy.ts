/**
 * Anonymous Strategy
 *
 * This strategy allows for endpoints to have optional authentication. It
 * will act as a pass-through and will not set the `req.user` parameter
 */

import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-anonymous';

@Injectable()
export default class AnonymousStrategy extends PassportStrategy(Strategy) {}
