import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import { when } from 'jest-when';

import JwtStrategy from './jwt.strategy';
import UserService from '../user/user.service';

describe('JWTStrategy', () => {
  let configService: any;
  let userService: any;
  let strategy: any;
  beforeEach(async () => {
    configService = {
      get: jest.fn(),
    };
    userService = {
      findById: jest.fn(),
    };

    const jwt = {
      issuer: 'some-issuer',
      secret: 'some-secret',
    };

    when(configService.get).calledWith('jwt.issuer').mockReturnValue(jwt.issuer);
    when(configService.get).calledWith('jwt.secret').mockReturnValue(jwt.secret);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JwtStrategy,
        {
          provide: ConfigService,
          useValue: configService,
        },
        {
          provide: UserService,
          useValue: userService,
        },
      ],
    }).compile();

    strategy = module.get<JwtStrategy>(JwtStrategy);
  });

  describe('#validate', () => {
    it('should find the user from the userService', async () => {
      const user = { id: 'some-user' };
      const response = 'some-response';
      userService.findById.mockResolvedValue(response);

      expect(await strategy.validate(user)).toBe(response);

      expect(userService.findById).toBeCalledWith(user.id);
    });
  });
});
