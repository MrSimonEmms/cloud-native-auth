import AnonymousStrategy from './anonymous.strategy';

describe('AnonymousStrategy', () => {
  it('should implement the anonymous strategy', () => {
    const strategy = new AnonymousStrategy();

    expect(strategy.name).toBe('anonymous');
  });
});
