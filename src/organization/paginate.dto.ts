import { ApiProperty } from '@nestjs/swagger';
import { IPaginate } from './interfaces';

export default class PaginateDTO<T> implements IPaginate<T> {
  data: T[];

  @ApiProperty({
    example: 25,
    description: 'Records displayed',
  })
  count: number;

  @ApiProperty({
    example: 25,
    description: 'Number of results per page',
  })
  limit: number;

  @ApiProperty({
    example: 1,
    description: 'Current page',
  })
  page: number;

  @ApiProperty({
    example: 3,
    description: 'Total number of pages',
  })
  pages: number;

  @ApiProperty({
    example: 67,
    description: 'Total number of records',
  })
  total: number;
}
