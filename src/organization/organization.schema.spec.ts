import { SchemaFactory } from '@nestjs/mongoose';
import * as uuid from 'uuid';
import { TypeMetadataStorage } from '@nestjs/mongoose/dist/storages/type-metadata.storage';
import { Organization, OrganizationSchema } from './organization.schema';
import { OrganizationUserSchema } from './organizationUser.schema';
import { OrgRoles } from './interfaces';
import { User } from '../user/user.schema';

jest.mock('uuid');
jest.mock('@nestjs/mongoose/dist/factories/schema.factory', () => ({
  SchemaFactory: {
    createForClass: jest.fn().mockReturnValue({
      pre: jest.fn(),
      post: jest.fn(),
      index: jest.fn(),
      methods: {},
      set: jest.fn(),
    }),
  },
}));

function getProp(key: string) {
  return TypeMetadataStorage.getSchemaMetadataByTarget(Organization).properties.find(
    ({ propertyKey }) => propertyKey === key,
  );
}

describe('OrganizationSchema', () => {
  beforeEach(() => {
    expect(SchemaFactory.createForClass).toBeCalledWith(Organization);
  });

  describe('props', () => {
    describe('_id', () => {
      let prop;
      beforeEach(() => {
        prop = getProp('_id');
      });

      it('should default to a uuid', () => {
        expect(prop.options).toEqual(
          expect.objectContaining({
            type: String,
          }),
        );

        const uuidValue = 'some-uuid';
        (<any>uuid).v4.mockReturnValue(uuidValue);
        expect(prop.options.default()).toBe(uuidValue);
      });
    });

    describe('name', () => {
      it('should configure the property', () => {
        expect(getProp('name').options).toEqual(
          expect.objectContaining({
            type: String,
            required: true,
            maxLength: 200,
          }),
        );
      });
    });

    describe('slug', () => {
      let prop;
      beforeEach(() => {
        prop = getProp('slug');
      });

      it('should configure the property', () => {
        expect(prop.options).toEqual(
          expect.objectContaining({
            type: String,
            required: true,
            minLength: 3,
            maxLength: 20,
            lowercase: true,
            index: true,
          }),
        );
      });

      describe('validate', () => {
        let validate;
        beforeEach(() => {
          validate = prop.options.validate;
        });

        it('should error if slug is not alphanumeric', async () => {
          try {
            await validate('%43');
            throw new Error('invalid');
          } catch (err) {
            expect(err).toEqual(new Error('Slug must be alphanumeric'));
          }
        });

        it('should error if slug already registered', async () => {
          const id = 'some-id';
          const context = {
            model: jest.fn().mockReturnThis(),
            findOne: jest.fn().mockReturnThis(),
            get: jest.fn().mockReturnValue(id),
            where: jest.fn().mockReturnThis(),
            nin: jest.fn().mockResolvedValue('some-org'),
          };
          const slug = 'some-slug';

          try {
            await validate.call(context, slug);
            throw new Error('invalid');
          } catch (err) {
            expect(err).toEqual(new Error('Slug already registered'));

            expect(context.model).toBeCalledWith(Organization.name);
            expect(context.findOne).toBeCalledWith({ slug });
            expect(context.where).toBeCalledWith('_id');
            expect(context.nin).toBeCalledWith([id]);
          }
        });

        it('should return true if valid', async () => {
          const id = 'some-id2';
          const context = {
            model: jest.fn().mockReturnThis(),
            findOne: jest.fn().mockReturnThis(),
            get: jest.fn().mockReturnValue(id),
            where: jest.fn().mockReturnThis(),
            nin: jest.fn().mockResolvedValue(null),
          };
          const slug = 'some-slug2';

          expect(await validate.call(context, slug)).toBe(true);

          expect(context.model).toBeCalledWith(Organization.name);
          expect(context.findOne).toBeCalledWith({ slug });
          expect(context.where).toBeCalledWith('_id');
          expect(context.nin).toBeCalledWith([id]);
        });
      });
    });

    describe('users', () => {
      let prop;
      beforeEach(() => {
        prop = getProp('users');
      });

      it('should configure the property', () => {
        expect(prop.options).toEqual(
          expect.objectContaining({
            type: [OrganizationUserSchema],
            required: true,
          }),
        );
      });

      describe('validate', () => {
        let validate;
        beforeEach(() => {
          validate = prop.options.validate;
        });

        it('should error if length is 0', async () => {
          try {
            await validate([]);
            throw new Error('invalid');
          } catch (err) {
            expect(err).toEqual(new Error('Organization must have at least one user'));
          }
        });

        it('should error if has no maintainer', async () => {
          try {
            await validate([
              {
                role: OrgRoles.ORG_USER,
              },
            ]);
            throw new Error('invalid');
          } catch (err) {
            expect(err).toEqual(new Error('Organization must have at least one maintainer'));
          }
        });

        it('should error if user not in database', async () => {
          const context = {
            model: jest.fn().mockReturnThis(),
            findById: jest.fn().mockResolvedValue(null),
          };

          const input = [
            {
              role: OrgRoles.ORG_MAINTAINER,
              userId: 'invalid',
            },
          ];

          try {
            await validate.call(context, input);
            throw new Error('invalid');
          } catch (err) {
            expect(err).toEqual(new Error(`Invalid user: ${input[0].userId}`));

            expect(context.model).toBeCalledWith(User.name);
            expect(context.findById).toBeCalledWith(input[0].userId);
          }
        });

        it('should error if duplicate users', async () => {
          const context = {
            model: jest.fn().mockReturnThis(),
            findById: jest.fn().mockResolvedValue('some-user'),
          };

          const input = [
            {
              role: OrgRoles.ORG_MAINTAINER,
              userId: 'user1',
            },
            {
              role: OrgRoles.ORG_USER,
              userId: 'user1',
            },
          ];

          try {
            await validate.call(context, input);
            throw new Error('invalid');
          } catch (err) {
            expect(err).toEqual(new Error('Duplicate users in list'));

            expect(context.model).toBeCalledWith(User.name);
            input.forEach(({ userId }) => {
              expect(context.findById).toBeCalledWith(userId);
            });
          }
        });

        it('should return true if valid', async () => {
          const context = {
            model: jest.fn().mockReturnThis(),
            findById: jest.fn().mockResolvedValue('some-user'),
          };

          const input = [
            {
              role: OrgRoles.ORG_MAINTAINER,
              userId: 'user1',
            },
            {
              role: OrgRoles.ORG_USER,
              userId: 'user2',
            },
          ];

          expect(await validate.call(context, input)).toBe(true);

          expect(context.model).toBeCalledWith(User.name);
          input.forEach(({ userId }) => {
            expect(context.findById).toBeCalledWith(userId);
          });
        });
      });
    });

    describe('createdAt', () => {
      it('should configure the property', () => {
        expect(getProp('createdAt').options).toEqual(
          expect.objectContaining({
            type: Date,
            index: true,
          }),
        );
      });
    });

    describe('updatedAt', () => {
      it('should configure the property', () => {
        expect(getProp('updatedAt').options).toEqual(
          expect.objectContaining({
            type: Date,
          }),
        );
      });
    });
  });

  describe('indexes', () => {
    it('should configure index on _id, users.userId and users.role', () => {
      expect(OrganizationSchema.index).toBeCalledWith({
        _id: 1,
        'users.userId': 1,
        'users.role': 1,
      });
    });
  });

  describe('methods', () => {
    describe('#removeUser', () => {
      it('should remove a user from the users array', () => {
        const users = [
          {
            userId: 'invalid1',
          },
          {
            userId: 'valid1',
          },
          {
            userId: 'invalid2',
          },
        ];

        const context = {
          get: jest.fn().mockReturnValue(users),
          set: jest.fn(),
        };

        const userId = 'valid1';

        const newUsers = users.filter((user) => user.userId !== userId);
        expect(newUsers).toHaveLength(users.length - 1);

        expect(OrganizationSchema.methods.removeUser.call(context, userId)).toBe(context);

        expect(context.set).toBeCalledWith('users', newUsers);
      });

      it('should do nothing if user not set', () => {
        const users = [
          {
            userId: 'invalid1',
          },
          {
            userId: 'invalid3',
          },
          {
            userId: 'invalid2',
          },
        ];

        const context = {
          get: jest.fn().mockReturnValue(users),
          set: jest.fn(),
        };

        const userId = 'valid2';

        expect(OrganizationSchema.methods.removeUser.call(context, userId)).toBe(context);

        expect(context.set).toBeCalledWith('users', users);
      });
    });
  });

  describe('#toJSON', () => {
    it('should remove _id from the output', () => {
      expect(OrganizationSchema.set).toBeCalledWith(
        'toJSON',
        expect.objectContaining({
          virtuals: true,
          versionKey: false,
        }),
      );

      const [, { transform }] = (<any>OrganizationSchema).set.mock.calls.find(
        ([key]) => key === 'toJSON',
      );

      const doc = 'some-doc';
      const ret = { hello: 'world', _id: 'hello' };
      expect(transform(doc, ret)).toEqual({
        ...ret,
        _id: undefined,
      });
    });
  });

  describe('#toObject', () => {
    it('should remove _id from the output', () => {
      expect(OrganizationSchema.set).toBeCalledWith(
        'toObject',
        expect.objectContaining({
          virtuals: true,
          versionKey: false,
        }),
      );

      const [, { transform }] = (<any>OrganizationSchema).set.mock.calls.find(
        ([key]) => key === 'toObject',
      );

      const doc = 'some-doc';
      const ret = { hello: 'world', _id: 'hello' };
      expect(transform(doc, ret)).toEqual({
        ...ret,
        _id: undefined,
      });
    });
  });
});
