import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import OrganizationController from './organization.controller';
import { Organization, OrganizationSchema } from './organization.schema';
import OrganizationService from './organization.service';
// eslint-disable-next-line import/no-cycle
import UserModule from '../user/user.module';
// eslint-disable-next-line import/no-cycle
import RolesModule from '../roles/roles.module';
import RequestParserModule from '../request-parser/request-parser.module';

@Module({
  imports: [
    forwardRef(() => RolesModule),
    RequestParserModule,
    forwardRef(() => UserModule),
    MongooseModule.forFeatureAsync([
      {
        name: Organization.name,
        useFactory: async () => {
          const schema = OrganizationSchema;
          schema.plugin(await import('mongoose-paginate-v2'));
          return schema;
        },
      },
    ]),
  ],
  controllers: [OrganizationController],
  providers: [OrganizationService],
  exports: [OrganizationService],
})
export default class OrganizationModule {}
