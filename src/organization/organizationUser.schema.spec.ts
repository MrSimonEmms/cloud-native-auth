import { TypeMetadataStorage } from '@nestjs/mongoose/dist/storages/type-metadata.storage';
import { SchemaFactory } from '@nestjs/mongoose';
import { OrganizationUser, OrganizationUserSchema } from './organizationUser.schema';
import { OrgRoles } from './interfaces';

jest.mock('@nestjs/mongoose/dist/factories/schema.factory', () => ({
  SchemaFactory: {
    createForClass: jest.fn().mockReturnValue({
      index: jest.fn(),
    }),
  },
}));

function getProp(key: string) {
  return TypeMetadataStorage.getSchemaMetadataByTarget(OrganizationUser).properties.find(
    ({ propertyKey }) => propertyKey === key,
  );
}

describe('OrganizationUserSchema', () => {
  beforeEach(() => {
    expect(SchemaFactory.createForClass).toBeCalledWith(OrganizationUser);
  });

  describe('props', () => {
    describe('name', () => {
      let prop;
      beforeEach(() => {
        prop = getProp('name');
      });

      it('should configure the property', () => {
        expect(prop.options).toEqual(
          expect.objectContaining({
            type: String,
          }),
        );
      });

      it('should error if not undefined', () => {
        try {
          prop.options.validate('hello');
          throw new Error('invalid');
        } catch (err) {
          expect(err).toEqual(new Error('Cannot set name'));
        }
      });

      it('should return void if undefined', () => {
        expect(prop.options.validate()).toBeUndefined();
      });
    });

    describe('username', () => {
      let prop;
      beforeEach(() => {
        prop = getProp('username');
      });

      it('should configure the property', () => {
        expect(prop.options).toEqual(
          expect.objectContaining({
            type: String,
          }),
        );
      });

      it('should error if not undefined', () => {
        try {
          prop.options.validate('hello');
          throw new Error('invalid');
        } catch (err) {
          expect(err).toEqual(new Error('Cannot set username'));
        }
      });

      it('should return void if undefined', () => {
        expect(prop.options.validate()).toBeUndefined();
      });
    });

    describe('role', () => {
      it('should configure the property', () => {
        expect(getProp('role').options).toEqual(
          expect.objectContaining({
            type: String,
            required: true,
            index: true,
            enum: Object.values(OrgRoles),
          }),
        );
      });
    });

    describe('userId', () => {
      it('should configure the property', () => {
        expect(getProp('userId').options).toEqual(
          expect.objectContaining({
            type: String,
            required: true,
            index: true,
          }),
        );
      });
    });

    describe('createdAt', () => {
      it('should configure the property', () => {
        expect(getProp('createdAt').options).toEqual(
          expect.objectContaining({
            type: Date,
          }),
        );
      });
    });

    describe('updatedAt', () => {
      it('should configure the property', () => {
        expect(getProp('updatedAt').options).toEqual(
          expect.objectContaining({
            type: Date,
          }),
        );
      });
    });
  });

  describe('indexes', () => {
    it('should configure index on userId and role', () => {
      expect(OrganizationUserSchema.index).toBeCalledWith({
        userId: 1,
        role: 1,
      });
    });
  });
});
