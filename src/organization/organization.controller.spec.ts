import { Test, TestingModule } from '@nestjs/testing';
import { METHOD_METADATA, PATH_METADATA } from '@nestjs/common/constants';
import { RequestMethod } from '@nestjs/common';

import OrganizationController from './organization.controller';
import OrganizationService from './organization.service';
import RolesService from '../roles/roles.service';
import { OrgSubjects } from './interfaces';

jest.mock('@nestjs/config');

describe('OrganizationController', () => {
  let controller: OrganizationController;
  let orgService: any;
  let rolesService: any;

  beforeEach(async () => {
    orgService = {
      create: jest.fn(),
      deleteById: jest.fn(),
      findById: jest.fn(),
      paginate: jest.fn(),
      updateById: jest.fn(),
    };
    rolesService = {};

    const module: TestingModule = await Test.createTestingModule({
      controllers: [OrganizationController],
      providers: [
        { provide: OrganizationService, useValue: orgService },
        { provide: RolesService, useValue: rolesService },
      ],
    }).compile();

    controller = module.get<OrganizationController>(OrganizationController);
  });

  describe('#getMany', () => {
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.getMany)).toEqual('/');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.getMany)).toEqual(RequestMethod.GET);
      expect(Reflect.getMetadata('roles', controller.getMany)).toEqual([
        {
          resource: OrgSubjects.Organization,
          action: 'read',
          possession: 'own',
        },
      ]);
    });

    it('should return the organization service paginate', async () => {
      const user = 'some-user';
      const query = 'some-query';
      const page = 1234;
      const limit = 2468;
      const result = 'some-result';

      orgService.paginate.mockResolvedValue(result);

      expect(await controller.getMany(user, query, page, limit)).toBe(result);

      expect(orgService.paginate).toBeCalledWith(query, page, limit, user);
    });
  });

  describe('#create', () => {
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.create)).toEqual('/');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.create)).toEqual(RequestMethod.POST);
      expect(Reflect.getMetadata('roles', controller.create)).toEqual([
        {
          resource: OrgSubjects.Organization,
          action: 'create',
          possession: 'own',
        },
      ]);
    });

    it('should return the organization service create', async () => {
      const user = 'some-user';
      const body: any = 'some-body';
      const result = 'some-result';

      orgService.create.mockResolvedValue(result);

      expect(await controller.create(user, body)).toBe(result);

      expect(orgService.create).toBeCalledWith(user, body);
    });
  });

  describe('#getOne', () => {
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.getOne)).toEqual('/:id');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.getOne)).toEqual(RequestMethod.GET);
      expect(Reflect.getMetadata('roles', controller.getOne)).toEqual([
        {
          resource: OrgSubjects.Organization,
          action: 'read',
          possession: 'own',
        },
      ]);
    });

    it('should return the organization service findById', async () => {
      const user = 'some-user';
      const id = 'some-id';
      const result = 'some-result';

      orgService.findById.mockResolvedValue(result);

      expect(await controller.getOne(user, id)).toBe(result);

      expect(orgService.findById).toBeCalledWith(id, true, user);
    });
  });

  describe('#deleteOne', () => {
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.deleteOne)).toEqual('/:id');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.deleteOne)).toEqual(
        RequestMethod.DELETE,
      );
      expect(Reflect.getMetadata('roles', controller.deleteOne)).toEqual([
        {
          resource: OrgSubjects.Organization,
          action: 'delete',
          possession: 'own',
        },
      ]);
    });

    it('should return the organization service deleteById', async () => {
      const user = 'some-user';
      const id = 'some-id';
      const result = 'some-result';

      orgService.deleteById.mockResolvedValue(result);

      expect(await controller.deleteOne(user, id)).toBe(result);

      expect(orgService.deleteById).toBeCalledWith(user, id);
    });
  });

  describe('#updateOne', () => {
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.updateOne)).toEqual('/:id');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.updateOne)).toEqual(
        RequestMethod.PATCH,
      );
      expect(Reflect.getMetadata('roles', controller.updateOne)).toEqual([
        {
          resource: OrgSubjects.Organization,
          action: 'update',
          possession: 'own',
        },
      ]);
    });

    it('should return the organization service updateById', async () => {
      const user = 'some-user';
      const id = 'some-id';
      const body: any = 'some-body';
      const result = 'some-result';

      orgService.updateById.mockResolvedValue(result);

      expect(await controller.updateOne(user, id, body)).toBe(result);

      expect(orgService.updateById).toBeCalledWith(user, id, body);
    });
  });
});
