import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { PinoLogger } from 'nestjs-pino';
import { ConfigService } from '@nestjs/config';
import { when } from 'jest-when';
import { BadRequestException, ForbiddenException, NotFoundException } from '@nestjs/common';

import OrganizationService from './organization.service';
import { Organization } from './organization.schema';
import UserService from '../user/user.service';
import { OrgRoles } from './interfaces';
import RequestParserService from '../request-parser/request-parser.service';

jest.mock('nestjs-pino');
jest.mock('@nestjs/config');

describe('OrganizationService', () => {
  let service: OrganizationService;
  let OrgModel: any;
  let userService: any;
  let configService: any;
  let requestParserService: any;

  beforeEach(async () => {
    OrgModel = jest.fn();
    OrgModel.find = jest.fn();
    OrgModel.findOne = jest.fn();
    OrgModel.findOneAndDelete = jest.fn();
    OrgModel.paginate = jest.fn();
    userService = {
      findById: jest.fn(),
    };
    configService = { get: jest.fn() };
    requestParserService = { parse: jest.fn() };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OrganizationService,
        {
          provide: getModelToken(Organization.name),
          useValue: OrgModel,
        },
        {
          provide: PinoLogger,
          useClass: PinoLogger,
        },
        {
          provide: ConfigService,
          useValue: configService,
        },
        {
          provide: RequestParserService,
          useValue: requestParserService,
        },
        {
          provide: UserService,
          useValue: userService,
        },
      ],
    }).compile();

    service = module.get<OrganizationService>(OrganizationService);
  });

  describe('#populateUser', () => {
    it('should throw an error if the user not found', async () => {
      const users = [[9]];
      const unique = Array.from(new Set(users.flat()));

      const userObjs = unique.map((user) => {
        const userObj = {
          get: jest.fn(),
          set: jest.fn().mockReturnThis(),
        };
        when(userObj.get).calledWith('userId').mockReturnValue(user);
        when(userObj.get).calledWith('name').mockReturnValue(`name-${user}`);
        when(userObj.get).calledWith('username').mockReturnValue(`username-${user}`);

        when(userService.findById)
          .calledWith(user)
          .mockResolvedValue(null as never); // I don't know why this has to be "never"

        return {
          userObj,
          id: user,
        };
      });

      const orgs = users.map((item) => {
        const org = {
          get: jest.fn(),
        };

        when(org.get).calledWith('users.userId').mockReturnValue(item);
        when(org.get)
          .calledWith('users')
          .mockReturnValue(item.map((user) => userObjs.find(({ id }) => id === user).userObj));

        return org;
      });

      try {
        await (<any>service).populateUsers(orgs);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new NotFoundException('User not found: 9'));
      }

      expect(userService.findById).toBeCalledTimes(unique.length);
      unique.forEach((id) => {
        expect(userService.findById).toBeCalledWith(id);
      });
    });

    it('should get the data for each unique user once only', async () => {
      const users = [[1, 2, 3], [4], [1, 5, 4]];
      const unique = Array.from(new Set(users.flat()));

      const userObjs = unique.map((user) => {
        const userObj = {
          get: jest.fn(),
          set: jest.fn().mockReturnThis(),
        };
        when(userObj.get).calledWith('userId').mockReturnValue(user);
        when(userObj.get).calledWith('name').mockReturnValue(`name-${user}`);
        when(userObj.get).calledWith('username').mockReturnValue(`username-${user}`);

        when(userService.findById)
          .calledWith(user)
          .mockResolvedValue(userObj as never); // I don't know why this has to be "never"

        return {
          userObj,
          id: user,
        };
      });

      const orgs = users.map((item) => {
        const org = {
          get: jest.fn(),
        };

        when(org.get).calledWith('users.userId').mockReturnValue(item);
        when(org.get)
          .calledWith('users')
          .mockReturnValue(item.map((user) => userObjs.find(({ id }) => id === user).userObj));

        return org;
      });

      await (<any>service).populateUsers(orgs);

      expect(userService.findById).toBeCalledTimes(unique.length);
      unique.forEach((id) => {
        expect(userService.findById).toBeCalledWith(id);
      });

      userObjs.forEach(({ userObj, id }) => {
        expect(userObj.set).toBeCalledWith('name', `name-${id}`);
        expect(userObj.set).toBeCalledWith('username', `username-${id}`);
      });
    });
  });

  describe('#save', () => {
    it('should throw BadRequestException is validation fails', async () => {
      const error = {
        errors: 'some-err',
      };
      const org = {
        validate: jest.fn().mockRejectedValue(error),
      };

      try {
        await (<any>service).save(org);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new BadRequestException(error.errors));
      }

      expect(org.validate).toBeCalledWith();
    });

    it('should save and return the findById', async () => {
      jest.spyOn(service, 'findById');

      const orgId = 'some-orgId';
      const org = {
        get: jest.fn().mockReturnValue(orgId),
        save: jest.fn().mockResolvedValue(undefined),
        validate: jest.fn().mockResolvedValue(undefined),
      };
      (<any>service).findById.mockResolvedValue(org);

      expect(await (<any>service).save(org)).toBe(org);

      expect(org.validate).toBeCalledWith();
      expect(org.save).toBeCalledWith({ validateBeforeSave: false });
      expect(org.get).toBeCalledWith('id');
      expect(service.findById).toBeCalledWith(orgId);
    });
  });

  describe('#create', () => {
    beforeEach(() => {
      jest.spyOn(service, 'save' as any);
    });

    it('should throw error if user is not registered', async () => {
      const user: any = {
        get: jest.fn().mockReturnValue(false),
      };
      const data: any = {};

      try {
        await service.create(user, data);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(
          new ForbiddenException('Cannot create organization when user unregistered'),
        );
      }
    });

    it('should save an org if user is registered', async () => {
      const user: any = {
        get: jest.fn(),
      };
      const data: any = { some: 'data' };
      const userId = 'some-user-id';
      const result = 'save-result';
      const orgInst = { 'some-org': 'inst' };
      (<any>service).save.mockResolvedValue(result);
      OrgModel.mockReturnValue(orgInst);

      when(user.get)
        .calledWith('registered')
        .mockReturnValue(true)
        .calledWith('id')
        .mockReturnValue(userId);

      expect(await service.create(user, data)).toBe(result);

      expect(OrgModel).toBeCalledWith({
        ...data,
        users: [{ role: OrgRoles.ORG_MAINTAINER, userId }],
      });
      expect((<any>service).save).toBeCalledWith(orgInst);
    });
  });

  describe('#deleteById', () => {
    it('should throw a NotFoundException if organization not found', async () => {
      const userId = 'some-user-id';
      const user: any = {
        get: jest.fn().mockReturnValue(userId),
      };
      const orgId = 'some-orgId';

      OrgModel.findOneAndDelete.mockResolvedValue(null);

      try {
        await service.deleteById(user, orgId);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new NotFoundException('Organization not found'));

        expect(OrgModel.findOneAndDelete).toBeCalledWith({
          _id: orgId,
          'users.role': OrgRoles.ORG_MAINTAINER,
          'users.userId': userId,
        });
      }
    });

    it('should return true if organization found', async () => {
      const userId = 'some-user-id2';
      const user: any = {
        get: jest.fn().mockReturnValue(userId),
      };
      const orgId = 'some-orgId2';

      OrgModel.findOneAndDelete.mockResolvedValue('some-org');

      expect(await service.deleteById(user, orgId)).toBe(true);

      expect(OrgModel.findOneAndDelete).toBeCalledWith({
        _id: orgId,
        'users.role': OrgRoles.ORG_MAINTAINER,
        'users.userId': userId,
      });
    });
  });

  describe('#findById', () => {
    beforeEach(() => {
      (<any>service).populateUsers = jest.fn();
    });

    it('should throw a NotFoundException is organization not found', async () => {
      const orgId = 'some-org-id';

      OrgModel.findOne.mockResolvedValue(null);

      try {
        await service.findById(orgId);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new NotFoundException('Organization not found'));

        expect(OrgModel.findOne).toBeCalledWith({
          _id: orgId,
        });
      }
    });

    it('should populate the users if no argument given', async () => {
      const orgId = 'some-org-id2';
      const org = 'some-org';

      OrgModel.findOne.mockResolvedValue(org);
      (<any>service).populateUsers.mockResolvedValue();

      expect(await service.findById(orgId)).toBe(org);

      expect(OrgModel.findOne).toBeCalledWith({
        _id: orgId,
      });

      expect((<any>service).populateUsers).toBeCalledWith([org]);
    });

    it('should populate the users if populateUsers is true', async () => {
      const orgId = 'some-org-id3';
      const org = 'some-org2';

      OrgModel.findOne.mockResolvedValue(org);
      (<any>service).populateUsers.mockResolvedValue();

      expect(await service.findById(orgId, true)).toBe(org);

      expect(OrgModel.findOne).toBeCalledWith({
        _id: orgId,
      });

      expect((<any>service).populateUsers).toBeCalledWith([org]);
    });

    it('should not populate the users if populateUsers is false', async () => {
      const orgId = 'some-org-id2';
      const org = 'some-org';

      OrgModel.findOne.mockResolvedValue(org);
      (<any>service).populateUsers.mockResolvedValue();

      expect(await service.findById(orgId, false)).toBe(org);

      expect(OrgModel.findOne).toBeCalledWith({
        _id: orgId,
      });

      expect((<any>service).populateUsers).not.toBeCalled();
    });

    it('should add user ID if user given', async () => {
      const orgId = 'some-org-id4';
      const org = 'some-org4';
      const userId = 'some-user';
      const user: any = {
        get: jest.fn().mockReturnValue(userId),
      };

      OrgModel.findOne.mockResolvedValue(org);
      (<any>service).populateUsers.mockResolvedValue();

      expect(await service.findById(orgId, true, user)).toBe(org);

      expect(OrgModel.findOne).toBeCalledWith({
        _id: orgId,
        'users.userId': userId,
      });

      expect((<any>service).populateUsers).toBeCalledWith([org]);
    });
  });

  describe('#getUserRoles', () => {
    it('should return the roles that the user has', async () => {
      const userId = 'some-user-id';
      const user: any = { get: jest.fn().mockReturnValue(userId) };
      const orgId = 'some-org-id';
      const users = [
        {
          userId: 'invalid',
          role: 'unknown',
        },
        {
          userId,
          role: 'role1',
        },
        {
          userId,
          role: 'role2',
        },
        {
          userId,
          role: 'role2',
        },
        {
          userId: 'other',
          role: 'role4',
        },
      ];
      const org: any = { get: jest.fn().mockReturnValue(users) };

      service.findById = jest.fn().mockResolvedValue(org);

      expect(await service.getUserRoles(orgId, user)).toEqual(
        Array.from(new Set(users.filter((item) => userId === item.userId).map(({ role }) => role))),
      );
    });
  });

  describe('#paginate', () => {
    it('should defer to the max limit if it exceeds it', async () => {
      const maxLimit = 10;
      when(configService.get)
        .calledWith('organization.pagination.maxLimit')
        .mockReturnValue(maxLimit);

      const userId = 'some-user-id';
      const user: any = {
        get: jest.fn().mockReturnValue(userId),
      };
      const queryStrings: any = 'some-query-strings';
      const page: any = 'some-page';
      const requestLimit = maxLimit + 1;
      const docs = new Array(10);
      const totalPages = 'some-total-pages';
      const totalDocs = 'some-total-docs';
      const query = { some: 'query' };
      const sortBy = 'some-sortby';

      requestParserService.parse.mockReturnValue({
        query,
        sortBy,
      });

      OrgModel.paginate.mockResolvedValue({
        docs,
        totalPages,
        totalDocs,
      });

      expect(await service.paginate(queryStrings, page, requestLimit, user)).toEqual({
        data: docs,
        page,
        limit: maxLimit,
        count: docs.length,
        pages: totalPages,
        total: totalDocs,
      });

      expect(requestParserService.parse).toBeCalledWith(queryStrings);

      expect(OrgModel.paginate).toBeCalledWith(
        {
          ...query,
          'users.userId': userId,
        },
        {
          limit: maxLimit,
          page,
          sort: sortBy,
        },
      );
    });

    it('should paginate the data', async () => {
      const maxLimit = 10;
      when(configService.get)
        .calledWith('organization.pagination.maxLimit')
        .mockReturnValue(maxLimit);

      const userId = 'some-user-id3';
      const user: any = {
        get: jest.fn().mockReturnValue(userId),
      };
      const queryStrings: any = 'some-query-strings3';
      const page: any = 'some-page3';
      const requestLimit = maxLimit - 1;
      const docs = new Array(100);
      const totalPages = 'some-total-pages2';
      const totalDocs = 'some-total-docs2';
      const query = { some: 'query3' };
      const sortBy = 'some-sortby3';

      requestParserService.parse.mockReturnValue({
        query,
        sortBy,
      });

      OrgModel.paginate.mockResolvedValue({
        docs,
        totalPages,
        totalDocs,
      });

      expect(await service.paginate(queryStrings, page, requestLimit, user)).toEqual({
        data: docs,
        page,
        limit: requestLimit,
        count: docs.length,
        pages: totalPages,
        total: totalDocs,
      });

      expect(requestParserService.parse).toBeCalledWith(queryStrings);

      expect(OrgModel.paginate).toBeCalledWith(
        {
          ...query,
          'users.userId': userId,
        },
        {
          limit: requestLimit,
          page,
          sort: sortBy,
        },
      );
    });
  });

  describe('#removeUser', () => {
    it('should fail first validation and throw a BadRequestException', async () => {
      const userId = 'some-user-id';

      const orgs = [
        {
          get: jest.fn().mockReturnValue('org1'),
          validate: jest.fn().mockRejectedValue(new Error('invalid model')),
          removeUser: jest.fn(),
        },
      ];

      OrgModel.find.mockResolvedValue(orgs);

      try {
        await service.removeUser(userId);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(
          new BadRequestException({
            statusCode: 400,
            message: 'Organizations will not be valid if user removed',
            organizations: orgs.map((org) => org.get('id')),
          }),
        );
      }

      orgs.forEach((org) => {
        expect(org.validate).toBeCalledWith();
        expect(org.removeUser).toBeCalledWith(userId);
      });
    });

    it('should fail non-first validation and throw a BadRequestException', async () => {
      const userId = 'some-user-id2';

      const orgs = [
        {
          get: jest.fn().mockReturnValue('org1'),
          validate: jest.fn().mockResolvedValue(undefined),
          removeUser: jest.fn(),
        },
        {
          get: jest.fn().mockReturnValue('org2'),
          validate: jest.fn().mockRejectedValue(new Error('invalid model')),
          removeUser: jest.fn(),
        },
        {
          get: jest.fn().mockReturnValue('org3'),
          validate: jest.fn().mockResolvedValue(undefined),
          removeUser: jest.fn(),
        },
      ];

      OrgModel.find.mockResolvedValue(orgs);

      try {
        await service.removeUser(userId);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(
          new BadRequestException({
            statusCode: 400,
            message: 'Organizations will not be valid if user removed',
            organizations: orgs.map((org) => org.get('id')),
          }),
        );
      }

      orgs.forEach((org) => {
        expect(org.validate).toBeCalledWith();
        expect(org.removeUser).toBeCalledWith(userId);
      });
    });

    it('should pass validation and return array of OrgDocs', async () => {
      const userId = 'some-user-id2';

      const orgs = [
        {
          get: jest.fn().mockReturnValue('org0'),
          validate: jest.fn().mockResolvedValue(undefined),
          removeUser: jest.fn(),
          save: jest.fn(),
        },
        {
          get: jest.fn().mockReturnValue('org8'),
          validate: jest.fn().mockResolvedValue(undefined),
          removeUser: jest.fn(),
          save: jest.fn(),
        },
        {
          get: jest.fn().mockReturnValue('org2'),
          validate: jest.fn().mockResolvedValue(undefined),
          removeUser: jest.fn(),
          save: jest.fn(),
        },
      ].map((org) => {
        org.save.mockResolvedValue(org);
        return org;
      });

      OrgModel.find.mockResolvedValue(orgs);

      expect(await service.removeUser(userId)).toEqual(orgs);

      orgs.forEach((org) => {
        expect(org.validate).toBeCalledWith();
        expect(org.removeUser).toBeCalledWith(userId);
        expect(org.save).toBeCalledWith();
      });
    });
  });

  describe('#updateById', () => {
    beforeEach(() => {
      jest.spyOn(service, 'save' as any);
      jest.spyOn(service, 'findById');
    });

    it('should throw error if user is not registered', async () => {
      const user: any = {
        get: jest.fn().mockReturnValue(false),
      };
      const orgId = 'some-org-id';
      const data: any = {};

      try {
        await service.updateById(user, orgId, data);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(
          new ForbiddenException('Cannot create organization when user unregistered'),
        );
      }
    });

    it('should save an org if user is registered', async () => {
      const user: any = {
        get: jest.fn(),
      };
      const data: any = { some: 'data', some2: 'date2', hello: 'world' };
      const orgId = 'some-org-id2';
      const result = 'save-result2';
      const orgInst = { set: jest.fn() };

      (<any>service).findById.mockResolvedValue(orgInst);
      (<any>service).save.mockResolvedValue(result);

      user.get.mockReturnValue(true);

      expect(await service.updateById(user, orgId, data)).toBe(result);

      Object.keys(data).forEach((key) => {
        expect(orgInst.set).toBeCalledWith(key, data[key]);
      });
      expect(orgInst.set).toBeCalledTimes(Object.keys(data).length);
      expect((<any>service).save).toBeCalledWith(orgInst);
    });
  });
});
