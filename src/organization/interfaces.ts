import { Document, FilterQuery, Model } from 'mongoose';

export interface PaginateResult<T> {
  docs: T[];
  totalDocs: number;
  limit: number;
  page?: number;
  totalPages: number;
  nextPage?: number | null;
  prevPage?: number | null;
  pagingCounter: number;
  hasPrevPage: boolean;
  hasNextPage: boolean;
  meta?: any;
  [customLabel: string]: T[] | number | boolean | null | undefined;
}

export interface IPaginateModel<T extends Document, TQueryHelpers = {}>
  extends Model<T, TQueryHelpers> {
  paginate(query?: FilterQuery<T>, options?: any): Promise<PaginateResult<T>>;
}

export interface IPaginate<T> {
  count: number;
  data: T[];
  limit: number;
  page: number;
  pages: number;
  total: number;
}

export interface IOrganizationUser {
  name: string;
  username: string;
  role: string;
  userId: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface IOrganization<U = IOrganizationUser> {
  _id: string;
  name: string;
  slug: string;
  users: U[];
  createdAt: Date;
  updatedAt: Date;
}

export enum OrgRoles {
  ORG_MAINTAINER = 'ORG_MAINTAINER',
  ORG_USER = 'ORG_USER',
}

export enum OrgSubjects {
  Organization = 'ORGANIZATION',
}

export interface IOrgFlagMetadata {
  flag: string;
  param: boolean;
}
