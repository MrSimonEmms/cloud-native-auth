import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';

import { IOrganizationUser, OrgRoles } from './interfaces';

export type OrganizationUserDocument = OrganizationUser & Document;

@Schema({
  timestamps: true,
  _id: false,
})
export class OrganizationUser implements IOrganizationUser {
  @Prop({
    type: String,
    validate(name: string) {
      if (name !== undefined) {
        throw new Error('Cannot set name');
      }
    },
  })
  name: string;

  @Prop({
    type: String,
    validate(username: string) {
      if (username !== undefined) {
        throw new Error('Cannot set username');
      }
    },
  })
  username: string;

  @Prop({
    type: String,
    required: true,
    index: true,
    enum: Object.values(OrgRoles),
  })
  role: string;

  @Prop({
    type: String,
    required: true,
    index: true,
  })
  userId: string;

  @Prop({
    type: Date,
    index: true,
  })
  @ApiProperty({
    example: new Date(),
    description: 'Created date',
  })
  createdAt: Date;

  @Prop({
    type: Date,
  })
  @ApiProperty({
    example: new Date(),
    description: 'Last updated date',
  })
  updatedAt: Date;
}

export const OrganizationUserSchema = SchemaFactory.createForClass(OrganizationUser);

OrganizationUserSchema.index({ userId: 1, role: 1 });
