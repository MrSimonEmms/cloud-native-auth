import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import * as uuid from 'uuid';

import { IOrganization, IOrganizationUser, OrgRoles } from './interfaces';
// eslint-disable-next-line import/no-cycle
import { OrganizationUserSchema } from './organizationUser.schema';
// eslint-disable-next-line import/no-cycle
import { User } from '../user/user.schema';

export type OrganizationDocument = Organization & Document;

@Schema({
  timestamps: true,
})
export class Organization implements IOrganization {
  removeUser: Function;

  @Prop({
    type: String,
    default: () => uuid.v4(),
  })
  @ApiProperty({
    name: 'id',
    example: 'e5b99a55-8b83-42c3-9895-c83287ae4810',
    description: 'Unique ID in UUIDv4 format',
  })
  _id: string;

  @Prop({
    type: String,
    required: true,
    maxLength: 200,
  })
  @ApiProperty({
    example: 'OrgName',
    description: 'Name of organization',
  })
  name: string;

  @Prop({
    type: String,
    required: true,
    minLength: 3,
    maxLength: 20,
    lowercase: true,
    index: true,
    async validate(slug: string): Promise<boolean> {
      if (!/^[a-z0-9\-_]+$/.test(slug)) {
        throw new Error('Slug must be alphanumeric');
      }

      const org = await this.model(Organization.name)
        .findOne({ slug })
        .where('_id')
        .nin([this.get('id')]);

      if (org !== null) {
        throw new Error('Slug already registered');
      }

      return true;
    },
  })
  @ApiProperty({
    example: 'OrgName',
    description: 'Name of organization',
  })
  slug: string;

  @Prop({
    type: [OrganizationUserSchema],
    required: true,
    async validate(users: IOrganizationUser[]): Promise<boolean> {
      if (users.length === 0) {
        throw new Error('Organization must have at least one user');
      }

      /* Must have at least one maintainer */
      const hasMaintainer = users.some(({ role }) => role === OrgRoles.ORG_MAINTAINER);

      if (!hasMaintainer) {
        throw new Error('Organization must have at least one maintainer');
      }

      /* Check user IDs are valid - subdocuments don't have "this.model" */
      const currentUsers = await Promise.all(
        users.map(async ({ userId }) => {
          const result = await this.model(User.name).findById(userId);

          if (!result) {
            throw new Error(`Invalid user: ${userId}`);
          }

          return {
            result,
            userId,
          };
        }),
      );

      const uniqueUsers = new Set(currentUsers.map(({ userId }) => userId));

      if (uniqueUsers.size !== currentUsers.length) {
        throw new Error('Duplicate users in list');
      }

      return true;
    },
  })
  users: IOrganizationUser[];

  @Prop({
    type: Date,
    index: true,
  })
  @ApiProperty({
    example: new Date(),
    description: 'Created date',
  })
  createdAt: Date;

  @Prop({
    type: Date,
  })
  @ApiProperty({
    example: new Date(),
    description: 'Last updated date',
  })
  updatedAt: Date;
}

export const OrganizationSchema = SchemaFactory.createForClass(Organization);

OrganizationSchema.index({ _id: 1, 'users.userId': 1, 'users.role': 1 });

OrganizationSchema.methods.removeUser = function removeUser(userId) {
  this.set(
    'users',
    this.get('users').filter((user) => user.userId !== userId),
  );

  return this;
};

['toJSON', 'toObject'].forEach((path) => {
  OrganizationSchema.set(path, {
    virtuals: true,
    versionKey: false,
    transform: /* istanbul ignore next */ (doc, ret) => {
      return {
        ...ret,
        _id: undefined,
      };
    },
  });
});
