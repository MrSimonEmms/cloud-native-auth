import {
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  HttpCode,
  Inject,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UseRoles } from 'nest-access-control';
import {
  ApiBearerAuth,
  ApiExtraModels,
  ApiOperation,
  ApiResponse,
  getSchemaPath,
} from '@nestjs/swagger';

import User from '../decorators/user.decorator';
import { OrgId } from './orgId.decorator';
import OrganizationService from './organization.service';
import { IOrganization, OrgSubjects } from './interfaces';
import RoleGuard from '../roles/roles.guard';
import { IgnoreRolesInterceptor } from '../roles/ignoreRolesInterceptor.decorator';
import { Organization as OrganizationSchema } from './organization.schema';
import PaginateDTO from './paginate.dto';

@UseGuards(AuthGuard('jwt'), RoleGuard)
@Controller('organization')
@ApiExtraModels(PaginateDTO)
export default class OrganizationController {
  @Inject(OrganizationService)
  private readonly organizationService: OrganizationService;

  @Get()
  @UseRoles({
    resource: OrgSubjects.Organization,
    action: 'read',
    possession: 'own',
  })
  @ApiBearerAuth('jwt')
  @ApiResponse({
    status: 200,
    description: 'Return many organizations',
    schema: {
      allOf: [
        {
          properties: {
            data: {
              type: 'array',
              items: { $ref: getSchemaPath(OrganizationSchema) },
            },
          },
        },
        { $ref: getSchemaPath(PaginateDTO) },
      ],
    },
  })
  @ApiOperation({ tags: ['organization'], summary: 'Return many organizations' })
  async getMany(
    @User() user,
    @Query() query,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(25), ParseIntPipe) limit: number,
  ) {
    return this.organizationService.paginate(query, page, limit, user);
  }

  @Post()
  @UseRoles({
    resource: OrgSubjects.Organization,
    action: 'create',
    possession: 'own',
  })
  @HttpCode(201)
  @ApiBearerAuth('jwt')
  @ApiResponse({
    status: 201,
    description: 'Create the organization object',
    type: OrganizationSchema,
  })
  @ApiOperation({ tags: ['organization'], summary: 'Create the organization object' })
  async create(@User() user, @Body() body: IOrganization) {
    return this.organizationService.create(user, body);
  }

  @Get('/:id')
  @OrgId('id')
  @UseRoles({
    resource: OrgSubjects.Organization,
    action: 'read',
    possession: 'own',
  })
  @ApiBearerAuth('jwt')
  @ApiResponse({
    status: 200,
    description: 'Get the organization object',
    type: OrganizationSchema,
  })
  @ApiOperation({ tags: ['organization'], summary: 'Get the organization object' })
  async getOne(@User() user, @Param('id') id) {
    return this.organizationService.findById(id, true, user);
  }

  @Delete('/:id')
  @OrgId('id')
  @UseRoles({
    resource: OrgSubjects.Organization,
    action: 'delete',
    possession: 'own',
  })
  @IgnoreRolesInterceptor()
  @HttpCode(204)
  @ApiBearerAuth('jwt')
  @ApiResponse({
    status: 204,
    description: 'Delete the organization object',
  })
  @ApiOperation({ tags: ['organization'], summary: 'Delete the organization object' })
  async deleteOne(@User() user, @Param('id') id) {
    return this.organizationService.deleteById(user, id);
  }

  @Patch('/:id')
  @OrgId('id')
  @UseRoles({
    resource: OrgSubjects.Organization,
    action: 'update',
    possession: 'own',
  })
  @ApiBearerAuth('jwt')
  @ApiResponse({
    status: 200,
    description: 'Update the organization object',
    type: OrganizationSchema,
  })
  @ApiOperation({ tags: ['organization'], summary: 'Update the organization object' })
  async updateOne(@User() user, @Param('id') id, @Body() body: Partial<IOrganization>) {
    return this.organizationService.updateById(user, id, body);
  }
}
