import { SetMetadata } from '@nestjs/common';
import { IOrgFlagMetadata } from './interfaces';

export const OrgIdMetaData: string = 'ORG_METADATA';

export const OrgId = (flag: string, param = true) =>
  SetMetadata<string, IOrgFlagMetadata>(OrgIdMetaData, {
    flag,
    param,
  });
