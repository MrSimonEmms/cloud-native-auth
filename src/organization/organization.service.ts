import {
  BadRequestException,
  ForbiddenException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PinoLogger } from 'nestjs-pino';
import { FilterQuery } from 'mongoose';
import { ConfigService } from '@nestjs/config';

import { Organization, OrganizationDocument } from './organization.schema';
import { IOrganization, IPaginateModel, OrgRoles } from './interfaces';
import { UserDocument } from '../user/user.schema';
// eslint-disable-next-line import/no-cycle
import UserService from '../user/user.service';
import PaginateDTO from './paginate.dto';
import RequestParserService from '../request-parser/request-parser.service';

@Injectable()
export default class OrganizationService {
  @Inject(ConfigService)
  private configService: ConfigService;

  @Inject(PinoLogger)
  private readonly logger: PinoLogger;

  @Inject(RequestParserService)
  private requestParserService: RequestParserService;

  @Inject(UserService)
  private userService: UserService;

  @InjectModel(Organization.name)
  private readonly OrganizationModel: IPaginateModel<OrganizationDocument>;

  protected async populateUsers(organizations: OrganizationDocument[]): Promise<void> {
    this.logger.info('Populating users for organizations');
    const userMap: Map<string, UserDocument> = new Map();

    /* Build a map of users */
    await Promise.all(
      Array.from(
        /* Use a set to ensure unique users */
        new Set(organizations.map((organization) => organization.get('users.userId')).flat()),
      ).map(async (userId) => {
        const user = await this.userService.findById(userId);
        if (!user) {
          throw new NotFoundException(`User not found: ${userId}`);
        }

        this.logger.debug({ userId }, 'Extracting user to user map');

        userMap.set(userId, user);
      }),
    );

    this.logger.info(
      { users: Array.from(userMap.keys()) },
      'Found map of unique organization users',
    );

    organizations.map((organization) =>
      organization.get('users').forEach((orgUser) => {
        const userId = orgUser.get('userId');
        const user = userMap.get(userId);

        orgUser.set('name', user.get('name')).set('username', user.get('username'));
      }),
    );
  }

  protected async save(org: OrganizationDocument): Promise<OrganizationDocument> {
    try {
      this.logger.info({ org }, 'Validating organization');
      await org.validate();
    } catch (err) {
      this.logger.warn({ err }, 'Organization validation failed');

      throw new BadRequestException(err.errors);
    }

    this.logger.info('Saving organization');
    await org.save({ validateBeforeSave: false });

    return this.findById(org.get('id'));
  }

  async create(user: UserDocument, data: IOrganization): Promise<OrganizationDocument> {
    if (user.get('registered') === false) {
      throw new ForbiddenException('Cannot create organization when user unregistered');
    }

    const org = new this.OrganizationModel({
      ...data,
      /* Set current user as maintainer */
      users: [{ role: OrgRoles.ORG_MAINTAINER, userId: user.get('id') }],
    });

    this.logger.info({ org }, 'Creating new organization');

    return this.save(org);
  }

  async deleteById(user: UserDocument, organizationId: string): Promise<boolean> {
    const userId = user.get('id');
    this.logger.info({ userId, organizationId }, 'Deleting organization');

    const organization = await this.OrganizationModel.findOneAndDelete({
      _id: organizationId,
      'users.role': OrgRoles.ORG_MAINTAINER,
      'users.userId': userId,
    });

    if (!organization) {
      throw new NotFoundException('Organization not found');
    }

    return true;
  }

  async findById(
    organizationId: string,
    populateUsers: boolean = true,
    user?: UserDocument,
  ): Promise<OrganizationDocument> {
    const filter: FilterQuery<OrganizationDocument> = { _id: organizationId };
    if (user) {
      filter['users.userId'] = user.get('id');
    }

    this.logger.info({ filter }, 'Finding organization by ID');
    const organization = await this.OrganizationModel.findOne(filter);

    if (!organization) {
      throw new NotFoundException('Organization not found');
    }

    if (populateUsers) {
      await this.populateUsers([organization]);
    }

    return organization;
  }

  async getUserRoles(organizationId: string, user: UserDocument): Promise<string[]> {
    const org = await this.findById(organizationId);
    const currentUserId = user.get('id');

    /* Currently, a user only has one role, but return string[] to allow for future modification - dedupe for safety */
    return Array.from(
      new Set(
        org
          .get('users')
          .filter(({ userId }) => currentUserId === userId)
          .map(({ role }) => role),
      ),
    );
  }

  async paginate(
    queryStrings: Object,
    page: number,
    requestLimit: number,
    user: UserDocument,
  ): Promise<PaginateDTO<OrganizationDocument>> {
    let limit = requestLimit;

    const userId = user.get('id');
    this.logger.debug({ userId, queryStrings, page, limit }, 'Paginate organizations');

    const maxLimit = this.configService.get<number>('organization.pagination.maxLimit');
    if (limit > maxLimit) {
      this.logger.warn({ requestLimit, maxLimit }, 'Requested limit has been exceeded');
      limit = maxLimit;
    }

    const { query, sortBy } = this.requestParserService.parse(queryStrings);

    /* Ensure that the user is a member of the organization */
    query['users.userId'] = userId;

    this.logger.info({ query, limit, page, sortBy }, 'Organization pagination query');

    const { docs, totalPages, totalDocs } = await this.OrganizationModel.paginate(query, {
      limit,
      page,
      sort: sortBy,
    });

    await this.populateUsers(docs);

    return {
      data: docs,
      page,
      limit,
      count: docs.length,
      pages: totalPages,
      total: totalDocs,
    };
  }

  async removeUser(userId: string): Promise<OrganizationDocument[]> {
    this.logger.info({ userId }, 'Finding organizations with user');

    const organizations = await this.OrganizationModel.find({
      'users.userId': userId,
    });

    const preSave = await Promise.all(
      organizations.map(async (organization) => {
        organization.removeUser(userId);

        let valid = false;
        try {
          await organization.validate();
          valid = true;
        } catch (err) {
          this.logger.warn({ err }, 'Error validating user delete');
        }

        return {
          organization,
          valid,
        };
      }),
    );

    const canRemove = preSave.every(({ valid }) => valid);

    this.logger.info({ count: organizations.length, canRemove }, 'Organizations with user');

    if (!canRemove) {
      const invalidOrgs = preSave
        .filter(({ valid }) => !valid)
        .map(({ organization }) => organization.get('id'));
      const message = 'Organizations will not be valid if user removed';

      this.logger.warn({ organizations: invalidOrgs }, message);

      throw new BadRequestException({
        statusCode: 400,
        message,
        organizations: invalidOrgs,
      });
    }

    this.logger.info('Deleting user from organizations');

    return Promise.all(
      preSave.map(async ({ organization }) => {
        this.logger.debug({ organization }, 'Saving organization');

        return organization.save();
      }),
    );
  }

  async updateById(
    user: UserDocument,
    organizationId: string,
    data: Partial<IOrganization>,
  ): Promise<OrganizationDocument> {
    if (user.get('registered') === false) {
      throw new ForbiddenException('Cannot create organization when user unregistered');
    }

    this.logger.info({ organizationId }, 'Updating organization');

    const org = await this.findById(organizationId, false);

    Object.keys(data).forEach((key) => {
      org.set(key, data[key]);
    });

    this.logger.info({ org, organizationId }, 'Updating organization');

    return this.save(org);
  }
}
