import * as qs from 'querystring';
import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { PinoLogger } from 'nestjs-pino';
import axios from 'axios';

import UserController from './user.controller';
import UserService from './user.service';
import { User, UserDocument, UserSchema } from './user.schema';
// eslint-disable-next-line import/no-cycle
import OrganizationModule from '../organization/organization.module';

const jwtModule = JwtModule.registerAsync({
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => ({
    signOptions: {
      notBefore: 0,
      expiresIn: configService.get('jwt.expiresIn'),
      issuer: configService.get('jwt.issuer'),
    },
    secret: configService.get('jwt.secret'),
  }),
});

@Module({
  imports: [
    forwardRef(() => OrganizationModule),
    MongooseModule.forFeatureAsync([
      {
        name: User.name,
        imports: [jwtModule],
        inject: [JwtService, ConfigService, PinoLogger],
        useFactory: (jwtService: JwtService, configService: ConfigService, logger: PinoLogger) => {
          const schema = UserSchema;

          const { enabled, url } = configService.get('hooks.confirmEmail');

          if (enabled) {
            schema.pre('save', async function preSave(this: UserDocument) {
              if (this.isModified('emailAddress')) {
                const id = this.get('id');

                const token = jwtService.sign({
                  id,
                  emailAddress: this.get('emailAddress'),
                });

                this.set('confirmed', false);

                try {
                  logger.info({ id, hooksUrl: url }, 'Generating a confirm email token for user');

                  const domain = configService.get(`domain.current`);
                  const tokenQs = qs.stringify({
                    token,
                  });

                  await axios.post(url, {
                    url: `${domain}/user/emailAddress/confirm?${tokenQs}`,
                    user: this.toPublic(),
                  });
                } catch (err) {
                  logger.error({ err }, 'Failed to dispatch to confirm email hook');
                }
              }
            });
          }

          return schema;
        },
      },
    ]),
    jwtModule,
  ],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export default class UserModule {}
