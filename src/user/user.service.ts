import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { PinoLogger } from 'nestjs-pino';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

import { User, UserDocument } from './user.schema';
import { IProviderUserData } from '../provider/interfaces';
import { IUser, IUserQueryStrings } from './interfaces';
// eslint-disable-next-line import/no-cycle
import OrganizationService from '../organization/organization.service';
import LoginDto from './login.dto';

@Injectable()
export default class UserService {
  @InjectModel(User.name)
  private readonly UserModel: Model<UserDocument>;

  @Inject(PinoLogger)
  private readonly logger: PinoLogger;

  @Inject(JwtService)
  private readonly jwt: JwtService;

  @Inject(ConfigService)
  private readonly configService;

  @Inject(forwardRef(() => OrganizationService))
  private readonly orgService: OrganizationService;

  async addProviderToUser(
    providerUser: IProviderUserData,
    linkUserId?: string,
  ): Promise<UserDocument> {
    let user: UserDocument | void;

    if (linkUserId) {
      this.logger.info({ linkUserId }, 'Linking provider to user');
      user = await this.findById(linkUserId);

      if (!user) {
        /* Unlikely to get here - fail-safe */
        throw new NotFoundException({
          code: 'UNKNOWN_USER',
          message: 'Unknown user',
        });
      }

      /* Check if the tokens are used for a different account */
      const otherUser = await this.findByProviderUserId(providerUser, user.id);

      if (otherUser) {
        /* This provider is associated with another user - be careful */
        if (otherUser.registered) {
          throw new BadRequestException({
            code: 'PROVIDER_REGISTERED',
            message: 'Provider registered with other user',
          });
        }

        const otherUserId = otherUser.get('id');
        this.logger.debug(
          { userId: otherUserId },
          'Provider registered to unregistered user - deleting',
        );
        await this.deleteById(otherUserId);
      }
    } else {
      this.logger.info('Creating a new user');

      /* We're creating a new user */
      user = await this.findByProviderUserId(providerUser);

      if (user) {
        this.logger.info('User already registered with provider');
      } else {
        this.logger.info('User does not exist - creating');
        user = new this.UserModel();
      }
    }

    user.addProvider(providerUser);

    /* This is not responsible for data other than the provider */
    await user.save({ validateBeforeSave: false });

    return user;
  }

  async confirmEmail(id: string, emailAddress: string): Promise<true> {
    const { n: numberMatched } = await this.UserModel.updateOne(
      {
        _id: id,
        emailAddress,
        confirmed: false,
      },
      {
        $set: {
          confirmed: true,
        },
      },
    );

    if (numberMatched !== 1) {
      throw new BadRequestException({
        code: 'UNABLE_TO_CONFIRM_EMAIL',
        message: 'Unable to confirm email address',
      });
    }

    return true;
  }

  async create(body: IUser): Promise<UserDocument> {
    const data: IUser = { ...body };

    /* Delete keys we cannot update here */
    delete data._id; // eslint-disable-line no-underscore-dangle
    delete data.registered;
    delete data.confirmed;
    delete data.providers; // These are expected to be password users
    delete data.createdAt;
    delete data.updatedAt;

    this.logger.info('Creating user');

    const document = new this.UserModel(data);

    try {
      await document.validate();
    } catch (err) {
      this.logger.warn({ err, errors: err.errors }, 'Create user schema failed');
      throw new BadRequestException(err.errors);
    }

    /* Save - already validated so don't bother repeating */
    await document.save({ validateBeforeSave: false });

    this.logger.info('Created user saved to database');

    return document;
  }

  async deleteById(id: string): Promise<boolean> {
    /* Check if it's the sole maintainer of any organizations and remove if not */
    await this.orgService.removeUser(id);

    this.logger.info({ id }, 'Deleting user by ID');
    const result = await this.UserModel.deleteOne({
      _id: id,
    });

    const deleted = result.deletedCount === 1;

    this.logger.info({ result, deleted }, 'Delete operation completed');

    return deleted;
  }

  async findById(userId: string): Promise<UserDocument | void> {
    this.logger.info({ userId }, 'Find user by ID');
    return this.UserModel.findById(userId);
  }

  async findByProviderUserId(
    provider: IProviderUserData,
    excludeId?: string,
  ): Promise<UserDocument | void> {
    const filter: FilterQuery<any> = {
      [`providers.${provider.type}.userId`]: provider.userId,
    };

    if (excludeId) {
      // eslint-disable-next-line no-underscore-dangle
      filter._id = {
        $ne: excludeId,
      };
    }

    this.logger.info({ filter }, 'Searching for user by provider details');

    return this.UserModel.findOne(filter);
  }

  getPublicLoginQueryStrings(user: UserDocument): IUserQueryStrings {
    return {
      token: Buffer.from(this.getToken(user)).toString('base64'),
    };
  }

  getToken(user: UserDocument): string {
    const id = user.get('id');

    this.logger.info({ id }, 'Generating a token for user');

    return this.jwt.sign({
      id,
    });
  }

  async login(input: LoginDto): Promise<UserDocument> {
    let user: UserDocument | undefined;
    let valid: boolean = false;

    const { username, emailAddress, password } = input;

    if (username) {
      this.logger.info({ username }, 'Finding user by username');

      user = await this.UserModel.findOne({
        usernameLower: username.toLowerCase(),
      });
    } else if (emailAddress) {
      this.logger.info({ emailAddress }, 'Finding user by emailAddress');

      user = await this.UserModel.findOne({
        emailAddressLower: emailAddress.toLowerCase(),
      });
    }

    if (user) {
      this.logger.debug('Verifying password');

      valid = await user.verifyPassword(password);
      this.logger.info({ valid }, 'Password verification result');
    }

    if (!valid) {
      this.logger.debug('User invalid');

      throw new UnauthorizedException();
    }

    return user;
  }

  async removeProvider(user: UserDocument, providerId: string): Promise<UserDocument> {
    user.removeProvider(providerId);

    await user.save({ validateBeforeSave: false });

    return user;
  }

  async updateUser(userId: string, body: Partial<IUser>) {
    const data: Partial<IUser> = { ...body };

    /* Delete keys we cannot update here */
    delete data._id; // eslint-disable-line no-underscore-dangle
    delete data.confirmed;
    delete data.providers;
    delete data.createdAt;
    delete data.updatedAt;

    if (!this.configService.get('providers.password.enabled')) {
      this.logger.debug('Removing password from input as password login disabled');
      delete data.password;
    }

    this.logger.info({ userId, data }, 'Updating user');

    const document = await this.findById(userId);

    if (!document) {
      this.logger.warn('User not found');
      throw new NotFoundException();
    }

    Object.keys(data).forEach((key) => {
      this.logger.debug({ value: data[key], key }, 'Setting user data');
      document.set(key, data[key]);
    });

    try {
      await document.validate();
    } catch (err) {
      this.logger.warn({ userId, err, errors: err.errors }, 'Update user schema failed');
      throw new BadRequestException(err.errors);
    }

    /* Save - already validated so don't bother repeating */
    await document.save({ validateBeforeSave: false });

    this.logger.info({ userId }, 'Updated document saved to database');

    return document;
  }
}
