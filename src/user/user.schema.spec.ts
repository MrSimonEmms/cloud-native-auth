import * as crypto from 'crypto';
import { TypeMetadataStorage } from '@nestjs/mongoose/dist/storages/type-metadata.storage';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { SchemaFactory } from '@nestjs/mongoose';
import { when } from 'jest-when';
import * as uuid from 'uuid';

import { User, UserSchema } from './user.schema';

jest.mock('uuid');
jest.mock('@nestjs/mongoose/dist/factories/schema.factory', () => ({
  SchemaFactory: {
    createForClass: jest.fn().mockReturnValue({
      pre: jest.fn(),
      post: jest.fn(),
      methods: {},
      set: jest.fn(),
    }),
  },
}));

function getProp(key: string) {
  return TypeMetadataStorage.getSchemaMetadataByTarget(User).properties.find(
    ({ propertyKey }) => propertyKey === key,
  );
}

describe('UserSchema', () => {
  beforeEach(() => {
    expect(SchemaFactory.createForClass).toBeCalledWith(User);
  });

  describe('props', () => {
    describe('_id', () => {
      let prop;
      beforeEach(() => {
        prop = getProp('_id');
      });

      it('should default to a uuid', () => {
        const uuidValue = 'some-uuid';
        (<any>uuid).v4.mockReturnValue(uuidValue);
        expect(prop.options.default()).toBe(uuidValue);
      });
    });

    describe('emailAddress', () => {
      let prop;
      beforeEach(() => {
        prop = getProp('emailAddress');
      });

      it('should throw an error if invalid email', async () => {
        try {
          await prop.options.validate('invalid');
          throw new Error('invalid');
        } catch (err) {
          expect(err).toEqual(new Error('Value is an invalid email address'));
        }
      });

      it('should throw an error if duplicate email', async () => {
        const userId = 'some-user-id';
        const context = {
          findOne: jest.fn().mockReturnThis(),
          get: jest.fn().mockReturnValue(userId),
          model: jest.fn().mockReturnThis(),
          nin: jest.fn().mockResolvedValue('some-user'),
          where: jest.fn().mockReturnThis(),
        };
        const emailAddress = 'duplicate@email.com';

        try {
          await prop.options.validate.call(context, emailAddress);
          throw new Error('invalid');
        } catch (err) {
          expect(err).toEqual(new Error('Email address already registered'));
        }

        expect(context.model).toBeCalledWith(User.name);
        expect(context.findOne).toBeCalledWith({ emailAddressLower: emailAddress });
        expect(context.where).toBeCalledWith('_id');
        expect(context.nin).toBeCalledWith([userId]);
      });

      it('should return true if valid', async () => {
        const userId = 'some-user-id2';
        const context = {
          findOne: jest.fn().mockReturnThis(),
          get: jest.fn().mockReturnValue(userId),
          model: jest.fn().mockReturnThis(),
          nin: jest.fn().mockResolvedValue(null),
          where: jest.fn().mockReturnThis(),
        };
        const emailAddress = 'unique@email.com';

        expect(await prop.options.validate.call(context, emailAddress)).toBe(true);

        expect(context.model).toBeCalledWith(User.name);
        expect(context.findOne).toBeCalledWith({ emailAddressLower: emailAddress });
        expect(context.where).toBeCalledWith('_id');
        expect(context.nin).toBeCalledWith([userId]);
      });
    });

    describe('username', () => {
      let prop;
      beforeEach(() => {
        prop = getProp('username');
      });

      it('should throw an error if invalid username', async () => {
        const invalids = ['a', 'ab', 'user%'];

        await Promise.all(
          invalids.map(async (item) => {
            try {
              await prop.options.validate(item);
              throw new Error('invalid');
            } catch (err) {
              expect(err).toEqual(new Error('Value is an invalid username'));
            }
          }),
        );
      });

      it('should throw an error if duplicate username', async () => {
        const userId = 'some-user-id';
        const context = {
          findOne: jest.fn().mockReturnThis(),
          get: jest.fn().mockReturnValue(userId),
          model: jest.fn().mockReturnThis(),
          nin: jest.fn().mockResolvedValue('some-user'),
          where: jest.fn().mockReturnThis(),
        };
        const username = 'duplicate';

        try {
          await prop.options.validate.call(context, username);
          throw new Error('invalid');
        } catch (err) {
          expect(err).toEqual(new Error('Username already registered'));
        }

        expect(context.model).toBeCalledWith(User.name);
        expect(context.findOne).toBeCalledWith({ usernameLower: username });
        expect(context.where).toBeCalledWith('_id');
        expect(context.nin).toBeCalledWith([userId]);
      });

      it('should return true if valid', async () => {
        const userId = 'some-user-id2';
        const context = {
          findOne: jest.fn().mockReturnThis(),
          get: jest.fn().mockReturnValue(userId),
          model: jest.fn().mockReturnThis(),
          nin: jest.fn().mockResolvedValue(null),
          where: jest.fn().mockReturnThis(),
        };
        const username = 'unique';

        expect(await prop.options.validate.call(context, username)).toBe(true);

        expect(context.model).toBeCalledWith(User.name);
        expect(context.findOne).toBeCalledWith({ usernameLower: username });
        expect(context.where).toBeCalledWith('_id');
        expect(context.nin).toBeCalledWith([userId]);
      });
    });

    describe('password', () => {
      let prop;
      beforeEach(() => {
        prop = getProp('password');
      });

      it('should return false if providers greater than 0', () => {
        const context = {
          providers: new Map([['hello', 'world']]),
        };

        expect(prop.options.required.call(context)).toBe(false);
      });

      it('should return true if providers equal to 0', () => {
        const context = {
          providers: new Map(),
        };

        expect(prop.options.required.call(context)).toBe(true);
      });
    });

    describe('providers', () => {
      let prop;
      beforeEach(() => {
        prop = getProp('providers');
      });

      it('should default to an empty object', () => {
        expect(prop.options.default()).toEqual({});
      });

      it('should error if no provider set', async () => {
        try {
          await prop.options.validate(new Map());
          throw new Error('invalid');
        } catch (err) {
          expect(err).toEqual(new Error('Must have at least 1 provider set'));
        }
      });

      it('should return true if valid', async () => {
        const providers = new Map([
          [
            'provider',
            {
              type: 'type1',
            },
          ],
        ]);
        expect(await prop.options.validate(providers)).toBe(true);
      });
    });
  });

  describe('hooks', () => {
    let preHooks;
    let postHooks;
    beforeEach(() => {
      preHooks = (<any>UserSchema).pre.mock.calls;
      postHooks = (<any>UserSchema).post.mock.calls;
    });

    describe('pre', () => {
      describe('validate', () => {
        let validate;

        beforeEach(() => {
          [, validate] = preHooks.find(([name]) => name === 'validate');
        });

        it('should set the lowercase keys - some value', () => {
          const lowercaseKeys = ['emailAddress', 'username'];

          const context = {
            get: jest.fn(),
            set: jest.fn(),
          };

          lowercaseKeys.forEach((key) =>
            when(context.get).calledWith(key).mockReturnValue(`${key}-value`),
          );

          expect(validate.call(context)).toBeUndefined();

          lowercaseKeys.forEach((key) => {
            expect(context.get).toBeCalledWith(key);
            expect(context.set).toBeCalledWith(`${key}Lower`, `${key}-value`);
          });
        });

        it('should set the lowercase keys - no value', () => {
          const lowercaseKeys = ['emailAddress', 'username'];

          const context = {
            get: jest.fn(),
            set: jest.fn(),
          };

          lowercaseKeys.forEach((key) =>
            when(context.get).calledWith(key).mockReturnValue(undefined),
          );

          expect(validate.call(context)).toBeUndefined();

          lowercaseKeys.forEach((key) => {
            expect(context.get).toBeCalledWith(key);
            expect(context.set).toBeCalledWith(`${key}Lower`, undefined);
          });
        });
      });

      describe('save', () => {
        let validate;

        beforeEach(() => {
          [, validate] = preHooks.find(([name]) => name === 'save');
        });

        it('should not set the password if not set', async () => {
          const context = {
            get: jest.fn(),
            set: jest.fn(),
          };

          context.get.mockReturnValue('password');

          expect(await validate.call(context)).toBeUndefined();

          expect(context.set).toBeCalledWith('tmpPassword', undefined);
        });

        it('should set the password if set', async () => {
          const context = {
            get: jest.fn(),
            set: jest.fn(),
          };
          const salt = Buffer.from('some-salt');
          const hash = Buffer.from('some-hash');
          const inputPassword = 'some-password';

          jest.spyOn(crypto, 'randomBytes');
          jest.spyOn(crypto, 'scrypt');

          (<any>crypto).randomBytes.mockReturnValue(salt);
          (<any>crypto).scrypt.mockImplementation((password, saltValue, hashKeyLength, cb) => {
            expect(password).toBe(inputPassword);
            expect(saltValue).toBe(salt.toString('hex'));
            expect(hashKeyLength).toBe(64);

            cb(null, hash);
          });

          when(context.get).calledWith('password').mockReturnValue(inputPassword);
          when(context.get).calledWith('tmpPassword').mockReturnValue('tmpPassword');

          expect(await validate.call(context)).toBeUndefined();

          expect(context.set).toBeCalledWith(
            'password',
            [salt.toString('hex'), hash.toString('hex')].join(':'),
          );
          expect(context.set).toBeCalledWith('tmpPassword', undefined);
          expect(crypto.randomBytes).toBeCalledWith(16);
        });
      });
    });

    describe('post', () => {
      describe('init', () => {
        let validate;

        beforeEach(() => {
          [, validate] = postHooks.find(([name]) => name === 'init');
        });

        it('should set the tmpPassword', () => {
          const context = {
            get: jest.fn(),
            set: jest.fn(),
          };

          const password = 'some-password';
          context.get.mockReturnValue(password);

          expect(validate.call(context)).toBeUndefined();

          expect(context.set).toBeCalledWith('tmpPassword', password);
          expect(context.get).toBeCalledWith('password');
        });
      });
    });
  });

  describe('methods', () => {
    describe('#addProvider', () => {
      let context: any;
      beforeEach(() => {
        context = {
          set: jest.fn(),
        };
      });

      it('should add a provider', () => {
        const providerUser = { type: 'type3', userId: 'some-user-id3', tokens: { hello: 'world' } };

        expect(UserSchema.methods.addProvider.call(context, providerUser)).toBeUndefined();

        expect(context.set).toBeCalledWith(`providers.${providerUser.type}`, providerUser);
      });
    });

    describe('#getProvider', () => {
      it('should return the provider by type', () => {
        const providerId = 'some-type';
        const providers = {
          [providerId]: {
            type: providerId,
          },
          'some-other-type': {
            type: 'some-other-type',
          },
        };

        const context = {
          get: jest.fn().mockReturnValue(providers[providerId]),
        };

        expect(UserSchema.methods.getProvider.call(context, providerId)).toBe(
          providers[providerId],
        );
      });
    });

    describe('#removeProvider', () => {
      it('should error if provider not registered', () => {
        const providers = new Map();
        const context = {
          get: jest.fn().mockReturnValue(providers),
        };
        const providerType = 'some-provider';

        try {
          UserSchema.methods.removeProvider.call(context, providerType);
          throw new Error('invalid');
        } catch (err) {
          expect(err).toEqual(new NotFoundException('Provider not registered'));
        }
      });

      it('should error if deleting last provider', () => {
        const providerType = 'some-provider';
        const providers = new Map([[providerType, { type: providerType }]]);
        const context = {
          get: jest.fn().mockReturnValue(providers),
        };

        try {
          UserSchema.methods.removeProvider.call(context, providerType);
          throw new Error('invalid');
        } catch (err) {
          expect(err).toEqual(new BadRequestException('Minimum of 1 provider must be registered'));
        }
      });

      it('should delete the provider', () => {
        const providerType = 'some-provider2';
        const providers = new Map([
          [providerType, { type: providerType }],
          ['some-other', { type: 'some-other' }],
        ]);
        const context = {
          get: jest.fn().mockReturnValue(providers),
        };

        expect(UserSchema.methods.removeProvider.call(context, providerType)).toBe(context);
      });
    });

    describe('#toPublic', () => {
      it('should call toObject and delete the providers', () => {
        const user = {
          hello: 'world',
          providers: {},
        };

        const context = {
          toObject: jest.fn().mockReturnValue(user),
        };

        const cleanUser = {
          ...user,
        };
        delete cleanUser.providers;

        expect(UserSchema.methods.toPublic.call(context)).toEqual(cleanUser);

        expect(context.toObject).toBeCalledWith();
      });
    });

    describe('#verifyPassword', () => {
      it('should verify a password', async () => {
        jest.spyOn(crypto, 'scrypt');
        jest.spyOn(crypto, 'timingSafeEqual');

        const derivedKey = 'some-derived-key';
        const inputPassword = 'some-password';
        const salt = 'some-salt';
        const key = 'some-key';
        const data = [salt, key].join(':');
        const response = 'some-response';

        (<any>crypto).timingSafeEqual.mockReturnValue(response);
        (<any>crypto).scrypt.mockImplementation((password, saltValue, hashKeyLength, cb) => {
          expect(password).toBe(inputPassword);
          expect(saltValue).toBe(salt);
          expect(hashKeyLength).toBe(64);

          cb(null, derivedKey);
        });

        const context = {
          get: jest.fn(),
        };
        when(context.get).calledWith('password').mockReturnValue(data);

        expect(await UserSchema.methods.verifyPassword.call(context, inputPassword)).toBe(response);
        expect(crypto.timingSafeEqual).toBeCalledWith(Buffer.from(key, 'hex'), derivedKey);
      });
    });
  });

  describe('#toJSON', () => {
    it('should remove _id from the output', () => {
      expect(UserSchema.set).toBeCalledWith(
        'toJSON',
        expect.objectContaining({
          virtuals: true,
          versionKey: false,
        }),
      );

      const [, { transform }] = (<any>UserSchema).set.mock.calls.find(([key]) => key === 'toJSON');

      const doc = 'some-doc';
      const ret = {
        hello: 'world',
        _id: 'hello',
        password: 'some-pass',
        tmpPassword: 'some-other',
      };
      expect(transform(doc, ret)).toEqual({
        ...ret,
        _id: undefined,
        password: undefined,
        tmpPassword: undefined,
      });
    });
  });

  describe('#toObject', () => {
    it('should remove _id from the output', () => {
      expect(UserSchema.set).toBeCalledWith(
        'toObject',
        expect.objectContaining({
          virtuals: true,
          versionKey: false,
        }),
      );

      const [, { transform }] = (<any>UserSchema).set.mock.calls.find(
        ([key]) => key === 'toObject',
      );

      const doc = 'some-doc';
      const ret = {
        hello: 'world',
        _id: 'hello',
        password: 'some-pass',
        tmpPassword: 'some-other',
      };
      expect(transform(doc, ret)).toEqual({
        ...ret,
        _id: undefined,
        password: undefined,
        tmpPassword: undefined,
      });
    });
  });
});
