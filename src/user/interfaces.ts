import { IProviderUserData } from '../provider/interfaces';

export interface IUserQueryStrings {
  [key: string]: any;
}

export interface IUser {
  _id: string;
  name: string;
  emailAddress: string;
  username: string;
  password?: string;
  confirmed: boolean;
  registered: boolean;
  providers: {
    [type: string]: IProviderUserData;
  };
  createdAt: Date;
  updatedAt: Date;
}

export enum UserRoles {
  USER_ADMIN = 'USER_ADMIN',
}

export enum UserSubjects {
  User = 'USER',
}
