import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { IProviderUserData } from '../provider/interfaces';

export type ProviderUserDocument = ProviderUser & Document;

@Schema({
  _id: false,
})
export class ProviderUser implements IProviderUserData {
  /* Tokens are an object literal - just put in whatever is sent through */
  @Prop({
    type: Object,
    required: true,
  })
  tokens: object;

  @Prop({
    type: String,
    required: true,
    enum: [],
    index: true,
  })
  type: string;

  @Prop({
    type: String,
    required: true,
    index: true,
  })
  userId: string;

  /* Provider may not supply any of these keys */
  @Prop({
    type: String,
  })
  emailAddress: string;

  @Prop({
    type: String,
  })
  name: string;

  @Prop({
    type: String,
  })
  username: string;
}

export const ProviderUserSchema = SchemaFactory.createForClass(ProviderUser);
