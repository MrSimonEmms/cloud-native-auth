import * as qs from 'querystring';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Inject,
  NotFoundException,
  Patch,
  Post,
  Query,
  Redirect,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiBody, ApiOperation, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

import UserService from './user.service';
import User from '../decorators/user.decorator';
import { IUserQueryStrings } from './interfaces';
import { User as UserSchema } from './user.schema';
import LoginDto from './login.dto';

@Controller('user')
export default class UserController {
  @Inject(ConfigService)
  private readonly config;

  @Inject(UserService)
  private readonly userService: UserService;

  @Inject(JwtService)
  private readonly jwtService: JwtService;

  private checkPasswordEnabled() {
    if (!this.config.get('providers.password.enabled')) {
      throw new NotFoundException();
    }
  }

  @Get()
  @ApiBearerAuth('jwt')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 200, description: 'Return the user object', type: UserSchema })
  @ApiOperation({ tags: ['user'], summary: 'Return the user object' })
  getOne(@User() user) {
    return user;
  }

  @Post()
  @ApiResponse({
    status: 201,
    description: 'Create a user with a password',
    schema: {
      type: 'object',
      properties: {
        token: {
          type: 'string',
          example:
            '62f717e7b2102775647eb4630be48d6b:56e97690714939a7724667d886a5b337c899ebe85a19258a17ca6cf6c29311c2e0bf89b25a94bf8f51a44f4de21b654b44777fdb2e011c5ea71e6c84655a4626',
        },
      },
    },
  })
  @ApiOperation({ tags: ['user'], summary: 'Create a user with a password' })
  async createOne(@Body() data: UserSchema) {
    this.checkPasswordEnabled();

    const user = await this.userService.create(data);

    return {
      token: this.userService.getToken(user),
    };
  }

  @Delete()
  @ApiBearerAuth('jwt')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(204)
  @ApiResponse({ status: 204, description: 'User successfully deleted' })
  @ApiOperation({ tags: ['user'], summary: 'Delete the user' })
  async deleteOne(@User() user) {
    const deleted = await this.userService.deleteById(user.id);

    if (!deleted) {
      /* Technically impossible, but a fail-safe */
      throw new NotFoundException();
    }
  }

  @Patch('/')
  @ApiBearerAuth('jwt')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 200, description: 'Update the user object', type: UserSchema })
  @ApiBody({ type: UserSchema })
  @ApiOperation({ tags: ['user'], summary: 'Update the user object' })
  async updateOne(@User() user, @Body() body: Partial<UserSchema>) {
    await this.userService.updateUser(user.id, body);

    return this.userService.findById(user.id);
  }

  @Get('/confirm')
  @Redirect()
  @ApiOperation({ tags: ['user'], summary: 'Confirm the user and email address' })
  @ApiQuery({
    name: 'token',
    description: 'Token',
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImQwNTBlNmU3LWY5NDMtNGFiOS1iYTRlLTY0ZjIyODdmMjI3YyIsImVtYWlsQWRkcmVzcyI6InRlc3RAdGVzdC5jb20iLCJpYXQiOjE2MjA0ODk4MDgsIm5iZiI6MTYyMDQ4OTgwOCwiZXhwIjoxNjIzMDgxODA4LCJpc3MiOiJpc3N1ZXItbmFtZSJ9.6UBsHyq8v1Gm7VI4djSFO0Lt4n0Y-vsqxuapB39dWzw',
  })
  @ApiResponse({ status: 302, description: 'Dispatch to the successfully confirmed page' })
  async confirmEmail(@Req() req, @Query('token') token: string) {
    const confirmEmail = this.config.get('hooks.confirmEmail');
    if (!confirmEmail.enabled) {
      req.log.error('Confirm email is disabled');
      throw new NotFoundException();
    }

    let responseQueryParams: IUserQueryStrings;

    try {
      req.log.info({ token }, 'Verifying confirm email token');
      const data = this.jwtService.verify(token);

      const confirmed = await this.userService.confirmEmail(data.id, data.emailAddress);

      responseQueryParams = { confirmed };
    } catch (err) {
      req.log.error({ err }, 'Unable to confirm email');

      responseQueryParams = {
        error: /* istanbul ignore next */ err?.response?.code ?? 'INVALID_CONFIRM_EMAIL_TOKEN',
      };
    }

    return {
      statusCode: 302,
      url: `${confirmEmail.redirectUrl}?${qs.stringify(responseQueryParams)}`,
    };
  }

  @Post('/login')
  @ApiResponse({
    status: 200,
    description: 'Login with a password',
    schema: {
      type: 'object',
      properties: {
        token: {
          type: 'string',
          example:
            '62f717e7b2102775647eb4630be48d6b:56e97690714939a7724667d886a5b337c899ebe85a19258a17ca6cf6c29311c2e0bf89b25a94bf8f51a44f4de21b654b44777fdb2e011c5ea71e6c84655a4626',
        },
      },
    },
  })
  @ApiOperation({ tags: ['user'], summary: 'Login with a password' })
  async login(@Body() body: LoginDto) {
    this.checkPasswordEnabled();

    const user = await this.userService.login(body);

    return {
      token: this.userService.getToken(user),
    };
  }
}
