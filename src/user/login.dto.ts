import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export default class LoginDto {
  @ApiPropertyOptional({
    example: 'test@test.com',
    description: 'Email address to login with - email address or username required',
  })
  emailAddress?: string;

  @ApiPropertyOptional({
    example: 'username',
    description: 'Username to login with - email address or username required',
  })
  username?: string;

  @ApiProperty({
    example: 'password',
    description: 'Password to login with',
  })
  password: string;
}
