import * as qs from 'querystring';
import { Test, TestingModule } from '@nestjs/testing';
import {
  HTTP_CODE_METADATA,
  METHOD_METADATA,
  PATH_METADATA,
  REDIRECT_METADATA,
} from '@nestjs/common/constants';
import { NotFoundException, RequestMethod } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { when } from 'jest-when';
import UserController from './user.controller';
import { User as UserSchema } from './user.schema';
import UserService from './user.service';
import LoginDto from './login.dto';

describe('UserController', () => {
  let controller: UserController;
  let userService: any;
  let configService: any;
  let jwtService: any;

  beforeEach(async () => {
    configService = {
      get: jest.fn(),
    };

    jwtService = {
      verify: jest.fn(),
    };

    userService = {
      confirmEmail: jest.fn(),
      create: jest.fn(),
      deleteById: jest.fn(),
      getToken: jest.fn(),
      findById: jest.fn(),
      updateUser: jest.fn(),
      login: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        { provide: ConfigService, useValue: configService },
        { provide: JwtService, useValue: jwtService },
        { provide: UserService, useValue: userService },
      ],
    }).compile();

    controller = module.get<UserController>(UserController);
  });

  describe('#createOne', () => {
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.createOne)).toEqual('/');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.createOne)).toEqual(
        RequestMethod.POST,
      );
    });

    it('should return not found error if password disabled', async () => {
      when(configService.get).calledWith('providers.password.enabled').mockReturnValue(false);
      const body = {};

      try {
        await controller.createOne(body as UserSchema);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new NotFoundException());
      }

      expect(userService.create).not.toBeCalled();
      expect(userService.getToken).not.toBeCalled();
    });

    it('should dispatch to service if password enabled', async () => {
      when(configService.get).calledWith('providers.password.enabled').mockReturnValue(true);

      const token = 'some-token';
      const body: any = { some: 'input' };
      const user = 'some-user';

      userService.create.mockResolvedValue(user);
      userService.getToken.mockReturnValue(token);

      expect(await controller.createOne(body as UserSchema)).toEqual({
        token,
      });

      expect(userService.create).toBeCalledWith(body);
      expect(userService.getToken).toBeCalledWith(user);
    });
  });

  describe('#getOne', () => {
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.getOne)).toEqual('/');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.getOne)).toEqual(RequestMethod.GET);
    });

    it('should return the user', () => {
      const user = 'some-user';
      expect(controller.getOne(user)).toBe(user);
    });
  });

  describe('#deleteOne', () => {
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.deleteOne)).toEqual('/');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.deleteOne)).toEqual(
        RequestMethod.DELETE,
      );
      expect(Reflect.getMetadata(HTTP_CODE_METADATA, controller.deleteOne)).toEqual(204);
    });

    it('should throw an exception if user not deleted', async () => {
      const user = { id: 'some-id2' };
      userService.deleteById.mockResolvedValue(false);

      try {
        expect(await controller.deleteOne(user)).toBeUndefined();
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new NotFoundException());
      }

      expect(userService.deleteById).toBeCalledWith(user.id);
      expect(userService.deleteById).toBeCalledWith(user.id);
    });

    it('should return undefined if user delete', async () => {
      const user = { id: 'some-id' };
      userService.deleteById.mockResolvedValue(true);

      expect(await controller.deleteOne(user)).toBeUndefined();

      expect(userService.deleteById).toBeCalledWith(user.id);
    });
  });

  describe('#updateOne', () => {
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.updateOne)).toEqual('/');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.updateOne)).toEqual(
        RequestMethod.PATCH,
      );
    });

    it('should update the user', async () => {
      const user = { id: 'some-id' };
      const body: any = 'some-body';
      const result = 'some-result';

      userService.findById.mockResolvedValue(result);

      expect(await controller.updateOne(user, body)).toEqual(result);

      expect(userService.updateUser).toBeCalledWith(user.id, body);
      expect(userService.findById).toBeCalledWith(user.id);
    });
  });

  describe('#confirmEmail', () => {
    let req;
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.confirmEmail)).toEqual('/confirm');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.confirmEmail)).toEqual(
        RequestMethod.GET,
      );
      expect(Reflect.getMetadata(REDIRECT_METADATA, controller.confirmEmail)).toEqual({
        statusCode: undefined,
        url: '',
      });

      req = {
        log: {
          error: jest.fn(),
          info: jest.fn(),
        },
      };
    });

    it('should throw a NotFoundException if confirmEmail hook is not enabled', async () => {
      when(configService.get).calledWith('hooks.confirmEmail').mockReturnValue({
        enabled: false,
      });

      try {
        await controller.confirmEmail(req, '');
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new NotFoundException());
      }
    });

    it('should redirect to an error page if token invalid', async () => {
      const confirmEmail = {
        enabled: true,
        redirectUrl: 'some-redirect-url',
      };
      const token = 'some-token';
      const error = new Error('some-error');
      (<any>error).response = { code: 'some-code' };

      when(configService.get).calledWith('hooks.confirmEmail').mockReturnValue(confirmEmail);

      jwtService.verify.mockImplementation(() => {
        throw error;
      });

      expect(await controller.confirmEmail(req, token)).toEqual({
        statusCode: 302,
        url: `${confirmEmail.redirectUrl}?${qs.stringify({ error: (<any>error).response.code })}`,
      });

      expect(jwtService.verify).toBeCalledWith(token);
      expect(userService.confirmEmail).not.toBeCalled();
    });

    it('should redirect to an error page if token invalid - default error', async () => {
      const confirmEmail = {
        enabled: true,
        redirectUrl: 'some-redirect-url',
      };
      const token = 'some-token';
      const error = new Error('some-error');

      when(configService.get).calledWith('hooks.confirmEmail').mockReturnValue(confirmEmail);

      jwtService.verify.mockImplementation(() => {
        throw error;
      });

      expect(await controller.confirmEmail(req, token)).toEqual({
        statusCode: 302,
        url: `${confirmEmail.redirectUrl}?${qs.stringify({
          error: 'INVALID_CONFIRM_EMAIL_TOKEN',
        })}`,
      });

      expect(jwtService.verify).toBeCalledWith(token);
      expect(userService.confirmEmail).not.toBeCalled();
    });

    it('should redirect to an error page if confirmEmail call fails', async () => {
      const confirmEmail = {
        enabled: true,
        redirectUrl: 'some-redirect-url',
      };
      const token = 'some-token';
      const data = {
        id: 'some-id',
        emailAddress: 'some-email-address',
      };
      const error = new Error('some-error');
      (<any>error).response = { code: 'some-code2' };

      when(configService.get).calledWith('hooks.confirmEmail').mockReturnValue(confirmEmail);

      jwtService.verify.mockReturnValue(data);
      userService.confirmEmail.mockImplementation(() => {
        throw error;
      });

      expect(await controller.confirmEmail(req, token)).toEqual({
        statusCode: 302,
        url: `${confirmEmail.redirectUrl}?${qs.stringify({ error: (<any>error).response.code })}`,
      });

      expect(jwtService.verify).toBeCalledWith(token);
      expect(userService.confirmEmail).toBeCalledWith(data.id, data.emailAddress);
    });

    it('should redirect to an error page if confirmEmail call fails - default error', async () => {
      const confirmEmail = {
        enabled: true,
        redirectUrl: 'some-redirect-url',
      };
      const token = 'some-token';
      const data = {
        id: 'some-id',
        emailAddress: 'some-email-address',
      };
      const error = new Error('some-error');

      when(configService.get).calledWith('hooks.confirmEmail').mockReturnValue(confirmEmail);

      jwtService.verify.mockReturnValue(data);
      userService.confirmEmail.mockImplementation(() => {
        throw error;
      });

      expect(await controller.confirmEmail(req, token)).toEqual({
        statusCode: 302,
        url: `${confirmEmail.redirectUrl}?${qs.stringify({
          error: 'INVALID_CONFIRM_EMAIL_TOKEN',
        })}`,
      });

      expect(jwtService.verify).toBeCalledWith(token);
      expect(userService.confirmEmail).toBeCalledWith(data.id, data.emailAddress);
    });

    it('should redirect to a success page if passes', async () => {
      const confirmEmail = {
        enabled: true,
        redirectUrl: 'some-redirect-url2',
      };
      const token = 'some-token2';
      const data = {
        id: 'some-id2',
        emailAddress: 'some-email-address2',
      };

      when(configService.get).calledWith('hooks.confirmEmail').mockReturnValue(confirmEmail);

      jwtService.verify.mockReturnValue(data);
      userService.confirmEmail.mockReturnValue(true);

      expect(await controller.confirmEmail(req, token)).toEqual({
        statusCode: 302,
        url: `${confirmEmail.redirectUrl}?${qs.stringify({ confirmed: true })}`,
      });

      expect(jwtService.verify).toBeCalledWith(token);
      expect(userService.confirmEmail).toBeCalledWith(data.id, data.emailAddress);
    });
  });

  describe('#login', () => {
    beforeEach(() => {
      expect(Reflect.getMetadata(PATH_METADATA, controller.login)).toEqual('/login');
      expect(Reflect.getMetadata(METHOD_METADATA, controller.login)).toEqual(RequestMethod.POST);
    });

    it('should return not found error if password disabled', async () => {
      when(configService.get).calledWith('providers.password.enabled').mockReturnValue(false);
      const body = {};

      try {
        await controller.login(body as LoginDto);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new NotFoundException());
      }

      expect(userService.login).not.toBeCalled();
      expect(userService.getToken).not.toBeCalled();
    });

    it('should dispatch to service if password enabled', async () => {
      when(configService.get).calledWith('providers.password.enabled').mockReturnValue(true);

      const token = 'some-token';
      const body: any = { some: 'input' };
      const user = 'some-user';

      userService.login.mockResolvedValue(user);
      userService.getToken.mockReturnValue(token);

      expect(await controller.login(body as LoginDto)).toEqual({
        token,
      });

      expect(userService.login).toBeCalledWith(body);
      expect(userService.getToken).toBeCalledWith(user);
    });
  });
});
