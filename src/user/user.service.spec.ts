import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { PinoLogger } from 'nestjs-pino';
import { JwtService } from '@nestjs/jwt';
import { BadRequestException, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { when } from 'jest-when';
import UserService from './user.service';
import { User, UserDocument } from './user.schema';
import { IProviderUserData } from '../provider/interfaces';
import { IUser } from './interfaces';
import OrganizationService from '../organization/organization.service';

jest.mock('nestjs-pino');
jest.mock('@nestjs/jwt');

describe('UserService', () => {
  let service: UserService;
  let UserModel: any;
  let orgService: any;
  let configService: any;

  beforeEach(async () => {
    UserModel = jest.fn();
    UserModel.deleteOne = jest.fn();
    UserModel.findById = jest.fn();
    UserModel.findOne = jest.fn();
    UserModel.updateOne = jest.fn();

    orgService = {
      removeUser: jest.fn(),
    };

    configService = {
      get: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getModelToken(User.name),
          useValue: UserModel,
        },
        {
          provide: PinoLogger,
          useClass: PinoLogger,
        },
        {
          provide: JwtService,
          useClass: JwtService,
        },
        {
          provide: OrganizationService,
          useValue: orgService,
        },
        {
          provide: ConfigService,
          useValue: configService,
        },
      ],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  describe('#addProviderToUser', () => {
    beforeEach(() => {
      service.deleteById = jest.fn();
      service.findById = jest.fn();
      service.findByProviderUserId = jest.fn();
    });

    describe('linking to existing user', () => {
      it('should throw error if user not found', async () => {
        (<any>service).findById.mockResolvedValue(undefined);
        const linkUser = '1234';

        try {
          await service.addProviderToUser({} as IProviderUserData, linkUser);
          throw new Error('invalid');
        } catch (err) {
          expect(service.findById).toBeCalledWith(linkUser);

          expect(err).toEqual(
            new NotFoundException({
              code: 'UNKNOWN_USER',
              message: 'Unknown user',
            }),
          );
        }
      });

      it('should throw error if provider set to other registered user', async () => {
        const user = { id: 'some-id' };
        (<any>service).findById.mockResolvedValue(user);
        const otherUser = { registered: true };
        (<any>service).findByProviderUserId.mockResolvedValue(otherUser);
        const linkUser = '1234';
        const providerUser: any = 'some-provider-user';

        try {
          await service.addProviderToUser(providerUser, linkUser);
          throw new Error('invalid');
        } catch (err) {
          expect(service.findById).toBeCalledWith(linkUser);
          expect(service.findByProviderUserId).toBeCalledWith(providerUser, user.id);

          expect(err).toEqual(
            new BadRequestException({
              code: 'PROVIDER_REGISTERED',
              message: 'Provider registered with other user',
            }),
          );
        }
      });

      it('should delete the other user if linked to unregistered user and add new provider', async () => {
        const user = {
          id: 'some-id2',
          addProvider: jest.fn(),
          save: jest.fn(),
        };
        (<any>service).findById.mockResolvedValue(user);
        (<any>service).findById.mockResolvedValue(user);
        const otherUserId = 'some-other-user-id2';
        const otherUser = {
          registered: false,
          get: jest.fn().mockReturnValue(otherUserId),
        };
        (<any>service).findByProviderUserId.mockResolvedValue(otherUser);
        const linkUser = '1235';
        const providerUser: any = 'some-provider-user2';

        expect(await service.addProviderToUser(providerUser, linkUser)).toBe(user);

        expect(service.findById(linkUser));
        expect(service.findByProviderUserId).toBeCalledWith(providerUser, user.id);
        expect(service.deleteById).toBeCalledWith(otherUserId);

        expect(user.addProvider).toBeCalledWith(providerUser);
        expect(user.save).toBeCalledWith({ validateBeforeSave: false });
      });

      it('should add a new provider', async () => {
        const user = {
          id: 'some-id3',
          addProvider: jest.fn(),
          save: jest.fn(),
        };
        (<any>service).findById.mockResolvedValue(user);
        (<any>service).findById.mockResolvedValue(user);
        (<any>service).findByProviderUserId.mockResolvedValue(undefined);
        const linkUser = '1236';
        const providerUser: any = 'some-provider-user3';

        expect(await service.addProviderToUser(providerUser, linkUser)).toBe(user);

        expect(service.findById(linkUser));
        expect(service.findByProviderUserId).toBeCalledWith(providerUser, user.id);
        expect(service.deleteById).not.toBeCalled();

        expect(user.addProvider).toBeCalledWith(providerUser);
        expect(user.save).toBeCalledWith({ validateBeforeSave: false });
      });
    });

    describe('no user linking', () => {
      it('should get existing user if provider registration found', async () => {
        const user = {
          id: 'some-id2',
          addProvider: jest.fn(),
          save: jest.fn(),
        };
        UserModel.mockReturnValue(user);
        const providerUser: any = 'some-provider-use2';

        (<any>service).findByProviderUserId.mockResolvedValue(user);

        expect(await service.addProviderToUser(providerUser)).toBe(user);

        expect(user.addProvider).toBeCalledWith(providerUser);
        expect(user.save).toBeCalledWith({ validateBeforeSave: false });
      });

      it('should create a new user if provider registration not found', async () => {
        const user = {
          id: 'some-id',
          addProvider: jest.fn(),
          save: jest.fn(),
        };
        UserModel.mockReturnValue(user);
        const providerUser: any = 'some-provider-user';

        expect(await service.addProviderToUser(providerUser)).toBe(user);

        expect(user.addProvider).toBeCalledWith(providerUser);
        expect(user.save).toBeCalledWith({ validateBeforeSave: false });
      });
    });
  });

  describe('#confirmEmail', () => {
    it('should not match any records and throw an error', async () => {
      const id = 'some-id';
      const emailAddress = 'some-email';

      UserModel.updateOne.mockResolvedValue({
        n: 0,
      });

      try {
        await service.confirmEmail(id, emailAddress);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(
          new BadRequestException({
            code: 'UNABLE_TO_CONFIRM_EMAIL',
            message: 'Unable to confirm email address',
          }),
        );

        expect(UserModel.updateOne).toBeCalledWith(
          {
            _id: id,
            emailAddress,
            confirmed: false,
          },
          {
            $set: {
              confirmed: true,
            },
          },
        );
      }
    });

    it('should match a record and return true', async () => {
      const id = 'some-id2';
      const emailAddress = 'some-email2';

      UserModel.updateOne.mockResolvedValue({
        n: 1,
      });

      expect(await service.confirmEmail(id, emailAddress)).toBe(true);

      expect(UserModel.updateOne).toBeCalledWith(
        {
          _id: id,
          emailAddress,
          confirmed: false,
        },
        {
          $set: {
            confirmed: true,
          },
        },
      );
    });
  });

  describe('#create', () => {
    it('should throw a BadRequest error if validation failed', async () => {
      const body: any = {
        _id: 'some-id',
        registered: true,
        confirmed: false,
        name: 'some-name',
        password: 'some-password',
        providers: 'some-providers',
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      const validationErrors = { errors: 'some-error' };
      const document = {
        validate: jest.fn().mockRejectedValue(validationErrors),
      };
      UserModel.mockReturnValue(document);

      try {
        await service.create(body);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new BadRequestException(validationErrors.errors));

        const expected = {
          ...body,
        };
        // eslint-disable-next-line no-underscore-dangle
        delete expected._id;
        delete expected.registered;
        delete expected.confirmed;
        delete expected.providers;
        delete expected.createdAt;
        delete expected.updatedAt;

        expect(UserModel).toBeCalledWith(expected);
      }
    });

    it('should save a new user', async () => {
      const body: any = {
        _id: 'some-id',
        registered: true,
        confirmed: false,
        name: 'some-name',
        password: 'some-password',
        providers: 'some-providers',
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      const document = {
        save: jest.fn(),
        validate: jest.fn().mockResolvedValue({}),
      };
      UserModel.mockReturnValue(document);

      expect(await service.create(body)).toBe(document);

      const expected = {
        ...body,
      };
      // eslint-disable-next-line no-underscore-dangle
      delete expected._id;
      delete expected.registered;
      delete expected.confirmed;
      delete expected.providers;
      delete expected.createdAt;
      delete expected.updatedAt;

      expect(UserModel).toBeCalledWith(expected);

      expect(document.save).toBeCalledWith({ validateBeforeSave: false });
    });
  });

  describe('#deleteById', () => {
    it('should return true if user deleted', async () => {
      const id = 'some-id';

      UserModel.deleteOne.mockResolvedValue({ deletedCount: 1 });

      expect(await service.deleteById(id)).toEqual(true);

      expect(UserModel.deleteOne).toBeCalledWith({ _id: id });
      expect(orgService.removeUser).toBeCalledWith(id);
    });

    it('should return false if user not deleted', async () => {
      const id = 'some-id2';

      UserModel.deleteOne.mockResolvedValue({ deletedCount: 2 });

      expect(await service.deleteById(id)).toEqual(false);

      expect(UserModel.deleteOne).toBeCalledWith({ _id: id });
      expect(orgService.removeUser).toBeCalledWith(id);
    });
  });

  describe('#findById', () => {
    it('should return the response', async () => {
      const id = 'some-id';
      const result = 'some-result';

      UserModel.findById.mockResolvedValue(result);

      expect(await service.findById(id)).toBe(result);

      expect(UserModel.findById).toBeCalledWith(id);
    });
  });

  describe('#findByProviderUserId', () => {
    it('should find by provider type and user ID', async () => {
      const provider = {
        type: 'some-type',
        userId: 'some-id',
      } as IProviderUserData;

      const result = 'some-result';

      UserModel.findOne.mockResolvedValue(result);

      expect(await service.findByProviderUserId(provider)).toBe(result);

      expect(UserModel.findOne).toBeCalledWith({
        [`providers.${provider.type}.userId`]: provider.userId,
      });
    });

    it('should find by provider type and user ID and not by _id', async () => {
      const provider = {
        type: 'some-type',
        userId: 'some-id',
      } as IProviderUserData;
      const excludingId = 'excludingId';

      const result = 'some-result';

      UserModel.findOne.mockResolvedValue(result);

      expect(await service.findByProviderUserId(provider, excludingId)).toBe(result);

      expect(UserModel.findOne).toBeCalledWith({
        [`providers.${provider.type}.userId`]: provider.userId,
        _id: {
          $ne: excludingId,
        },
      });
    });
  });

  describe('#getPublicLoginQueryStrings', () => {
    it('should generate a token object', () => {
      const token = 'some-token';
      service.getToken = jest.fn().mockReturnValue(token);
      const user = { id: 'some-id' } as UserDocument;

      expect(service.getPublicLoginQueryStrings(user)).toEqual({
        token: Buffer.from(token).toString('base64'),
      });

      expect(service.getToken).toBeCalledWith(user);
    });
  });

  describe('#getToken', () => {
    it('should return a jwt signed token', () => {
      const user = {
        get: jest.fn(),
      };
      const userId = 'some-id';
      const token = 'some-token';

      user.get.mockReturnValue(userId);
      (<any>service).jwt.sign.mockReturnValue(token);

      expect(service.getToken(user as any)).toBe(token);

      expect((<any>service).jwt.sign).toBeCalledWith({
        id: userId,
      });
    });
  });

  describe('#login', () => {
    it('should error if no email or username provided', async () => {
      const input = {
        password: '11111',
      };

      try {
        await service.login(input);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new UnauthorizedException());
      }
    });

    it('should error if user not found with email', async () => {
      const input = {
        emailAddress: 'some-email',
        password: 'some-password',
      };
      UserModel.findOne.mockResolvedValue(undefined);

      try {
        await service.login(input);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new UnauthorizedException());
      }

      expect(UserModel.findOne).toBeCalledWith({
        emailAddressLower: input.emailAddress.toLowerCase(),
      });
    });

    it('should error if user not found with user', async () => {
      const input = {
        username: 'some-username',
        password: 'some-password',
      };
      UserModel.findOne.mockResolvedValue(undefined);

      try {
        await service.login(input);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new UnauthorizedException());
      }

      expect(UserModel.findOne).toBeCalledWith({
        usernameLower: input.username.toLowerCase(),
      });
    });

    it('should fail to verify password with email', async () => {
      const input = {
        emailAddress: 'some-emAIL2',
        password: 'some-passwORD2',
      };
      const user = {
        verifyPassword: jest.fn().mockResolvedValue(false),
      };
      UserModel.findOne.mockResolvedValue(user);

      try {
        await service.login(input);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new UnauthorizedException());
      }

      expect(UserModel.findOne).toBeCalledWith({
        emailAddressLower: input.emailAddress.toLowerCase(),
      });
      expect(user.verifyPassword).toBeCalledWith(input.password);
    });

    it('should fail to verify password with user', async () => {
      const input = {
        username: 'some-userNAME2',
        password: 'some-passwORD2',
      };
      const user = {
        verifyPassword: jest.fn().mockResolvedValue(false),
      };
      UserModel.findOne.mockResolvedValue(user);

      try {
        await service.login(input);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new UnauthorizedException());
      }

      expect(UserModel.findOne).toBeCalledWith({
        usernameLower: input.username.toLowerCase(),
      });
      expect(user.verifyPassword).toBeCalledWith(input.password);
    });

    it('should verify user with email', async () => {
      const input = {
        emailAddress: 'some-emAIL',
        password: 'some-password',
      };
      const user = {
        verifyPassword: jest.fn().mockResolvedValue(true),
      };
      UserModel.findOne.mockResolvedValue(user);

      expect(await service.login(input)).toBe(user);

      expect(UserModel.findOne).toBeCalledWith({
        emailAddressLower: input.emailAddress.toLowerCase(),
      });
      expect(user.verifyPassword).toBeCalledWith(input.password);
    });

    it('should verify user with user', async () => {
      const input = {
        username: 'some-userNAME',
        password: 'some-password2',
      };
      const user = {
        verifyPassword: jest.fn().mockResolvedValue(true),
      };
      UserModel.findOne.mockResolvedValue(user);

      expect(await service.login(input)).toBe(user);

      expect(UserModel.findOne).toBeCalledWith({
        usernameLower: input.username.toLowerCase(),
      });
      expect(user.verifyPassword).toBeCalledWith(input.password);
    });
  });

  describe('#removeProvider', () => {
    beforeEach(() => {});

    it('should dispatch to the model', async () => {
      const user: any = { removeProvider: jest.fn(), save: jest.fn() };
      const providerId = 'some-provider-id';
      expect(await service.removeProvider(user, providerId)).toBe(user);
      //
      expect(user.removeProvider).toBeCalledWith(providerId);
      expect(user.save).toBeCalledWith({ validateBeforeSave: false });
    });
  });

  describe('#updateUser', () => {
    beforeEach(() => {
      service.findById = jest.fn();

      when(configService.get).calledWith('providers.password.enabled').mockReturnValue(false);
    });

    it('should throw an NotFound error if user not found', async () => {
      const userId = 'some-user-id';
      const body: Partial<IUser> = {
        name: 'some-name',
        providers: {
          provider: {
            name: 'some-name',
          } as IProviderUserData,
        },
        createdAt: new Date(),
        updatedAt: new Date(),
      };

      (<any>service).findById.mockResolvedValue(undefined);

      try {
        await service.updateUser(userId, body);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new NotFoundException());

        expect(service.findById).toBeCalledWith(userId);
      }
    });

    it('should throw a BadRequest error if validation failed', async () => {
      const userId = 'some-user-id';
      const body: Partial<IUser> = {
        name: 'some-name',
        providers: {
          provider: {
            name: 'some-name',
          } as IProviderUserData,
        },
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      const validationErrors = { errors: 'some-error' };
      const document = {
        set: jest.fn(),
        validate: jest.fn().mockRejectedValue(validationErrors),
      };
      (<any>service).findById.mockResolvedValue(document);

      try {
        await service.updateUser(userId, body);
        throw new Error('invalid');
      } catch (err) {
        expect(err).toEqual(new BadRequestException(validationErrors.errors));

        expect(service.findById).toBeCalledWith(userId);

        const ignoreKeys = ['providers', 'createdAt', 'updatedAt'];

        Object.keys(body).forEach((key) => {
          if (ignoreKeys.includes(key)) {
            expect(document.set).not.toBeCalledWith(key, body[key]);
          } else {
            expect(document.set).toBeCalledWith(key, body[key]);
          }
        });
      }
    });

    it('should return document if saved', async () => {
      const userId = 'some-user-id';
      const body: Partial<IUser> = {
        _id: 'some-id',
        name: 'some-name2',
        confirmed: true,
        registered: true,
        emailAddress: 'some-email',
        username: 'some-username',
        password: 'some-password',
        providers: {
          provider: {
            name: 'some-name',
          } as IProviderUserData,
        },
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      const document = {
        save: jest.fn(),
        set: jest.fn(),
        validate: jest.fn().mockResolvedValue(undefined),
      };
      (<any>service).findById.mockResolvedValue(document);

      expect(await service.updateUser(userId, body)).toBe(document);

      expect(service.findById).toBeCalledWith(userId);

      const ignoreKeys = ['password', 'confirmed', 'providers', 'createdAt', 'updatedAt', '_id'];

      Object.keys(body).forEach((key) => {
        if (ignoreKeys.includes(key)) {
          expect(document.set).not.toBeCalledWith(key, body[key]);
        } else {
          expect(document.set).toBeCalledWith(key, body[key]);
        }
      });

      expect(document.save).toBeCalledWith({ validateBeforeSave: false });
    });

    it('should return document if saved with password enabled', async () => {
      when(configService.get).calledWith('providers.password.enabled').mockReturnValue(true);

      const userId = 'some-user-id';
      const body: Partial<IUser> = {
        _id: 'some-id',
        name: 'some-name2',
        confirmed: true,
        registered: true,
        emailAddress: 'some-email',
        username: 'some-username',
        password: 'some-password',
        providers: {
          provider: {
            name: 'some-name',
          } as IProviderUserData,
        },
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      const document = {
        save: jest.fn(),
        set: jest.fn(),
        validate: jest.fn().mockResolvedValue(undefined),
      };
      (<any>service).findById.mockResolvedValue(document);

      expect(await service.updateUser(userId, body)).toBe(document);

      expect(service.findById).toBeCalledWith(userId);

      const ignoreKeys = ['confirmed', 'providers', 'createdAt', 'updatedAt', '_id'];

      Object.keys(body).forEach((key) => {
        if (ignoreKeys.includes(key)) {
          expect(document.set).not.toBeCalledWith(key, body[key]);
        } else {
          expect(document.set).toBeCalledWith(key, body[key]);
        }
      });

      expect(document.save).toBeCalledWith({ validateBeforeSave: false });
    });
  });
});
