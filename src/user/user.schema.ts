import * as crypto from 'crypto';
import { promisify } from 'util';
import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as uuid from 'uuid';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import isEmail from 'validator/lib/isEmail';
import { ApiProperty } from '@nestjs/swagger';

import { ProviderUserSchema } from './providerUser.schema';
import { IProviderUserData } from '../provider/interfaces';
import { IUser } from './interfaces';

export type UserDocument = User & Document;

const lowercaseKeys = ['emailAddress', 'username'];

const hashJoinKey = ':';
const hashKeyLength = 64;

@Schema({
  timestamps: true,
})
export class User implements IUser {
  addProvider: Function;

  getProvider: Function;

  removeProvider: Function;

  toPublic: Function;

  verifyPassword: Function;

  @Prop({
    type: String,
    default: () => uuid.v4(),
  })
  @ApiProperty({
    name: 'id',
    example: 'e5b99a55-8b83-42c3-9895-c83287ae4810',
    description: 'Unique ID in UUIDv4 format',
  })
  _id: string;

  @Prop({
    type: String,
    required: true,
    maxlength: 200,
    default: null,
  })
  @ApiProperty({
    example: 'John Doe',
    description: 'Name of user',
  })
  name: string;

  @Prop({
    type: String,
    required: true,
    default: null,
    async validate(value: string) {
      if (!isEmail(value)) {
        throw new Error('Value is an invalid email address');
      }

      const user = await this.model(User.name)
        .findOne({ emailAddressLower: value })
        .where('_id')
        .nin([this.get('id')]);

      if (user !== null) {
        throw new Error('Email address already registered');
      }

      return true;
    },
  })
  @ApiProperty({
    example: 'john@doe.com',
    description: 'Email address of user',
  })
  emailAddress: string;

  @Prop({
    type: String,
    required: true,
    maxLength: 20,
    default: null,
    async validate(value: string) {
      /* Alphanumeric, hyphens, between 3 and 20 characters */
      if (!/^[\w-]{3,20}$/i.test(value)) {
        throw new Error('Value is an invalid username');
      }

      const user = await this.model(User.name)
        .findOne({ usernameLower: value })
        .where('_id')
        .nin([this.get('id')]);

      if (user !== null) {
        throw new Error('Username already registered');
      }

      return true;
    },
  })
  @ApiProperty({
    example: 'johndoe',
    description: 'Username of user',
  })
  username: string;

  @Prop({
    type: Boolean,
    default: false,
  })
  @ApiProperty({
    example: false,
    description: 'User has acknowledged confirmation email',
  })
  confirmed: boolean;

  @Prop({
    type: Boolean,
    default: false,
  })
  @ApiProperty({
    example: false,
    description: 'User registration is complete',
  })
  registered: boolean;

  @Prop({
    type: String,
    required() {
      return this.providers.size === 0;
    },
    minLength: 6,
  })
  @ApiProperty({
    example: 'password',
    description: 'Password of the user',
  })
  password?: string;

  @Prop({
    select: false,
    type: String,
  })
  tmpPassword?: string;

  @Prop(
    raw({
      type: Map,
      of: ProviderUserSchema,
      required: true,
      default: () => ({}),
      async validate(providers: Map<string, IProviderUserData>) {
        if (!this.password && providers.size === 0) {
          throw new Error('Must have at least 1 provider set');
        }

        return true;
      },
    }),
  )
  @ApiProperty({
    example: {
      github: {
        tokens: {
          accessToken: 'abc123',
        },
        type: 'github',
        userId: 12345,
        emailAddress: 'john@doe.com',
        name: 'John Doe',
        username: 'johndoe1',
      },
    },
    description: 'Providers used to authenticate the user',
  })
  providers: {
    [type: string]: IProviderUserData;
  };

  @Prop({
    select: false,
    type: String,
    lowercase: true,
    index: true,
  })
  emailAddressLower: string;

  @Prop({
    select: false,
    type: String,
    lowercase: true,
    index: true,
  })
  usernameLower: string;

  @Prop({
    type: Date,
    index: true,
  })
  @ApiProperty({
    example: new Date(),
    description: 'Created date',
  })
  createdAt: Date;

  @Prop({
    type: Date,
  })
  @ApiProperty({
    example: new Date(),
    description: 'Last updated date',
  })
  updatedAt: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.pre('validate', function preValidate() {
  lowercaseKeys.forEach((key) => {
    this.set(`${key}Lower`, this.get(key));
  });
});

UserSchema.pre('save', async function preSave() {
  const password = this.get('password');
  if (password !== this.get('tmpPassword')) {
    const salt = crypto.randomBytes(16).toString('hex');

    const hash = (
      await promisify<crypto.BinaryLike, crypto.BinaryLike, number, Buffer>(crypto.scrypt)(
        password,
        salt,
        hashKeyLength,
      )
    ).toString('hex');

    this.set('password', [salt, hash].join(hashJoinKey));
  }

  /* Unset the tmpPassword field */
  this.set('tmpPassword', undefined);
});

UserSchema.post('init', function postInit() {
  /* When loading from DB, set tmpPassword to watch for changes */
  this.set('tmpPassword', this.get('password'));
});

UserSchema.methods.addProvider = function addProvider(providerUser: IProviderUserData): void {
  this.set(`providers.${providerUser.type}`, providerUser);
};

UserSchema.methods.getProvider = function getProvider(providerType): IProviderUserData | void {
  return this.get(`providers.${providerType}`);
};

UserSchema.methods.removeProvider = function removeProvider(
  this: UserDocument,
  providerType: string,
): UserDocument {
  const providers = this.get('providers');

  if (!providers.has(providerType)) {
    throw new NotFoundException('Provider not registered');
  }

  /* Use "<=" as fail-safe, although length 0 will error as provider not registered */
  if (providers.size <= 1) {
    throw new BadRequestException('Minimum of 1 provider must be registered');
  }

  providers.delete(providerType);

  return this;
};

UserSchema.methods.toPublic = function toPublic(this: UserDocument): Omit<IUser, 'providers'> {
  const user = this.toObject();

  delete user.providers;

  return user;
};

UserSchema.methods.verifyPassword = async function verify(
  this: UserDocument,
  password: string,
): Promise<boolean> {
  const [salt, key] = this.get('password').split(hashJoinKey);
  const keyBuffer = Buffer.from(key, 'hex');

  const derivedKey = await promisify<string, string, number, Buffer>(crypto.scrypt)(
    password,
    salt,
    hashKeyLength,
  );

  return crypto.timingSafeEqual(keyBuffer, derivedKey);
};

['toJSON', 'toObject'].forEach((path) => {
  UserSchema.set(path, {
    virtuals: true,
    versionKey: false,
    transform: (doc, ret) => ({
      ...ret,
      _id: undefined,
      password: undefined,
      tmpPassword: undefined,
    }),
  });
});
