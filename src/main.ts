import 'source-map-support/register';

import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { PinoLogger } from 'nestjs-pino';
import * as cookieParser from 'cookie-parser';
import * as clientSessions from 'client-sessions';
import * as helmet from 'helmet';

import AppModule from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = app.get(ConfigService);
  const logger = await app.resolve(PinoLogger);
  const cookieSecret = config.get('cookies.secret');

  if (config.get<boolean>('server.swagger.enabled', false)) {
    logger.debug('Swagger enabled');
    const document = SwaggerModule.createDocument(
      app,
      new DocumentBuilder()
        .setTitle(process.env.npm_package_name)
        .setDescription(process.env.npm_package_description)
        .setVersion(process.env.npm_package_version)
        .setContact(
          process.env.npm_package_author_name,
          process.env.npm_package_author_url,
          process.env.npm_package_author_email,
        )
        .setLicense(
          process.env.npm_package_license,
          `https://opensource.org/licenses/${process.env.npm_package_license}`,
        )
        .addBearerAuth(
          {
            type: 'http',
            scheme: 'bearer',
            description: 'Authentication token',
            bearerFormat: 'JWT',
          },
          'jwt',
        )
        .build(),
    );
    SwaggerModule.setup('api', app, document);
  } else {
    logger.debug('Swagger disabled');
  }

  if (!cookieSecret) {
    logger.fatal({ cookieSecret: `${cookieSecret}` }, 'COOKIE_SECRET must be set');
    throw new Error('COOKIE_SECRET must be set');
  }

  app
    .use(
      clientSessions({
        cookieName: 'session',
        secret: cookieSecret,
      }),
    )
    .use(cookieParser(cookieSecret));

  if (config.get<boolean>('server.helmet.enabled')) {
    logger.debug('Helmet enabled');
    app.use(helmet());
  } else {
    logger.debug('Helmet disabled');
  }

  const port = config.get('server.port');

  if (config.get<boolean>('server.enableShutdownHooks')) {
    logger.debug('Enabling shutdown hooks');
    app.enableShutdownHooks();
  } else {
    logger.debug('Shutdown hooks not enabled');
  }

  logger.debug('Starting HTTP listener');
  await app.listen(port);
  logger.info({ port }, 'Application running');
}

bootstrap().catch((err) => {
  /* Unlikely to get to here but a final catchall */
  console.log(err.stack);
  process.exit(1);
});
