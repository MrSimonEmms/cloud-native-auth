import { PrometheusController } from '@willsoto/nestjs-prometheus';
import { Controller, Get, Res } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';

@Controller()
export default class MetricsController extends PrometheusController {
  @Get()
  @ApiOperation({ tags: ['monitoring'], summary: 'Output Prometheus metrics' })
  @ApiResponse({
    status: 200,

    description: 'Prometheus metrics output',
    content: {
      'text/html': {
        example: `# HELP process_cpu_user_seconds_total Total user CPU time spent in seconds.
# TYPE process_cpu_user_seconds_total counter
process_cpu_user_seconds_total 0.568884`,
      },
    },
  })
  index(@Res() response: unknown): Promise<void> {
    return super.index(response);
  }
}
