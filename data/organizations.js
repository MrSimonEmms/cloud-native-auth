module.exports = () => [
  {
    _id: 'org1-id',
    name: 'Org 1',
    slug: 'org1',
    users: [
      {
        role: 'ORG_MAINTAINER',
        userId: 'test',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ],
    __v: 0,
  },
  {
    _id: 'org2-id',
    name: 'Org 2',
    slug: 'org2',
    users: [
      {
        role: 'ORG_MAINTAINER',
        userId: 'test2',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        role: 'ORG_USER',
        userId: 'test',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ],
    __v: 0,
  },
];
