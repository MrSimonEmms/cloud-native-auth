import { authToken, request } from './setup';

describe('/user', () => {
  let app;
  let token: string;

  describe('/', () => {
    describe('DELETE', () => {
      beforeEach(async () => {
        token = `bearer ${await authToken()}`;
        app = (await request())().delete('/user');
      });

      it('should return 401 if no credentials provided', () => app.expect(401));

      it('should return 401 if invalid credentials provided', () =>
        app.set('authorization', 'bearer bad-token').expect(401));

      it('should not delete the user if owns an org', () =>
        app.set('authorization', token).expect(400, {
          statusCode: 400,
          message: 'Organizations will not be valid if user removed',
          organizations: ['org1-id'],
        }));

      it('should delete a user', async () => {
        token = await authToken('test3@test.com');

        return (await request())()
          .delete('/user')
          .set('authorization', `bearer ${token}`)
          .expect(204);
      });
    });

    describe('GET', () => {
      beforeEach(async () => {
        token = `bearer ${await authToken()}`;
        app = (await request())().get('/user');
      });

      it('should return 401 if no credentials provided', () => app.expect(401));

      it('should return 401 if invalid credentials provided', () =>
        app.set('authorization', 'bearer bad-token').expect(401));

      it('should return the logged in user', async () => {
        const { body } = await app.set('authorization', token).expect(200);

        expect(body).toEqual(
          expect.objectContaining({
            registered: true,
            confirmed: true,
            username: 'test',
            emailAddress: 'test@test.com',
            name: 'Test Testington',
            providers: {},
            id: 'test',
          }),
        );
      });
    });

    describe('POST', () => {
      beforeEach(async () => {
        token = `bearer ${await authToken()}`;
        app = (await request())().post('/user');
      });

      it('should fail validation', () =>
        app.expect(400, {
          password: {
            name: 'ValidatorError',
            message: 'Path `password` is required.',
            properties: {
              message: 'Path `password` is required.',
              type: 'required',
              path: 'password',
            },
            kind: 'required',
            path: 'password',
          },
          username: {
            name: 'ValidatorError',
            message: 'Path `username` is required.',
            properties: {
              message: 'Path `username` is required.',
              type: 'required',
              path: 'username',
              value: null,
            },
            kind: 'required',
            path: 'username',
            value: null,
          },
          emailAddress: {
            name: 'ValidatorError',
            message: 'Path `emailAddress` is required.',
            properties: {
              message: 'Path `emailAddress` is required.',
              type: 'required',
              path: 'emailAddress',
              value: null,
            },
            kind: 'required',
            path: 'emailAddress',
            value: null,
          },
          name: {
            name: 'ValidatorError',
            message: 'Path `name` is required.',
            properties: {
              message: 'Path `name` is required.',
              type: 'required',
              path: 'name',
              value: null,
            },
            kind: 'required',
            path: 'name',
            value: null,
          },
          providers: {
            name: 'ValidatorError',
            message: 'Must have at least 1 provider set',
            properties: {
              message: 'Must have at least 1 provider set',
              type: 'user defined',
              path: 'providers',
              value: {},
              reason: {},
            },
            kind: 'user defined',
            path: 'providers',
            value: {},
            reason: {},
          },
        }));

      it('should create a new user', async () => {
        const { body } = await app
          .send({
            name: 'some name',
            username: 'some-user',
            emailAddress: 'some@user.com',
            password: 'my-password',
          })
          .expect(201);

        /* Check it can be called */
        return app.set('authorization', `bearer ${body.token}`).expect(200);
      });
    });

    describe('PATCH', () => {
      beforeEach(async () => {
        token = `bearer ${await authToken()}`;
        app = (await request())().patch('/user');
      });

      it('should return 401 if no credentials provided', () => app.expect(401));

      it('should return 401 if invalid credentials provided', () =>
        app.set('authorization', 'bearer bad-token').expect(401));

      it('should fail validation', () =>
        app
          .set('authorization', token)
          .send({
            emailAddress: 'invalid',
          })
          .expect(400, {
            emailAddress: {
              name: 'ValidatorError',
              message: 'Value is an invalid email address',
              properties: {
                message: 'Value is an invalid email address',
                type: 'user defined',
                path: 'emailAddress',
                value: 'invalid',
                reason: {},
              },
              kind: 'user defined',
              path: 'emailAddress',
              value: 'invalid',
              reason: {},
            },
          }));

      it('should change a value', async () => {
        const { body } = await app
          .set('authorization', token)
          .send({
            emailAddress: 'some-new@email.com',
          })
          .expect(200);

        expect(new Date(body.updatedAt).getTime()).toBeGreaterThan(
          new Date(body.createdAt).getTime(),
        );

        expect(body).toEqual(
          expect.objectContaining({
            registered: true,
            confirmed: true,
            username: 'test',
            emailAddress: 'some-new@email.com',
            name: 'Test Testington',
            providers: {},
            id: 'test',
          }),
        );
      });
    });
  });

  describe('/login', () => {
    describe('POST', () => {
      beforeEach(async () => {
        app = (await request())().post('/user/login');
      });

      it('should return 401 if no credentials provided', () => app.send().expect(401));

      it('should return 401 if invalid credentials provided', () =>
        app.send({ emailAddress: 'test@test.com', password: 'bad' }).expect(401));

      it('should return user object if valid auth', async () => {
        const { body } = await app
          .send({ emailAddress: 'test@test.com', password: 'q1w2e3r4' })
          .expect(201);

        expect(body).toEqual(
          expect.objectContaining({
            token: expect.any(String),
          }),
        );
      });
    });
  });
});
