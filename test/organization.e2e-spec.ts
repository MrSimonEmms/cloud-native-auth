import { authToken, request } from './setup';

describe('/organization', () => {
  let app;
  let token: string;

  describe('/', () => {
    describe('GET', () => {
      beforeEach(async () => {
        token = `bearer ${await authToken()}`;
        app = (await request())().get('/organization');
      });

      it('should return 401 if no credentials provided', () => app.expect(401));

      it('should return 401 if invalid credentials provided', () =>
        app.set('authorization', 'bearer bad-token').expect(401));

      it('should return user org', async () => {
        const { body } = await app.set('authorization', token).expect(200);

        expect(body).toEqual({
          data: [
            expect.objectContaining({
              name: 'Org 1',
              slug: 'org1',
              users: [
                expect.objectContaining({
                  role: 'ORG_MAINTAINER',
                  userId: 'test',
                  name: 'Test Testington',
                  username: 'test',
                }),
              ],
              id: 'org1-id',
            }),
            expect.objectContaining({
              name: 'Org 2',
              slug: 'org2',
              users: [
                expect.objectContaining({
                  role: 'ORG_MAINTAINER',
                  userId: 'test2',
                  name: 'Test2 Testington',
                  username: 'test2',
                }),
                expect.objectContaining({
                  role: 'ORG_USER',
                  userId: 'test',
                  name: 'Test Testington',
                  username: 'test',
                }),
              ],
              id: 'org2-id',
            }),
          ],
          page: 1,
          limit: 25,
          count: 2,
          pages: 1,
          total: 2,
        });
      });

      it('should return a different user org', async () => {
        token = `bearer ${await authToken('test2@test.com')}`;

        const { body } = await app.set('authorization', token).expect(200);

        expect(body).toEqual({
          data: [
            expect.objectContaining({
              name: 'Org 2',
              slug: 'org2',
              users: [
                expect.objectContaining({
                  role: 'ORG_MAINTAINER',
                  userId: 'test2',
                  name: 'Test2 Testington',
                  username: 'test2',
                }),
                expect.objectContaining({
                  role: 'ORG_USER',
                  userId: 'test',
                  name: 'Test Testington',
                  username: 'test',
                }),
              ],
              id: 'org2-id',
            }),
          ],
          page: 1,
          limit: 25,
          count: 1,
          pages: 1,
          total: 1,
        });
      });
    });

    describe('POST', () => {
      beforeEach(async () => {
        token = `bearer ${await authToken()}`;
        app = (await request())().post('/organization');
      });

      it('should return 401 if no credentials provided', () => app.expect(401));

      it('should return 401 if invalid credentials provided', () =>
        app.set('authorization', 'bearer bad-token').expect(401));

      it('should throw a validation error', () =>
        app
          .set('authorization', token)
          .send({})
          .expect(400, {
            name: {
              name: 'ValidatorError',
              message: 'Path `name` is required.',
              properties: {
                message: 'Path `name` is required.',
                type: 'required',
                path: 'name',
              },
              kind: 'required',
              path: 'name',
            },
            slug: {
              name: 'ValidatorError',
              message: 'Path `slug` is required.',
              properties: {
                message: 'Path `slug` is required.',
                type: 'required',
                path: 'slug',
              },
              kind: 'required',
              path: 'slug',
            },
          }));

      it('should fail slug validation', () =>
        app
          .set('authorization', token)
          .send({
            name: 'Invalid',
            slug: 'org1',
          })
          .expect(400, {
            slug: {
              name: 'ValidatorError',
              message: 'Slug already registered',
              properties: {
                message: 'Slug already registered',
                type: 'user defined',
                path: 'slug',
                value: 'org1',
                reason: {},
              },
              kind: 'user defined',
              path: 'slug',
              value: 'org1',
              reason: {},
            },
          }));

      it('should create a new organization', async () => {
        const { body } = await app
          .set('authorization', token)
          .send({
            name: 'New Org',
            slug: 'new-org',
          })
          .expect(201);

        expect(body).toEqual(
          expect.objectContaining({
            name: 'New Org',
            slug: 'new-org',
            users: [
              expect.objectContaining({
                role: 'ORG_MAINTAINER',
                userId: 'test',
                name: 'Test Testington',
                username: 'test',
              }),
            ],
          }),
        );
      });
    });

    describe('/:id', () => {
      describe('GET', () => {
        beforeEach(async () => {
          token = `bearer ${await authToken()}`;
          app = (await request())();
        });

        it('should return 401 if no credentials provided', () =>
          app.get('/organization/org1-id').expect(401));

        it('should return 401 if invalid credentials provided', () =>
          app.get('/organization/org1-id').set('authorization', 'bearer bad-token').expect(401));

        it('should return 404 if no access to org', async () => {
          token = `bearer ${await authToken('test2@test.com')}`;

          return app.get('/organization/org1-id').set('authorization', token).expect(404);
        });

        it('should return org', async () => {
          const { body } = await app
            .get('/organization/org1-id')
            .set('authorization', token)
            .expect(200);

          expect(body).toEqual(
            expect.objectContaining({
              name: 'Org 1',
              slug: 'org1',
              users: [
                expect.objectContaining({
                  role: 'ORG_MAINTAINER',
                  userId: 'test',
                  name: 'Test Testington',
                  username: 'test',
                }),
              ],
              id: 'org1-id',
            }),
          );
        });
      });

      describe('DELETE', () => {
        beforeEach(async () => {
          token = `bearer ${await authToken()}`;
          app = (await request())();
        });

        it('should return 401 if no credentials provided', () =>
          app.delete('/organization/org1-id').expect(401));

        it('should return 401 if invalid credentials provided', () =>
          app.delete('/organization/org1-id').set('authorization', 'bearer bad-token').expect(401));
      });

      describe('PATCH', () => {
        beforeEach(async () => {
          token = `bearer ${await authToken()}`;
          app = (await request())();
        });

        it('should return 401 if no credentials provided', () =>
          app.patch('/organization/org1-id').expect(401));

        it('should return 401 if invalid credentials provided', () =>
          app.patch('/organization/org1-id').set('authorization', 'bearer bad-token').expect(401));

        it('should return 404 if no access to org', async () => {
          token = `bearer ${await authToken('test2@test.com')}`;

          return app.patch('/organization/org1-id').set('authorization', token).expect(403);
        });

        it('should fail slug validation', () =>
          app
            .patch('/organization/org1-id')
            .set('authorization', token)
            .send({
              name: 'Invalid',
              slug: 'org2',
            })
            .expect(400, {
              slug: {
                name: 'ValidatorError',
                message: 'Slug already registered',
                properties: {
                  message: 'Slug already registered',
                  type: 'user defined',
                  path: 'slug',
                  value: 'org2',
                  reason: {},
                },
                kind: 'user defined',
                path: 'slug',
                value: 'org2',
                reason: {},
              },
            }));

        it('should update org', async () => {
          const { body } = await app
            .patch('/organization/org1-id')
            .set('authorization', token)
            .send({
              name: 'My name',
              slug: 'my-name',
            })
            .expect(200);

          expect(body).toEqual(
            expect.objectContaining({
              name: 'My name',
              slug: 'my-name',
              users: [
                expect.objectContaining({
                  role: 'ORG_MAINTAINER',
                  userId: 'test',
                  name: 'Test Testington',
                  username: 'test',
                }),
              ],
              id: 'org1-id',
            }),
          );

          expect(new Date(body.updatedAt).getTime()).toBeGreaterThan(
            new Date(body.createdAt).getTime(),
          );
        });
      });
    });
  });
});
