/* eslint-disable import/first, */
process.env.LOG_LEVEL = 'silent';

import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import axios from 'axios';
import * as supertest from 'supertest';
import AppModule from '../src/app.module';

let app: INestApplication;

export const destroyApp = async () => {
  if (app) {
    await app.close();

    /* Remove definition to prevent being run again */
    app = undefined;
  }
};

export const request = async (): Promise<() => supertest.SuperTest<supertest.Test>> => {
  const moduleFixture: TestingModule = await Test.createTestingModule({
    imports: [AppModule],
  }).compile();

  app = moduleFixture.createNestApplication();
  await app.init();

  return () => supertest(app.getHttpServer());
};

export const authToken = async (emailAddress = 'test@test.com', password = 'q1w2e3r4') => {
  // await destroyApp();

  const {
    body: { token },
  } = await (await request())()
    .post('/user/login')
    .send({
      emailAddress,
      password,
    })
    .expect(201);

  // await destroyApp();

  return token;
};

export { supertest };

beforeEach(() =>
  axios({
    url: `${process.env.DATA_INGESTER_URL}/data/reset`,
    method: 'post',
    headers: {
      'content-type': 'application/json',
    },
    data: {},
  }),
);

afterEach(() => destroyApp());
