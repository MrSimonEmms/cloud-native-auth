import { authToken, request } from './setup';

describe('/role', () => {
  let app;
  let token: string;

  describe('/', () => {
    describe('GET', () => {
      beforeEach(async () => {
        token = `bearer ${await authToken()}`;
        app = (await request())().get('/role');
      });

      it('should return 401 if no credentials provided', () => app.expect(401));

      it('should return 401 if invalid credentials provided', () =>
        app.set('authorization', 'bearer bad-token').expect(401));

      it('should return the logged in roles', () =>
        app.set('authorization', token).expect(200, {
          grants: {
            ORG_USER: {
              ORGANIZATION: {
                'read:own': ['*'],
              },
            },
            ORG_MAINTAINER: {
              $extend: ['ORG_USER'],
              ORGANIZATION: {
                'create:own': ['*'],
                'update:own': ['*'],
                'delete:own': ['*'],
              },
            },
            USER_ADMIN: {
              ORGANIZATION: {
                'read:own': ['*'],
                'create:own': ['*'],
              },
              USER: {
                'create:own': ['*'],
                'read:own': ['*'],
                'update:own': ['*'],
                'delete:own': ['*'],
              },
            },
          },
          roles: ['ORG_USER', 'ORG_MAINTAINER', 'USER_ADMIN'],
        }));
    });

    describe('POST', () => {
      beforeEach(async () => {
        token = `bearer ${await authToken()}`;
        app = (await request())().post('/role');
      });

      it('should return 401 if no credentials provided', () => app.expect(401));

      it('should return 401 if invalid credentials provided', () =>
        app.set('authorization', 'bearer bad-token').expect(401));

      it('should return status for user with access', () =>
        app
          .set('authorization', token)
          .send({
            roles: [
              {
                resource: 'ORGANIZATION',
                possession: 'own',
                action: 'create',
              },
            ],
          })
          .expect(200, { hasAccess: true, userRoles: ['USER_ADMIN'] }));

      it('should return status for user without access', () =>
        app
          .set('authorization', token)
          .send({
            roles: [
              {
                resource: 'UNKNOWN_RESOURCE',
                possession: 'own',
                action: 'create',
              },
            ],
          })
          .expect(403));

      it('should return status for user on org with access', () =>
        app
          .set('authorization', token)
          .send({
            orgId: 'org1-id',
            roles: [
              {
                resource: 'ORGANIZATION',
                possession: 'own',
                action: 'create',
              },
            ],
          })
          .expect(200, {
            hasAccess: true,
            orgId: 'org1-id',
            userRoles: ['USER_ADMIN', 'ORG_MAINTAINER'],
          }));

      it('should return status for user on org without access', () =>
        app
          .set('authorization', token)
          .send({
            orgId: 'org1-id',
            roles: [
              {
                resource: 'UNKNOWN_RESOURCE',
                possession: 'own',
                action: 'create',
              },
            ],
          })
          .expect(403));
    });
  });
});
