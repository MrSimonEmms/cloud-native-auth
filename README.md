# Cloud Native Auth

Authentication service that will work in a cloud-native environment

<!-- toc -->

- [Supported Providers](#supported-providers)
- [How It Works](#how-it-works)
  * [Demo](#demo)
- [Getting Started](#getting-started)
  * [Config](#config)
    + [Provider Specific Config](#provider-specific-config)
      - [Local Password Authentication](#local-password-authentication)
    + [Cookies](#cookies)
    + [MongoDB](#mongodb)
  * [Docker Compose](#docker-compose)
  * [Kubernetes/Helm](#kuberneteshelm)
- [Swagger](#swagger)
- [JWT Token](#jwt-token)
- [Authentication](#authentication)
- [Authorization/Role Based Access Control](#authorizationrole-based-access-control)
  * [Organizations](#organizations)
  * [Roles](#roles)
    + [Endpoints](#endpoints)
    + [Extending and Additional Roles](#extending-and-additional-roles)
      - [Example](#example)
- [Hooks](#hooks)
  * [Confirm Email](#confirm-email)
    + [Error Codes](#error-codes)

<!-- tocstop -->

# Supported Providers

- [GitHub](https://github.com/settings/applications/new)
- [GitLab](https://docs.gitlab.com/ee/integration/oauth_provider.html#user-owned-applications)
- [Google](https://developers.google.com/identity/protocols/oauth2)
- [Twitter](https://developer.twitter.com/apps)

# How It Works

![alt text](docs/sequence.png "Sequence Diagram")

There are three actors in the sequence:
 - Your web app
 - Cloud Native Auth
 - Authentication Provider(s)

A user can have multiple providers added and, once registered, must always retain
at least one. This works great with OAuth authentication methods, although it can
be used with other mechanisms.

The workflow is fairly straightforward:
1. User selects the provider that they wish to log in with
2. Application dispatches to Cloud Native Auth
3. Cloud Native Auth configures the URL to redirect to
4. User logs in to the provider
5. Provider returns access tokens to Cloud Native Auth
6. Cloud Native Auth creates or updates a user record, adding the provider's
access tokens
7. Cloud Native Auth redirects to your application's post-login page and returns
a [JSON Web Token](https://jwt.io/introduction) to allow your application to
store the user's state

## Demo

A working demo is available in the [demo](./demo) folder. This shows how to integrate
with Cloud Native Auth.

# Getting Started

You will need to configure at least one authentication provider - for this example,
we will use GitHub.

Log into your GitHub account and [register a new OAuth application](https://github.com/settings/applications/new).
 - **Application name:** &lt;your application name&gt;
 - **Homepage URL:** &lt;your application URL&gt;
 - **Authorization callback URL:** http://localhost:3000/provider/github/callback

This will display a client ID. You will also need to generate a client secret.

## Config

| Environment Variable | Required | Default | Description |
| --- | --- | --- | --- |
| HOOKS_CONFIRM_EMAIL_ENABLED | N️| `false` | Enable the [confirm email hook](#confirm-email)|
| HOOKS_CONFIRM_EMAIL_REDIRECT_URL | Y* | - | The URL to redirect to once the confirmation URL is invoked |
| HOOKS_CONFIRM_EMAIL_URL | Y* | - | The URL to call to trigger the send confirmation email |
| COOKIE_SECRET | Y️| - | String to encrypt cookies. A minimum of 32 bytes is recommended |
| COOKIE_SECURE | N | `false` | Use secure cookies. HTTPS required |
| DOMAIN_CURRENT | Y | - | Domain of the Cloud Native Auth application, e.g. https://example.com. Should also be set to provider callbacks |
| DOMAIN_LOGIN_PATH | Y | - | URL to return to in your application, e.g. https://myapp.com/post-login |
| JWT_EXPIRY | N | `30d` | Expiry time on JWT tokens. Use [zeit/ms](https://github.com/vercel/ms) format |
| JWT_ISSUER | N | `cloud-native-auth` | Name of the issuer |
| JWT_SECRET | Y | - | String to encrypt JWT. A minimum of 32 bytes is recommended |
| LOG_LEVEL | N | `trace` | Log level. Any [Pino](https://getpino.io/#/docs/api?id=level-string) level allowed |
| MONGODB_URL | Y | - | Any [MongoDB URL](https://docs.mongodb.com/manual/reference/connection-string/) |
| MONGODB_AUTO_INDEX | N | `true` | [Should Mongoose create the indices on load?](https://stackoverflow.com/questions/14342708/mongoose-indexing-in-production-code) |
| MONGODB_DB_NAME | N | - | Specify a database name to use. Often required for certain cloud versions of MongoDB (e.g., Azure) |
| MONGODB_POOL_SIZE | N | `5` | Number of connection pools to maintain |
| PAGINATION_MAX_LIMIT | N | `100` | Maximum number of paginated items to return |
| ROLES_PATH | N | `./roles.yaml` | Location of the [custom roles YAML](https://onury.io/accesscontrol/?content=guide#defining-all-grants-at-once) |
| SERVER_ENABLE_SHUTDOWN_HOOKS | N | `true` | Enable the [NestJS Shutdown Hooks](https://docs.nestjs.com/fundamentals/lifecycle-events) |
| SERVER_HELMET_ENABLED | N | `true` | Enable server hardening via [HelmetJS](https://helmetjs.github.io/) |
| SERVER_PORT | N | `3000` | Port to host the server on |
| SERVER_SWAGGER_ENABLED | N | `true` | Publish the Swagger documentation on `/api` and `/api-json` |

### Provider Specific Config

> At least one provider is required

| Environment Variable | Required | Default | Description |
| --- | --- | --- | --- |
| GITHUB_ENABLED | N | `false` | Enable the GitHub provider |
| GITHUB_CLIENT_ID | Y* | - | Provider client ID |
| GITHUB_CLIENT_SECRET | Y* | - | Provider client secret |
| GITHUB_SCOPES | N | - | Any additional OAuth scopes to claim |
| GITLAB_ENABLED | N | `false` | Enable the GitLab provider |
| GITLAB_CLIENT_ID | Y* | - | Provider client ID |
| GITLAB_CLIENT_SECRET | Y* | - | Provider client secret |
| GITLAB_SCOPES | N | - | Any additional OAuth scopes to claim |
| GOOGLE_ENABLED | N | `false` | Enable the Google provider |
| GOOGLE_CLIENT_ID | Y* | - | Provider client ID |
| GOOGLE_CLIENT_SECRET | Y* | - | Provider client secret |
| GOOGLE_SCOPES | N | - | Any additional OAuth scopes to claim |
| PASSWORD_ENABLED | N | `false` | Enable the local password provider |
| TWITTER_ENABLED | N | `false` | Enable the Twitter provider |
| TWITTER_API_KEY | Y* | - | Provider API key |
| TWITTER_API_SECRET_KEY | Y* | - | Provider API secret key |

\* Parameter required if provider enabled

#### Local Password Authentication

Local password authentication exists as a provider. This will store a hashed password
with the user in the database. The hashing mechanism in use is [scrypt](https://nodejs.org/api/crypto.html#crypto_crypto_scrypt_password_salt_keylen_options_callback).
following the advice of [Michele Preziuso](https://medium.com/analytics-vidhya/password-hashing-pbkdf2-scrypt-bcrypt-and-argon2-e25aaf41598e).
The reason for choosing scrypt over argon2 because NodeJS supports scrypt natively -
[argon2](https://www.npmjs.com/package/argon2) requires C/C++ bindings with `node-gyp`
support. Whilst it works perfectly fine in a containerized environment, it can make
local development more problematic as the bindings would need to be built in the Alpine
image.

Encryption is something that will need revisiting regularly so this is by no means a
final decision.

### Cookies

Although mostly a stateless application, cookies are used to hold a user ID received
by the initial `GET:/provider/:id` endpoint. This is then used by the
`GET:/provider/:id/callback` endpoint, so the application knows which user to assign
the provider to.

In the OAuth specification, there is no way of assigning generic variables to the
provider call and then having them being returned. This limited and specific way of
maintaining a state between these calls is a necessary step. All cookies are cleaned
up once used and are only valid for the life of the browser session.

Cookies are also configured to provide a [pseudo-session](https://github.com/mozilla/node-client-sessions)
to allow usage of providers that require a session - this is the case with OAuth1.0a,
as used by Twitter.

To reduce the infrastructure required to maintain, secure cookies are used. This is
to avoid the requirement for an external session store, such as Redis.

### MongoDB

MongoDB is used as the data store. No special features are used so any version
later than v3.0 should be compatible.

## Docker Compose

> This should only be used for local development

```shell
(cd demo && npm ci) # Install demo dependencies
npm ci # Install dependencies
export GITHUB_ENABLED=true
export GITHUB_CLIENT_ID=<value of client ID>
export GITHUB_CLIENT_SECRET=<value of client secret>
docker-compose up
```

This will load the demo application at [localhost:9999](http://localhost:9999).

## Kubernetes/Helm

> Coming soon

# Swagger

Swagger documentation is enabled by default. See [localhost:3000/api](http://localhost:3000/api)
for details. The raw JSON is also viewable at [localhost:3000/api-json](http://localhost:3000/api-json).

# JWT Token

Authentication is provided by a [JSON Web Token](https://jwt.io/introduction). The
token can be provided in two distinct ways:
 - as a header, e.g. `Authorization: Bearer <token>`
 - as a query string, e.g. `?_auth=<token>`

The query string option is provided so that adding an additional provider to a user
can be done a simple HTTP call via a link. The login process requires forwarding
the user to the provider's login page, so this cannot be done via a proxied call -
browsers cannot add custom headers to an `a` tag.

```html
<a href="/provider/{{ provider.id }}?_auth={{ token }}">{{ provider.name }}</a>
```

If you want to avoid having the token publicly visible in your HTML, you can set up
a redirect endpoint in your application that forwards `/provider/:action/:providerId`
to the endpoint - see the [demo](./demo) application for a working example of this
mechanism.

# Authentication

The purpose of authentication is to ensure that the user is who they say they are.

- `GET:/provider` - get list of available providers. This returns a JSON array of
  providers
- `GET:/provider/:id` - dispatch to the provider login page. If the query is given
  with a valid JWT Token, it adds the provider to an existing user
- `GET:/provider/:id/callback` - verify the provider and save to user. Returns to
  your post-login page with the JWT Token in base 64 format or error code as a query
  string
- `DELETE:/provider/:id` - remove a provider from a user. JWT Token is required.

# Authorization/Role Based Access Control

The purpose of authorization is to ensure that the user is able to access a resource.

## Organizations

A user can create any number of organizations. An "organization" is a collection of
users that are granted rights within that organization. It's outside the scope of
this project how that's implemented - this is purely focused on organization creation
and validation of the actions within that.

All these endpoints require authentication using the [token](#jwt-token).

In order to create an organization, the user must be `registered`. This is controlled
outside this application and left up to you to decide how best to achieve that. Typically,
that would involve the userdata being provided and validated.

The `/organization` namespace provides CRUD endpoints:

- `GET:/organization` - get a list of available organization. These are locked to
organizations the user is a member of. This uses [@nestjsx/crud-request](https://github.com/nestjsx/crud/wiki/Requests#description)
to provide searching via query parameters.
- `POST:/organization` - creates a new organization
- `GET:/organization/:id` - gets a single organization via the `id`
- `PATCH:/organization/:id` - updates an organization via the `id`
- `DELETE:/organization/:id` - deletes an organization via the `id`

## Roles

> This wraps [AccessControl](https://onury.io/accesscontrol/). An understanding of how this
> works is assumed.

A role grants a user rights. These are purely additive - if one role grants a right, there is
no way to rescind it.

Role queries are broken down into 3 parameters:
- `resource` - the name of the resource, such as `USER`, `ORGANIZATION` etc
- `action` - the CRUD action being performed, `create`, `read`, `update` or `delete`
- `possession` - either `own` (a resource the user is part of) or `any` (a public resource)

This can optionally receive the `orgId` as well.

### Endpoints

- `GET:/role` - get a list of roles and the grants.
- `POST:/role` - verify the access for a role. This will return a `403` error access not granted

### Extending and Additional Roles

> Adding a custom `roles.yaml` will not affect any of the base roles

By default, there are three roles defined - `ORG_MAINTAINER`, `ORG_USER` and `USER_ADMIN`. These
are fully configured, but you will almost certainly need to extend this with additional roles.

This can be done by configuring a `roles.yaml` - the location of this is controlled by the `ROLES_PATH`
environment variable. The YAML file itself takes it in the format:

```yaml
<role_name>:
  <resource>:
    <action>:<possession>:
      - <parameter_filters>
```

#### Example

This creates a new `VIDEO` resource, granting `read:own` rights, but filtering out the `rating` and `views` parameters

```yaml
ORG_MAINTAINER:
  VIDEO:
    read:own:
      - "*"
      - "!rating"
      - "!views"
```

# Hooks

Hooks exist to enable optional workflows to be plugged into the application

## Confirm Email

The `user.confirmed` parameter exists to mark when the email address has been
confirmed. If enabled, whenever the `user.emailAddress` property is modified,
the `user.confirmed` parameter will be set to `false` and a call will be made
to trigger an email.

This will POST to the URL given in the `HOOKS_CONFIRM_EMAIL_URL` hook, sending
this body:

```typescript
interface IConfirmEmailCall {
  url: string;
  user: {
    id: string;
    username: string | null;
    emailAddress: string;
    registered: boolean;
    confirmed: boolean;
    createdAt: Date;
    updatedAt: Date;
  }
}
```

The `url` should be presented to the user to encourage them to click it to
confirm their email address. When they visit the URL, they will be redirected
to the URL in `HOOKS_CONFIRM_EMAIL_REDIRECT_URL`, with either `?confirmed=true`
or `?error=<CODE>` as a query string.

### Error Codes

 - `UNABLE_TO_CONFIRM_EMAIL`: Cannot find the user in the database. Usually,
this will be because they have already confirmed the email or they have changed
the email since the token was generated.
 - `INVALID_CONFIRM_EMAIL_TOKEN`: Generic error.
