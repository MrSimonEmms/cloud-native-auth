ARG NODE_VERSION=14-alpine
FROM node:${NODE_VERSION}
WORKDIR /opt/app
COPY dist dist
COPY node_modules node_modules
COPY package.json .
COPY package-lock.json .
COPY roles.yaml .
ENV LOG_LEVEL=trace
ENV SERVER_ENABLE_SHUTDOWN_HOOKS="true"
ENV SERVER_PORT=3000
# Do not rely on NODE_ENV - exists for performance reasons only
ENV NODE_ENV=production
EXPOSE 3000
USER node
CMD [ "npm", "run", "start:prod" ]
