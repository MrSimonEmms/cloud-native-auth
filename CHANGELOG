# [1.0.0-rc.15](https://gitlab.com/MrSimonEmms/cloud-native-auth/compare/v1.0.0-rc.14...v1.0.0-rc.15) (2021-09-16)


### Bug Fixes

* **dependencies:** update to nestjs v8 ([7d17ffd](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/7d17ffd3c60a3c611c2a3ac8adb977868692a844))

# [1.0.0-rc.14](https://gitlab.com/MrSimonEmms/cloud-native-auth/compare/v1.0.0-rc.13...v1.0.0-rc.14) (2021-08-16)


### Bug Fixes

* **user:** remove the tmpPassword from toJSON and toObject ([54bd7af](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/54bd7afd25412311288a4586f9f32d18341ab9c8))

# [1.0.0-rc.13](https://gitlab.com/MrSimonEmms/cloud-native-auth/compare/v1.0.0-rc.12...v1.0.0-rc.13) (2021-07-03)


### Bug Fixes

* **logger:** check for a correlation id header ([3e0a158](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/3e0a1585498278c877669cab48a5e59e351394fe))

# [1.0.0-rc.12](https://gitlab.com/MrSimonEmms/cloud-native-auth/compare/v1.0.0-rc.11...v1.0.0-rc.12) (2021-06-01)


### Features

* **provider:** add user password as login provider ([387cee2](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/387cee2744cbb397f0660d1e4baa02c0f4e95916))

# [1.0.0-rc.11](https://gitlab.com/MrSimonEmms/cloud-native-auth/compare/v1.0.0-rc.10...v1.0.0-rc.11) (2021-05-27)


### Bug Fixes

* **roles:** add roles.yaml to docker image ([0c602eb](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/0c602eb0819edad75ed867296fa3d253b99d5b6f))

# [1.0.0-rc.10](https://gitlab.com/MrSimonEmms/cloud-native-auth/compare/v1.0.0-rc.9...v1.0.0-rc.10) (2021-05-23)


### Bug Fixes

* **organizations:** fix spelling in PAGINATION_MAX_LIMIT envvar ([ac9eeec](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/ac9eeeccae14838538e70c2bae002b653d00160a))


### Features

* **user:** prevent user deletion if it would leave invalid organization model ([c1a2d35](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/c1a2d3582e8a85ced11903c26ce76900641b7ad2))

# [1.0.0-rc.9](https://gitlab.com/MrSimonEmms/cloud-native-auth/compare/v1.0.0-rc.8...v1.0.0-rc.9) (2021-05-22)


### Features

* **organizations:** create organization module ([5e71782](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/5e7178268c3e0e877523a7141359e87a9c2cbe82))
* **request-parser:** create request parser for mongodb ([e4600c3](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/e4600c3efb3da4ece86434d98e4dde7eba12f5fc))
* **roles:** add organization roles and methods ([39cc899](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/39cc899ab86a3e5a3b5fded47a77a28860780840))
* **roles:** add roles module ([5917faf](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/5917faffee422ed09e6b8aead752fb16e98475d7))
* **roles:** add user roles ([3680c20](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/3680c201da4f336c53ebc35719b718da4266f061))

# [1.0.0-rc.8](https://gitlab.com/MrSimonEmms/cloud-native-auth/compare/v1.0.0-rc.7...v1.0.0-rc.8) (2021-05-08)


### Bug Fixes

* **schemas:** remove the providerUser schema as root-level schema ([8b7ba1f](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/8b7ba1f7068a2aceff5059568a1eb7e4f22e9dfe))


### Features

* **user:** dispatch a confirm email hook whenever the email changes ([9ae218b](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/9ae218b2b342383a87f6c878374b77edced35b3c))

# [1.0.0-rc.7](https://gitlab.com/MrSimonEmms/cloud-native-auth/compare/v1.0.0-rc.6...v1.0.0-rc.7) (2021-05-06)


### Bug Fixes

* **config:** update configuration defaults ([dcfee1c](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/dcfee1c97b36383535217b51d13993d8a2937cfc))
* **provider:** change tokens to be key/value pair ([82d8300](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/82d830033ed0a4bc2de893907196c395c0841290))
* **user:** update data types ([2975bcb](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/2975bcbb8bb9f6b953a5b02e34c51fad4c287a3f))


### Features

* **server:** add helmet to the express server ([ec35c52](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/ec35c526c1ec759844488a3de52f8e74a7d398ba))
* **swagger:** add swagger documentation for the endpoints ([5a669b2](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/5a669b2bde1c13da9d208cadf811b0e34a8ce501))

# [1.0.0-rc.6](https://gitlab.com/MrSimonEmms/cloud-native-auth/compare/v1.0.0-rc.5...v1.0.0-rc.6) (2021-05-05)


### Features

* **bootstrap:** run check on boot that at least one provider is enabled ([7f4db09](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/7f4db092ff739fd88deefa4bc2bd47877e787fe8))

# [1.0.0-rc.5](https://gitlab.com/MrSimonEmms/cloud-native-auth/compare/v1.0.0-rc.4...v1.0.0-rc.5) (2021-05-05)


### Features

* **providers:** add custom scope option to the oauth2 providers ([3e9c280](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/3e9c280676c271bef9622e48d9780b954d3b87aa))

# [1.0.0-rc.4](https://gitlab.com/MrSimonEmms/cloud-native-auth/compare/v1.0.0-rc.3...v1.0.0-rc.4) (2021-05-04)


### Features

* **provider:** add google as an auth provider ([e37d021](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/e37d021bf641237438338b18872ab7f55425545a))

# [1.0.0-rc.3](https://gitlab.com/MrSimonEmms/cloud-native-auth/compare/v1.0.0-rc.2...v1.0.0-rc.3) (2021-05-04)


### Bug Fixes

* **passport:** disable session for initial passport login calls ([229f14a](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/229f14a527dc44decf158ee9b745c2e18400d8d5))
* **providers:** change provider config to be DRYer ([359c70a](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/359c70a95bf9686c87eac92c5f3cec4828beacf8))
* **user:** change providers to a map from an array ([c1ca894](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/c1ca89400103598ace17ad4af8e8af878b630545))


### Features

* **session:** add client-session to fake req.session ([49bac00](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/49bac00304c2c291e808e538fe9ee86f07fd604e))
* **twitter:** add twitter provider ([11b01e1](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/11b01e1d6d11106f63835fb8b8568b9c18a56387))

# [1.0.0-rc.2](https://gitlab.com/MrSimonEmms/cloud-native-auth/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2021-05-04)


### Features

* **provider:** add gitlab as auth provider ([4df8756](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/4df8756f783b539482c419be3eab510f43783ab1))
* **user:** create endpoint for removing a provider from a user ([a400284](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/a40028413886232980e896926be9a7665576a5c4))

# 1.0.0-rc.1 (2021-05-03)


### Features

* **authentication:** add ability to link an additional provider to an existing user ([7a6e327](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/7a6e327d9e432223f268cf02554fb479360bad1d))
* **authentication:** add first auth provider to the user ([cac5c15](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/cac5c1554e229aabd218374ce25f669541b66d62))
* **config:** add nest config service and add server settings ([4af3893](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/4af3893c1145bb0947b57e89c4eeafbadb7765cb))
* **cookies:** add cookie parser to the app ([538a41c](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/538a41c014edd3723e124b11da480ac9983ed2b6))
* **logger:** setup pino logger ([af6fef9](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/af6fef997eb9a9c39029848e0df5693ee4fc0bb3))
* **mongodb:** initial setup for mongodb ([906fc35](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/906fc35556d123dbde6e9defe7a11621794cf1b9))
* **monitoring:** add prometheus to the application ([932b30b](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/932b30bd12f327d9a2a2baf908f554c306596b91))
* **passport:** add anonymous authentication strategy ([3208579](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/3208579795be3734a6aece0de0b6f2eae4d5fe73))
* **passport:** configure passport module with jwt auth ([c025e14](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/c025e14946d0e178c766a7ea1bf936a99d600dd5))
* **user:** add crud methods to update the user model ([011ee56](https://gitlab.com/MrSimonEmms/cloud-native-auth/commit/011ee563d01e3cc09479656c8565faed9263d8d7))
