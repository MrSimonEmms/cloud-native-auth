/**
 * Demo
 *
 * @license MIT
 * @author Simon Emms <simon@simonemms.com>
 */

const express = require('express');
const axios = require('axios');
const session = require('express-session');
const redis = require('redis');
const connectRedis = require('connect-redis');
require('express-async-errors');

const config = {
  login: {
    url: process.env.LOGIN_URL,
    www: process.env.LOGIN_WWW,
  },
  server: {
    port: Number(process.env.PORT ?? 9999),
  },
  session: {
    host: process.env.SESSION_HOST,
    secret: process.env.SESSION_SECRET,
  },
};

const app = express();

const RedisStore = connectRedis(session);

app
  .use(
    // Not production ready
    session({
      store: new RedisStore({ client: redis.createClient(config.session.host) }),
      secret: config.session.secret,
      resave: false,
      saveUninitialized: false,
    }),
  )
  .use(async (req, res, next) => {
    /* Get the available providers */
    const { data } = await axios.get('/provider', {
      baseURL: config.login.url,
    });

    req.providers = data;

    next();
  });

app.set('view engine', 'pug').set('views', `${__dirname}/views`);

app
  .get('/', async (req, res) => {
    const { token } = req.session;
    if (!token) {
      res.redirect('/login');
      return;
    }

    const { data } = await axios.get('/user', {
      headers: {
        authorization: `Bearer ${req.session.token}`,
      },
      baseURL: config.login.url,
    });

    res.render('pages/login', {
      title: 'Providers',
      user: data,
      disableLogin: Object.keys(data.providers).length <= 1,
      providers: req.providers,
      login: config.login,
      isLoggedIn: (providerId) => !!data.providers[providerId],
      token,
    });
  })
  .get('/logout', (req, res) => {
    delete req.session.token;

    res.redirect('/');
  })
  .get('/login', (req, res) => {
    if (req.session.token) {
      res.redirect('/');
      return;
    }

    res.render('pages/login', {
      title: 'Login',
      providers: req.providers,
      login: config.login,
    });
  })
  .get('/login/callback', async (req, res) => {
    // Save token to session
    req.session.token = Buffer.from(req.query.token, 'base64').toString('ascii');

    res.redirect('/');
  })
  .get('/provider/:action/:providerId', async (req, res) => {
    const { token } = req.session;
    const { action, providerId } = req.params;

    let url = '/';

    if (action === 'link') {
      console.log(`Logging in to ${providerId}`);
      url = `${config.login.www}/provider/${providerId}`;

      if (token) {
        url += `?_auth=${token}`;
      }
    } else {
      console.log(`Logging out of ${providerId}`);

      await axios.delete(`/provider/${providerId}`, {
        headers: {
          authorization: `Bearer ${req.session.token}`,
        },
        baseURL: config.login.url,
      });
    }

    res.redirect(url);
  });

app.listen(config.server.port, () => {
  console.log(`Listening on ${config.server.port}`);
  // Contains secure variables not normally exposed in production
  console.log(JSON.stringify(config, null, 2));
});
