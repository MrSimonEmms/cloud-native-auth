# Demo

This is a demo app describing how to interact with Cloud Native Auth as a
microservice.

> This demo application is **NOT** production-ready and contains some potential
> security issues.

# Getting Started

> This will require the Cloud Native App to be setup correctly. Please view that
> [README](../README.md) for details

```shell
cd ./demo
npm ci

docker-compose up
```

- Go to [localhost:9999](http://localhost:9999)
- Log in to a provider
